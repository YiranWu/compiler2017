grammar Mxstar;


prog : (simpDeclStmt|funcDef|classDef)* ;//done


declaration
	:	simpDeclStmt        #Simp//done
	|	funcDef         #Func//done
	|	classDef        #Class//done
	;

simpDeclStmt : typeSpec Identifier ('=' expression)? ';' ;//done

typeSpec
    :   simpTypeSpec        #SimpType//done
    |   typeSpec '['']'      #ArrayType//done
	|	Identifier          #ClassType
    ;

simpTypeSpec
	:	typespec='bool'
	|	typespec='int'
	|	typespec='void'
	|	typespec='string'
	;//alldone

newTypeSpec
    :   typeSpec ('[' expression ']')+ ('['']')+ ('[' expression ']')+  #ExceptionNew//done
    |   typeSpec ('[' expression ']')+ ('['']')*                        #ArrayNew//done
    |   typeSpec                                                        #SimpNew//done
    ;

funcDef : typeSpec Identifier? '(' (typeSpec Identifier (',' typeSpec Identifier)*)? ')' blkStmt ;//done

classDef : 'class' Identifier '{' (funcDef | simpDeclStmt)* '}' ;

stmt
	:	exprStmt
   	|	blkStmt
	|	ifStmt
	|	iterStmt
	|	ctrlStmt
	|   simpDeclStmt
	;

exprStmt : expression? ';' ;//done

blkStmt : '{' stmt* '}' ;//done

ifStmt : 'if' '(' expression ')' thenstmt=stmt ('else' elsestmt=stmt)? ;//done

iterStmt
	:	'for' '(' init=expression? ';' cond=expression? ';' step=expression? ')' stmt    #ForStmt//done
	|	'while' '(' expression ')' stmt                       #WhileStmt//done
	;

ctrlStmt
	:	'break' ';'             #BreakStmt//done
	|	'continue' ';'          #ContinueStmt//done
	|	'return' expression?';' #RetStmt//done
	;


expression
	:	Identifier                       #VarExpr//done
	|	constant                         #ConstExpr//done
	|	'(' expression ')'               #ParenExpr//done


	|	expression op=('++' | '--')                  #Postfix
	|	arrname=expression '[' indice=expression ']'          #ArrayExpr
	|	expression '(' (expression (',' expression)*)? ')'      #FuncCall
	|	expression '.' Identifier        #MemberExpr


	|	<assoc=right> op=('++' | '--' | '-' | '~' | '!') expression      #Unary
    |   <assoc=right> 'new' newTypeSpec    #New


	|	expression op=('*' | '/' | '%') expression       #MulLevel

	|	expression op=('+' | '-') expression       #AddLevel

	|	expression op=('<<' | '>>') expression       #ShiftLevel

	|	expression op=('>' | '>=' | '<' | '<=') expression        #CmpLevel

	|	expression op=('==' | '!=') expression       #EqualLevel

	|	expression op='&' expression        #AndExpr

	|	expression op='^' expression        #XorExpr

	|	expression op='|' expression        #OrExpr

	|	expression op='&&' expression       #LogicAndExpr

	|	expression op='||' expression       #LogicOrExpr

	|	<assoc=right> expression op='=' expression        #AssignExpr
    ;//all done

constant : type=StrLiteral | type=IntConstant | type=BoolConstant | type=NullConstant ;//done




Break : 'break' ;
Char : 'char' ;
Class : 'class' ;
Continue : 'continue' ;
Else : 'else' ;
For : 'for' ;
If : 'if' ;
Int : 'int' ;
Return : 'return' ;
Void : 'void';
While : 'while';
Bool : 'bool';
String : 'string';
New : 'new' ;
LeftParen : '(';
RightParen : ')';
LeftBrack : '[';
RightBrack : ']';
LeftBrace : '{';
RightBrace : '}';
Less : '<';
Leq : '<=';
Greater : '>';
Geq : '>=';
LeftShift : '<<';
RightShift : '>>';
Plus : '+';
PlusPlus : '++';
Minus : '-';
MinusMinus : '--';
Mul : '*';
Div : '/';
Mod : '%';
And : '&';
AndAnd : '&&';
Or : '|';
OrOr : '||';
Caret : '^';
Not : '!';
Tilde : '~';
Semi : ';';
Coma : ',';
Assign : '=';
Equal : '==';
Neq : '!=';
Dot : '.';


fragment
Digit : [0-9] ;

fragment
NonDigit : [a-zA-Z_] ;



IntConstant : '0' | [1-9]Digit* ;

BoolConstant : 'true' | 'false' ;

NullConstant : 'null' ;

StrLiteral : '"' StrChar* '"' ;

fragment
StrChar
	:	~["\n]
	|	EscapeSeq
	;

fragment
EscapeSeq : '\\'  [n\"] ;

Identifier : NonDigit (NonDigit|Digit)* ;

Blank : [ \t\n]+ -> skip ;

Comment : '//' ~[\n]* -> skip ;


