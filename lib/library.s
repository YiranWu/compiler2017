	global main
	extern printf
	extern scanf
	extern strlen
	extern malloc
	extern strcpy
	extern sprintf
	extern strncpy
	extern atoi
	extern strcat
	extern strcmp

	section .text

SysPrint:
	mov rdi, StrFormat
	sub rsp, 8
	call printf
	add rsp, 8
	ret

SysPrintInt:
	mov rdi, IntFormat
	sub rsp, 8
	call printf
	add rsp, 8
	ret

SysPrintLn:
	mov rdi, StrLnFormat
	sub rsp, 8
	call printf
	add rsp, 8
	ret

SysGetStr:
	mov rdi, StrFormat
	mov rsi, StrBuffer
	sub rsp, 8
	call scanf
	mov rdi, StrBuffer
	call strlen
	mov rdi, rax
	add rdi, 1
	call malloc
	mov rdi, rax
	mov rsi, StrBuffer
	call strcpy
	add rsp, 8
	ret
	
SysGetInt:
	mov rdi, IntFormat
	mov rsi, IntBuffer
	sub rsp, 8
	call scanf
	add rsp, 8
	mov rax, [IntBuffer]
	ret
	
SysToStr:
	mov rdi, StrBuffer
	mov rsi, IntFormat
	sub rsp, 8
	call sprintf
	mov rdi, StrBuffer
	call strlen
	mov rdi, rax
	add rdi, 1
	call malloc
	mov rdi, rax
	mov rsi, StrBuffer
	call strcpy
	add rsp, 8
	ret

SysStrLen:
	sub rsp, 8
	call strlen
	add rsp, 8
	ret

SysStrSub:
	mov rdi, StrBuffer
	push rdx
	mov r10b, 0
	mov [rdi+rdx], r10b
	call strncpy
	pop rdi
	add rdi, 1
	sub rsp, 8
	call malloc
	mov rdi, rax
	mov rsi, StrBuffer
	call strcpy
	add rsp, 8
	ret

SysParseInt:
	sub rsp, 8
	call atoi
	add rsp, 8
	ret

SysStrConcat:
	push rdi
	mov rdi, StrBuffer
	call strcpy
	mov rdi, rax
	pop rsi
	sub rsp, 8
	call strcat
	mov rdi, rax
	call strlen
	mov rdi, rax
	add rdi, 1
	call malloc
	mov rdi, rax
	mov rsi, StrBuffer
	call strcpy
	add rsp, 8
	ret





