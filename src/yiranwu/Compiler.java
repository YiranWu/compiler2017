package yiranwu;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import yiranwu.BackEnd.CFG.Allocator.GlobalAllocator.GlobalAllocator;
import yiranwu.BackEnd.CFG.Graph;
import yiranwu.BackEnd.Translator.x86Translator;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.CST.Listener.ASTBuilderListener;
import yiranwu.FrontEnd.CST.Listener.ClassNameListener;
import yiranwu.FrontEnd.CST.Listener.FuncNameAndDeclListener;
import yiranwu.FrontEnd.CST.Listener.SyntaxErrorListener;
import yiranwu.FrontEnd.CST.Parser.MxstarLexer;
import yiranwu.FrontEnd.CST.Parser.MxstarParser;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.io.PrintStream;

/**
 * Created by wyr on 17-5-20.
 */
public class Compiler {
    public static void main(String [] argv) {
        try {
            GlobalTable.init();

            ANTLRInputStream input = new ANTLRInputStream(System.in);
            MxstarLexer lexer = new MxstarLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            MxstarParser parser = new MxstarParser(tokens);
            parser.removeErrorListeners();
            parser.addErrorListener(new SyntaxErrorListener());
            ParseTree tree = parser.prog();
            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(new ClassNameListener(), tree);
            walker.walk(new FuncNameAndDeclListener(), tree);
            walker.walk(new ASTBuilderListener(), tree);

            if(!GlobalTable.symbolTable.contains("main"))
                throw new CE("no main func");

            for (FuncType funcType : GlobalTable.prog.funcList) {
                funcType.graph=new Graph(funcType);
                funcType.allocator=new GlobalAllocator(funcType);
            }
            new x86Translator(new PrintStream(System.out)).translate();
        }
        catch (CE e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        catch (InternalError e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
