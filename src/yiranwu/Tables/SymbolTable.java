package yiranwu.Tables;

import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Utility.CE;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by wyr on 17-5-20.
 */
public class SymbolTable {
    public Map<String,Stack<Symbol>> allSymbol;
    public Stack<Map<String,Symbol>> symbolStack;

    public SymbolTable() {
        allSymbol=new HashMap<>();
        symbolStack=new Stack<>();
    }

    public Symbol add(String name,Type type) {
        if (symbolStack.peek().containsKey(name))
            throw new CE("Duplicate symbol named "+name);
        if (!allSymbol.containsKey(name)) allSymbol.put(name, new Stack<>());
        Symbol symbol=new Symbol(name, type);
        allSymbol.get(name).push(symbol);
        symbolStack.peek().put(name, symbol);
        return symbol;
    }

    public Symbol addGlobalVar(String name, Type type) {
        Symbol symbol=add(name,type);
        symbol.reg=GlobalTable.regTable.addGlobalReg(symbol);
        return symbol;
    }

    public Symbol addTempVar(String name, Type type) {
        Symbol symbol=add(name,type);
        symbol.reg=GlobalTable.regTable.addTempReg(symbol);
        return symbol;
    }

    public Symbol addParamVar(String name, Type type) {
        Symbol symbol=add(name, type);
        symbol.reg=GlobalTable.regTable.addParamReg(symbol);
        return symbol;
    }

    public Symbol get(String name) {return allSymbol.get(name).peek();}

    public boolean contains(String name) {return allSymbol.containsKey(name);}

    public void enterScope() {symbolStack.push(new HashMap<>());}

    public void exitScope() {
        symbolStack.pop().forEach((name, symbol) -> {
            allSymbol.get(name).pop();
            if(allSymbol.get(name).empty()) allSymbol.remove(name);
        });
    }
}
