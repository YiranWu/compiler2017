package yiranwu.Tables;

import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-20.
 */
public class Symbol {
    public String name;
    public Type type;
    public Scope scope;
    public VReg reg;

    public Symbol(String name,Type type) {
        this.name = name;
        this.type = type;
        this.scope = GlobalTable.getScope();
        this.reg = null;
    }
}
