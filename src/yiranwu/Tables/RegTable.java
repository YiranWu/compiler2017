package yiranwu.Tables;

import yiranwu.BackEnd.CFG.Operand.VReg.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wyr on 17-5-20.
 */
public class RegTable{
    public Set<VReg> regList;

    public RegTable() {regList = new HashSet<>();}

    public VReg addGlobalReg(Symbol symbol) {
        VReg reg = new GlobalReg(symbol);
        regList.add(reg);
        return reg;
    }

    public VReg addTempReg(Symbol symbol) {
        VReg reg=new TempReg(symbol);
        regList.add(reg);
        return reg;
    }

    public VReg addParamReg(Symbol symbol) {
        VReg reg=new ParamReg(symbol);
        regList.add(reg);
        return reg;
    }

    public VReg addStrReg(String literal) {
        VReg reg = new StrReg(literal);
        regList.add(reg);
        return reg;
    }
}
