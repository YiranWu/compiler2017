package yiranwu.Tables;

import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.Utility.CE;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wyr on 17-5-20.
 */
public class ClassTable {
    public Map<String, ClassType> classList;

    public ClassTable() {
        classList = new HashMap<>();
    }

    public void put(String name, ClassType type) {
        if (classList.containsKey(name))
            throw new CE("Duplicate class named "+name);
        classList.put(name, type);
    }

    public ClassType get(String name) {return classList.get(name);}

    public boolean contains(String name) {return classList.containsKey(name);}
}
