package yiranwu.Tables;


import yiranwu.FrontEnd.AST.Stmt.IterStmt;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.FuncType;

import java.util.Stack;


/**
 * Created by wyr on 17-5-20.
 */

public class ScopeTable {
    public Stack<Scope> scopeList;
    public Stack<ClassType> classList;
    public Stack<FuncType> funcList;
    public Stack<IterStmt> iterList;

    public ScopeTable() {
        scopeList = new Stack<>();
        classList = new Stack<>();
        funcList = new Stack<>();
        iterList = new Stack<>();
    }

    public void enterScope(Scope scope) {
        scopeList.push(scope);
        if (scope instanceof IterStmt) iterList.push((IterStmt)scope);
        if (scope instanceof FuncType) funcList.push((FuncType)scope);
        if (scope instanceof ClassType) classList.push((ClassType)scope);
    }

    public void exitScope() {
        if (scopeList.empty())
            throw new InternalError("Exiting Scope with no scope");////////
        Scope scope = scopeList.pop();
        if (scope instanceof ClassType) {
            if (classList.empty())
                throw new InternalError("Exiting classScope with no class");///////
            classList.pop();
        }
        if (scope instanceof FuncType) {
            if (funcList.empty())
                throw new InternalError("Exiting funcScope with no func");
            funcList.pop();
        }
        if (scope instanceof IterStmt) {
            if (iterList.empty())
                throw new InternalError("Exiting loopScope with no loop");
            iterList.pop();
        }
    }

    public Scope getScope() {
        if (scopeList.empty()) return null;
        return scopeList.peek();
    }

    public ClassType getClassScope() {
        if (classList.empty()) return null;
        return classList.peek();
    }

    public FuncType getFuncScope() {
        if (funcList.empty()) return null;
        return funcList.peek();
    }

    public IterStmt getIterScope() {
        if (iterList.empty()) return null;
        return iterList.peek();
    }
}
