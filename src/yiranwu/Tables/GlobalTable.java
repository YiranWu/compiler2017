package yiranwu.Tables;

import yiranwu.FrontEnd.AST.Stmt.IterStmt;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;
import yiranwu.FrontEnd.AST.Type.Basic.VoidType;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Prog;

import java.util.ArrayList;

/**
 * Created by wyr on 17-5-20.
 */
public class GlobalTable {
    public static Prog prog;
    public static ScopeTable scopeTable;
    public static SymbolTable symbolTable;
    public static ClassTable classTable;
    public static RegTable regTable;
    public static int HackCode;

    public static void init() {
        scopeTable = new ScopeTable();
        symbolTable = new SymbolTable();
        classTable = new ClassTable();
        regTable = new RegTable();
        enterScope(prog = Prog.getProg());
        addBuiltinFunctions();
    }

    public static void enterScope(Scope scope) {
        scopeTable.enterScope(scope);
        symbolTable.enterScope();
    }

    public static void exitScope() {
        symbolTable.exitScope();
        scopeTable.exitScope();
    }

    public static Scope getScope() {return scopeTable.getScope();}

    public static ClassType getClassScope() {return scopeTable.getClassScope();}

    public static FuncType getFuncScope() {return scopeTable.getFuncScope();}

    public static IterStmt getIterScope() {return scopeTable.getIterScope();}

    public static void addBuiltinFunctions() {
        symbolTable.add("print", FuncType.getFuncType(
                "SysPrint",
                VoidType.voidObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("str", StringType.stringObj));
                }}
        ));
        symbolTable.add("println", FuncType.getFuncType(
                "SysPrintln",
                VoidType.voidObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("str", StringType.stringObj));
                }}
        ));
        symbolTable.add("getString", FuncType.getFuncType(
                "SysGetStr",
                StringType.stringObj,
                new ArrayList<>()
        ));
        symbolTable.add("getInt", FuncType.getFuncType(
                "SysGetInt",
                IntType.intObj,
                new ArrayList<>()
        ));
        symbolTable.add("toString", FuncType.getFuncType(
                "SysToStr",
                StringType.stringObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("int", IntType.intObj));
                }}
        ));
        symbolTable.add("SysArraySize", FuncType.getFuncType(
                "SysArraySize",
                IntType.intObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("this", VoidType.voidObj));
                }}
        ));
        symbolTable.add("SysStrLen", FuncType.getFuncType(
                "SysStrLen",
                IntType.intObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("this", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrOrd", FuncType.getFuncType(
                "SysStrOrd",
                IntType.intObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("this", StringType.stringObj));
                    add(new Symbol("pos", IntType.intObj));
                }}
        ));
        symbolTable.add("SysStrSub", FuncType.getFuncType(
                "SysStrSub",
                StringType.stringObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("this", StringType.stringObj));
                    add(new Symbol("lhs", IntType.intObj));
                    add(new Symbol("rhs", IntType.intObj));
                }}
        ));
        symbolTable.add("SysStrParseInt", FuncType.getFuncType(
                "SysStrParseInt",
                IntType.intObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("this", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrConcat", FuncType.getFuncType(
                "SysStrConcat",
                StringType.stringObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("lhs", StringType.stringObj));
                    add(new Symbol("rhs", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrEqual", FuncType.getFuncType(
                "SysStrEqual",
                BoolType.boolObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("lhs", StringType.stringObj));
                    add(new Symbol("rhs", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrGreat", FuncType.getFuncType(
                "SysStrGreat",
                BoolType.boolObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("lhs", StringType.stringObj));
                    add(new Symbol("rhs", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrGeq", FuncType.getFuncType(
                "SysStrGeq",
                BoolType.boolObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("lhs", StringType.stringObj));
                    add(new Symbol("rhs", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrLess", FuncType.getFuncType(
                "SysStrLess",
                BoolType.boolObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("lhs", StringType.stringObj));
                    add(new Symbol("rhs", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrLeq", FuncType.getFuncType(
                "SysStrLeq",
                BoolType.boolObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("lhs", StringType.stringObj));
                    add(new Symbol("rhs", StringType.stringObj));
                }}
        ));
        symbolTable.add("SysStrNeq", FuncType.getFuncType(
                "SysStrNeq",
                BoolType.boolObj,
                new ArrayList<Symbol>() {{
                    add(new Symbol("lhs", StringType.stringObj));
                    add(new Symbol("rhs", StringType.stringObj));
                }}
        ));
    }
}