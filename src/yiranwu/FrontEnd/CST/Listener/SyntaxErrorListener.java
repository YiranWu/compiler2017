package yiranwu.FrontEnd.CST.Listener;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import yiranwu.Utility.CE;

/**
 * Created by wyr on 17-5-23.
 */
public class SyntaxErrorListener extends BaseErrorListener {
    @Override
    public void syntaxError(Recognizer<?,?> recognizer,Object symbol,int r,int c,String msg,RecognitionException e) {
        BaseListener.r=r;
        BaseListener.c=c;
        throw new CE(msg);
    }
}
