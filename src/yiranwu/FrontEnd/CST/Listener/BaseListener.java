package yiranwu.FrontEnd.CST.Listener;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import yiranwu.FrontEnd.AST.ASTNode;
import yiranwu.FrontEnd.CST.Parser.MxstarBaseListener;

/**
 * Created by wyr on 17-5-21.
 */
public class BaseListener extends MxstarBaseListener {
    public static int r,c;
    static ParseTreeProperty<ASTNode> map=new ParseTreeProperty<>();

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        r=ctx.getStart().getLine();
        c=ctx.getStart().getCharPositionInLine();
    }
}
