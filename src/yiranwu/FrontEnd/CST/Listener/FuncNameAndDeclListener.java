package yiranwu.FrontEnd.CST.Listener;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import yiranwu.FrontEnd.AST.Type.ArrayType;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;
import yiranwu.FrontEnd.AST.Type.Basic.VoidType;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.FrontEnd.CST.Parser.MxstarParser;
import yiranwu.Tables.GlobalTable;
import yiranwu.Tables.Symbol;
import yiranwu.Utility.CE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class FuncNameAndDeclListener extends BaseListener {
    //funcDef : typeSpec Identifier? '(' (typeSpec Identifier (',' typeSpec Identifier)*)? ')' 
    // blkStmt ;
    @Override
    public void exitFuncDef(MxstarParser.FuncDefContext ctx) {
        String name;
        Type type=VoidType.getType();
        ClassType classType=GlobalTable.getClassScope();
        boolean isConstructor=(ctx.Identifier().size()!=ctx.typeSpec().size());
        
        if (!isConstructor) {
            name=ctx.Identifier(0).getText();
            type=(Type)map.get(ctx.typeSpec(0));
            if(name.equals("this"))
                throw new CE("this is reserved");
        }
        else {
            if (classType==null)
                throw new CE("constructor without class");
            name=ctx.typeSpec(0).getText();
            if (!name.equals(classType.name))
                throw new CE("illegal constructor name");
        }
        List<Symbol> paramList=new ArrayList<Symbol>() {{
            if (classType!=null)
                add(new Symbol("this",classType));
            for (int i=1;i<ctx.typeSpec().size();++i) {
                String paramName=ctx.Identifier(i-(isConstructor?1:0)).getText();
                Type paramType=(Type)map.get(ctx.typeSpec(i));
                add(new Symbol(paramName, paramType));
            }
        }};
        FuncType funcType=FuncType.getFuncType(name,type,paramList);
        if (classType!=null) {
            if (!isConstructor)
                classType.addMem(name,funcType);
            else
                classType.addConstructor(funcType);
        }
        else
            GlobalTable.symbolTable.add(name,funcType);
        GlobalTable.prog.addFuncType(funcType);
        map.put(ctx,funcType);
    }

    //classDef : 'class' Identifier '{' (funcDef | simpDeclStmt)* '}' ;
    @Override
    public void enterClassDef(MxstarParser.ClassDefContext ctx) {
        ClassType classType = (ClassType)map.get(ctx);
        //	enter class scope
        GlobalTable.enterScope(classType);
    }

    @Override
    public void exitClassDef(MxstarParser.ClassDefContext ctx) {
        ClassType classType = (ClassType)map.get(ctx);
        ctx.simpDeclStmt().forEach(stmt -> {
            String memName=stmt.Identifier().getText();
            Type memType=(Type)map.get(stmt.typeSpec());
            classType.addMem(memName, memType);
        });
        GlobalTable.exitScope();
    }

    //simpTypeSpec
	//:	typespec='bool'
    //        |	typespec='int'
    //        |	typespec='void'
    //        |	typespec='string'
    //;
    //typeSpec
    //:   simpTypeSpec        #SimpType//done
    @Override
    public void exitSimpTypeSpec(MxstarParser.SimpTypeSpecContext ctx) {
        switch(ctx.typespec.getType()) {
            case MxstarParser.Bool : map.put(ctx, BoolType.boolObj); break;
            case MxstarParser.Int : map.put(ctx, IntType.intObj); break;
            case MxstarParser.Void : map.put(ctx, VoidType.voidObj); break;
            case MxstarParser.String : map.put(ctx, StringType.stringObj); break;
        }
    }
    @Override
    public void exitSimpType(MxstarParser.SimpTypeContext ctx) {
        map.put(ctx,map.get(ctx.simpTypeSpec()));
    }

    //typeSpec
    //|   typeSpec '['']'      #ArrayType//done
	//|	Identifier          #ClassType
    //;

    @Override
    public void exitClassType(MxstarParser.ClassTypeContext ctx) {
        String className=ctx.getText();
        if (!GlobalTable.classTable.contains(className))
            throw new CE("No class named "+className);
        map.put(ctx,GlobalTable.classTable.get(className));
    }

    @Override
    public void exitArrayType(MxstarParser.ArrayTypeContext ctx) {
        Type baseType=(Type)map.get(ctx.typeSpec());
        map.put(ctx,ArrayType.getEnclose(baseType));
    }
}
