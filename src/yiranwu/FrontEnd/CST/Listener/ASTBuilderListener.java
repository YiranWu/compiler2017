package yiranwu.FrontEnd.CST.Listener;

import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import yiranwu.FrontEnd.AST.Expr.*;
import yiranwu.FrontEnd.AST.Expr.BinaryExpr.*;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.BoolConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.IntConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.NullConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.StringConstant;
import yiranwu.FrontEnd.AST.Expr.UnaryExpr.*;
import yiranwu.FrontEnd.AST.Stmt.*;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.Class.MemObj;
import yiranwu.FrontEnd.AST.Type.Class.MemVar;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.FrontEnd.CST.Parser.MxstarParser;
import yiranwu.Tables.GlobalTable;
import yiranwu.Tables.Symbol;
import yiranwu.Utility.CE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class ASTBuilderListener extends BaseListener {
    @Override
    public void exitProg(MxstarParser.ProgContext ctx) {
        ctx.simpDeclStmt().forEach(stmt -> {
            SimpDeclStmt decl=(SimpDeclStmt)map.get(stmt);
            GlobalTable.prog.addGlobalVar(decl);
        });
    }

    @Override
    public void enterFuncDef(MxstarParser.FuncDefContext ctx) {
        FuncType funcType=(FuncType)map.get(ctx);
        GlobalTable.enterScope(funcType);
    }

    @Override
    public void exitFuncDef(MxstarParser.FuncDefContext ctx) {
        FuncType funcType=(FuncType)map.get(ctx);
        funcType.addStmt((BlkStmt)map.get(ctx.blkStmt()));
        GlobalTable.exitScope();
    }

    @Override
    public void enterClassDef(MxstarParser.ClassDefContext ctx) {
        ClassType classType=(ClassType)map.get(ctx);
        GlobalTable.enterScope(classType);
        classType.memVarMap.forEach((name,member) ->
                GlobalTable.symbolTable.add(name,member.type));
        classType.memFuncMap.forEach((name,member) ->
                GlobalTable.symbolTable.add(name, member.funcType));
    }

    @Override
    public void exitClassDef(MxstarParser.ClassDefContext ctx) {
        GlobalTable.exitScope();
    }

    @Override
    public void enterStmt(MxstarParser.StmtContext ctx) {
        if(ctx.parent instanceof MxstarParser.IfStmtContext) GlobalTable.enterScope(null);
    }
    
    @Override
    public void exitStmt(MxstarParser.StmtContext ctx) {
        if(ctx.parent instanceof MxstarParser.IfStmtContext) GlobalTable.exitScope();
        map.put(ctx, map.get(ctx.getChild(0)));
    }

    @Override
    public void enterBlkStmt(MxstarParser.BlkStmtContext ctx) {
        BlkStmt blkStmt=(BlkStmt)BlkStmt.getStmt();
        GlobalTable.enterScope(blkStmt);
        if (ctx.parent instanceof MxstarParser.FuncDefContext) {
            FuncType funcType=(FuncType)map.get(ctx.parent);
            for (int i=0;i<funcType.paramList.size();++i) {
                Symbol param=funcType.paramList.get(i);
                funcType.paramList.set(i,
                        GlobalTable.symbolTable.addParamVar(param.name,param.type));
            }
        }
        map.put(ctx,blkStmt);
    }

    @Override
    public void exitBlkStmt(MxstarParser.BlkStmtContext ctx) {
        ctx.stmt().forEach(stmt ->
            ((BlkStmt)map.get(ctx)).addStmt( (Stmt)map.get(stmt) )
        );
        GlobalTable.exitScope();
    }

    @Override
    public void exitExprStmt(MxstarParser.ExprStmtContext ctx) {
        map.put(ctx,ExprStmt.getStmt(  (Expr)map.get(ctx.expression())  ));
    }

    @Override
    public void exitIfStmt(MxstarParser.IfStmtContext ctx) {
        map.put(ctx, IfStmt.getStmt(
                (Expr)map.get(ctx.expression()),
                (Stmt)map.get(ctx.stmt(0)),
                (Stmt)map.get(ctx.stmt(1))
        ));
    }

    @Override
    public void enterWhileStmt(MxstarParser.WhileStmtContext ctx) {
        WhileStmt whileStmt=(WhileStmt)WhileStmt.getStmt();
        GlobalTable.enterScope(whileStmt);
        map.put(ctx,whileStmt);
    }

    @Override
    public void exitWhileStmt(MxstarParser.WhileStmtContext ctx) {
        WhileStmt whileStmt=(WhileStmt)map.get(ctx);
        whileStmt.addCond((Expr)map.get(ctx.expression()));
        whileStmt.addStmt((Stmt)map.get(ctx.stmt()));
        GlobalTable.exitScope();
    }

    @Override
    public void enterForStmt(MxstarParser.ForStmtContext ctx) {
        ForStmt forStmt=(ForStmt)ForStmt.getStmt();
        GlobalTable.enterScope(forStmt);
        map.put(ctx,forStmt);
    }

    @Override
    public void exitForStmt(MxstarParser.ForStmtContext ctx) {
        ForStmt forStmt=(ForStmt)map.get(ctx);
        forStmt.addStmt(((Stmt)map.get(ctx.stmt())));
        forStmt.addCond((Expr)map.get(ctx.cond));
        forStmt.addInit((Expr)map.get(ctx.init));
        forStmt.addStep((Expr)map.get(ctx.step));
        GlobalTable.exitScope();
    }

    @Override
    public void exitContinueStmt(MxstarParser.ContinueStmtContext ctx) {
        map.put(ctx,ContinueStmt.getStmt());
    }

    @Override
    public void exitBreakStmt(MxstarParser.BreakStmtContext ctx) {
        map.put(ctx,BreakStmt.getStmt());
    }

    @Override
    public void exitRetStmt(MxstarParser.RetStmtContext ctx) {
        Expr expr=(Expr)map.get(ctx.expression());
        map.put(ctx,RetStmt.getStmt(expr));
    }

    @Override
    public void exitSimpDeclStmt(MxstarParser.SimpDeclStmtContext ctx) {
        if(ctx.Identifier().getText().equals("this"))
            throw new CE("this is reserved");
        if (!(ctx.parent instanceof MxstarParser.ClassDefContext)) {
            Type type=(Type)map.get(ctx.typeSpec());
            String name=ctx.Identifier().getText();
            Symbol symbol;
            if (GlobalTable.getScope()==GlobalTable.prog)
                symbol=GlobalTable.symbolTable.addGlobalVar(name,type);
            else
                symbol=GlobalTable.symbolTable.addTempVar(name,type);
            Expr expr=(Expr)map.get(ctx.expression());
            map.put(ctx, SimpDeclStmt.getStmt(symbol, expr));
        }
    }

    @Override
    public void exitConstExpr(MxstarParser.ConstExprContext ctx) {
        map.put(ctx,map.get(ctx.constant()));
    }

    @Override
    public void exitVarExpr(MxstarParser.VarExprContext ctx) {
        map.put(ctx,IdExpr.getExpr(ctx.getText()));
    }

    @Override
    public void exitMemberExpr (MxstarParser.MemberExprContext ctx) {
        map.put(ctx, MemberExpr.getExpr(
                (Expr)map.get(ctx.expression()),
                ctx.Identifier().getText()
        ));
    }

    @Override
    public void exitArrayExpr(MxstarParser.ArrayExprContext ctx) {
        map.put(ctx, ArrayExpr.getExpr(
                (Expr)map.get(ctx.expression(0)),
                (Expr)map.get(ctx.expression(1))
        ));
    }

    @Override
    public void exitParenExpr(MxstarParser.ParenExprContext ctx) {
        map.put(ctx, map.get(ctx.expression()));
    }

    @Override
    public void exitPostfix(MxstarParser.PostfixContext ctx) {
        Expr expression=(Expr)map.get(ctx.expression());
        if (ctx.op.getText().equals("++"))
            map.put(ctx, PostIncExpr.getExpr(expression));
        else if (ctx.op.getText().equals("--"))
            map.put(ctx, PostDecExpr.getExpr(expression));
        else throw new InternalError();
    }

    @Override
    public void exitUnary(MxstarParser.UnaryContext ctx) {
        Expr expression = (Expr)map.get(ctx.expression());
        //if (ctx.op.getText().equals("+")) {
        //    map.put(ctx, UnaryPlusExpr.getExpr(expression));
        //} else if (ctx.operator.getText().equals("-")) {
        if (ctx.op.getText().equals("-"))
            map.put(ctx, NegativeExpr.getExpr(expression));
        else if (ctx.op.getText().equals("!"))
            map.put(ctx, LogicNotExpr.getExpr(expression));
        else if (ctx.op.getText().equals("~"))
            map.put(ctx, BitNotExpr.getExpr(expression));
        else if (ctx.op.getText().equals("++"))
            map.put(ctx, PreIncExpr.getExpr(expression));
        else if (ctx.op.getText().equals("--"))
            map.put(ctx, PreDecExpr.getExpr(expression));
        else throw new InternalError();
    }

    @Override
    public void exitFuncCall(MxstarParser.FuncCallContext ctx) {
        Expr funcType=(Expr)map.get(ctx.expression(0));
        List<Expr> paramList=new ArrayList<>();
        for (int i=1;i<ctx.expression().size();++i) {
            Expr param=(Expr)map.get(ctx.expression(i));
            paramList.add(param);
        }
        map.put(ctx,CallExpr.getExpr(funcType, paramList));
    }

    @Override
    public void exitExceptionNew(MxstarParser.ExceptionNewContext ctx) {
        throw new CE("Illegal new syntax");
    }

    @Override
    public void exitSimpNew(MxstarParser.SimpNewContext ctx) {
        Type baseType=(Type)map.get(ctx.typeSpec());
        map.put(ctx,NewExpr.getExpr(baseType,new ArrayList<>()));
    }

    @Override
    public void exitArrayNew(MxstarParser.ArrayNewContext ctx) {
        List<Expr> dimList=new ArrayList<>();
        ctx.expression().forEach(expr -> {
            Expr dim=(Expr)map.get(expr);
            dimList.add(dim);
        });
        int nullNum=ctx.getTokens(MxstarParser.LeftBrack).size()-ctx.expression().size();
        for(int i=1;i<=nullNum;++i) dimList.add(null);
        Type baseType=(Type)map.get(ctx.typeSpec());
        map.put(ctx,NewExpr.getExpr(baseType,dimList));
    }

    @Override
    public void exitNew(MxstarParser.NewContext ctx) {
        map.put(ctx,map.get(ctx.newTypeSpec()));
    }

    @Override
    public void exitMulLevel(MxstarParser.MulLevelContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        if (ctx.op.getText().equals("*"))
            map.put(ctx,MulExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals("/"))
            map.put(ctx,DivExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals("%"))
            map.put(ctx,ModExpr.getExpr(lhs,rhs));
        else throw new InternalError();
    }

    @Override
    public void exitAddLevel(MxstarParser.AddLevelContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        if (ctx.op.getText().equals("+"))
            map.put(ctx,AddExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals("-"))
            map.put(ctx,SubExpr.getExpr(lhs,rhs));
        else throw new InternalError();
    }

    @Override
    public void exitShiftLevel(MxstarParser.ShiftLevelContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        if (ctx.op.getText().equals("<<"))
            map.put(ctx,BitLShiftExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals(">>"))
            map.put(ctx,BitRShiftExpr.getExpr(lhs,rhs));
        else throw new InternalError();
    }

    @Override
    public void exitCmpLevel(MxstarParser.CmpLevelContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        if (ctx.op.getText().equals("<"))
            map.put(ctx,LessExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals(">"))
            map.put(ctx,GreatExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals("<="))
            map.put(ctx,LessEqualExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals(">="))
            map.put(ctx,GreatEqualExpr.getExpr(lhs,rhs));
        else throw new InternalError();
    }

    @Override
    public void exitEqualLevel(MxstarParser.EqualLevelContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        if (ctx.op.getText().equals("=="))
            map.put(ctx, EqualExpr.getExpr(lhs,rhs));
        else if (ctx.op.getText().equals("!="))
            map.put(ctx, NotEqualExpr.getExpr(lhs,rhs));
        else throw new InternalError();
    }

    @Override
    public void exitAndExpr(MxstarParser.AndExprContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        map.put(ctx,BitAndExpr.getExpr(lhs,rhs));
    }

    @Override
    public void exitXorExpr(MxstarParser.XorExprContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        map.put(ctx,BitXorExpr.getExpr(lhs,rhs));
    }

    @Override
    public void exitOrExpr(MxstarParser.OrExprContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        map.put(ctx,BitOrExpr.getExpr(lhs,rhs));
    }

    @Override
    public void exitLogicAndExpr(MxstarParser.LogicAndExprContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        map.put(ctx,LogicAndExpr.getExpr(lhs,rhs));
    }

    @Override
    public void exitLogicOrExpr(MxstarParser.LogicOrExprContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        map.put(ctx,LogicOrExpr.getExpr(lhs,rhs));
    }

    @Override
    public void exitAssignExpr(MxstarParser.AssignExprContext ctx) {
        Expr lhs=(Expr)map.get(ctx.expression(0));
        Expr rhs=(Expr)map.get(ctx.expression(1));
        map.put(ctx,AssignExpr.getExpr(lhs,rhs));
    }

    @Override
    public void exitConstant(MxstarParser.ConstantContext ctx) {
        String content=ctx.type.getText();
        switch(ctx.type.getType()) {
            case MxstarParser.StrLiteral :
                map.put(ctx, StringConstant.getConstant(content.substring(1,content.length()-1))); break;
            case MxstarParser.IntConstant :
                map.put(ctx, IntConstant.getConstant(Integer.valueOf(content))); break;
            case MxstarParser.BoolConstant :
                map.put(ctx, BoolConstant.getConstant(content.equals("true"))); break;
            case MxstarParser.NullConstant :
                map.put(ctx, NullConstant.getConstant() ); break;
        }
    }
}
