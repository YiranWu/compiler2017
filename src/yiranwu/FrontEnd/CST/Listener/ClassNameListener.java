package yiranwu.FrontEnd.CST.Listener;

import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.CST.Parser.MxstarParser;
import yiranwu.Tables.GlobalTable;

/**
 * Created by wyr on 17-5-21.
 */
public class ClassNameListener extends BaseListener {
    @Override
    public void exitClassDef(MxstarParser.ClassDefContext ctx) {
        String name=ctx.Identifier().getText();
        ClassType classType=(ClassType)ClassType.getType(name);
        GlobalTable.classTable.put(name,classType);
        GlobalTable.prog.addClassType(classType);
        map.put(ctx,classType);
    }
}
