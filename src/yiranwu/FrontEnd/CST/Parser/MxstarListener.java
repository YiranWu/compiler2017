// Generated from /home/wyr/compiler_project/compiler2017-2.0/src/yiranwu/FrontEnd/CST/Parser/Mxstar.g4 by ANTLR 4.6
package yiranwu.FrontEnd.CST.Parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MxstarParser}.
 */
public interface MxstarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MxstarParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(MxstarParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(MxstarParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Simp}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterSimp(MxstarParser.SimpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Simp}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitSimp(MxstarParser.SimpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterFunc(MxstarParser.FuncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitFunc(MxstarParser.FuncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Class}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterClass(MxstarParser.ClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Class}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitClass(MxstarParser.ClassContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#simpDeclStmt}.
	 * @param ctx the parse tree
	 */
	void enterSimpDeclStmt(MxstarParser.SimpDeclStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#simpDeclStmt}.
	 * @param ctx the parse tree
	 */
	void exitSimpDeclStmt(MxstarParser.SimpDeclStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArrayType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void enterArrayType(MxstarParser.ArrayTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArrayType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void exitArrayType(MxstarParser.ArrayTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SimpType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void enterSimpType(MxstarParser.SimpTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SimpType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void exitSimpType(MxstarParser.SimpTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ClassType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void enterClassType(MxstarParser.ClassTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ClassType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void exitClassType(MxstarParser.ClassTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#simpTypeSpec}.
	 * @param ctx the parse tree
	 */
	void enterSimpTypeSpec(MxstarParser.SimpTypeSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#simpTypeSpec}.
	 * @param ctx the parse tree
	 */
	void exitSimpTypeSpec(MxstarParser.SimpTypeSpecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExceptionNew}
	 * labeled alternative in {@link MxstarParser#newTypeSpec}.
	 * @param ctx the parse tree
	 */
	void enterExceptionNew(MxstarParser.ExceptionNewContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExceptionNew}
	 * labeled alternative in {@link MxstarParser#newTypeSpec}.
	 * @param ctx the parse tree
	 */
	void exitExceptionNew(MxstarParser.ExceptionNewContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArrayNew}
	 * labeled alternative in {@link MxstarParser#newTypeSpec}.
	 * @param ctx the parse tree
	 */
	void enterArrayNew(MxstarParser.ArrayNewContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArrayNew}
	 * labeled alternative in {@link MxstarParser#newTypeSpec}.
	 * @param ctx the parse tree
	 */
	void exitArrayNew(MxstarParser.ArrayNewContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SimpNew}
	 * labeled alternative in {@link MxstarParser#newTypeSpec}.
	 * @param ctx the parse tree
	 */
	void enterSimpNew(MxstarParser.SimpNewContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SimpNew}
	 * labeled alternative in {@link MxstarParser#newTypeSpec}.
	 * @param ctx the parse tree
	 */
	void exitSimpNew(MxstarParser.SimpNewContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#funcDef}.
	 * @param ctx the parse tree
	 */
	void enterFuncDef(MxstarParser.FuncDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#funcDef}.
	 * @param ctx the parse tree
	 */
	void exitFuncDef(MxstarParser.FuncDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#classDef}.
	 * @param ctx the parse tree
	 */
	void enterClassDef(MxstarParser.ClassDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#classDef}.
	 * @param ctx the parse tree
	 */
	void exitClassDef(MxstarParser.ClassDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(MxstarParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(MxstarParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#exprStmt}.
	 * @param ctx the parse tree
	 */
	void enterExprStmt(MxstarParser.ExprStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#exprStmt}.
	 * @param ctx the parse tree
	 */
	void exitExprStmt(MxstarParser.ExprStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#blkStmt}.
	 * @param ctx the parse tree
	 */
	void enterBlkStmt(MxstarParser.BlkStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#blkStmt}.
	 * @param ctx the parse tree
	 */
	void exitBlkStmt(MxstarParser.BlkStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void enterIfStmt(MxstarParser.IfStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void exitIfStmt(MxstarParser.IfStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ForStmt}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void enterForStmt(MxstarParser.ForStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ForStmt}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void exitForStmt(MxstarParser.ForStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code WhileStmt}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void enterWhileStmt(MxstarParser.WhileStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code WhileStmt}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void exitWhileStmt(MxstarParser.WhileStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BreakStmt}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void enterBreakStmt(MxstarParser.BreakStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BreakStmt}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void exitBreakStmt(MxstarParser.BreakStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ContinueStmt}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void enterContinueStmt(MxstarParser.ContinueStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ContinueStmt}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void exitContinueStmt(MxstarParser.ContinueStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RetStmt}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void enterRetStmt(MxstarParser.RetStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RetStmt}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void exitRetStmt(MxstarParser.RetStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code EqualLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqualLevel(MxstarParser.EqualLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EqualLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqualLevel(MxstarParser.EqualLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AndExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpr(MxstarParser.AndExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AndExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpr(MxstarParser.AndExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code New}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNew(MxstarParser.NewContext ctx);
	/**
	 * Exit a parse tree produced by the {@code New}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNew(MxstarParser.NewContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ConstExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConstExpr(MxstarParser.ConstExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ConstExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConstExpr(MxstarParser.ConstExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code XorExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterXorExpr(MxstarParser.XorExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code XorExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitXorExpr(MxstarParser.XorExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ShiftLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterShiftLevel(MxstarParser.ShiftLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ShiftLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitShiftLevel(MxstarParser.ShiftLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicAndExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicAndExpr(MxstarParser.LogicAndExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicAndExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicAndExpr(MxstarParser.LogicAndExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CmpLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCmpLevel(MxstarParser.CmpLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CmpLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCmpLevel(MxstarParser.CmpLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary(MxstarParser.UnaryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary(MxstarParser.UnaryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code OrExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpr(MxstarParser.OrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OrExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpr(MxstarParser.OrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AssignExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignExpr(MxstarParser.AssignExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AssignExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignExpr(MxstarParser.AssignExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FuncCall}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFuncCall(MxstarParser.FuncCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FuncCall}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFuncCall(MxstarParser.FuncCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Postfix}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfix(MxstarParser.PostfixContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Postfix}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfix(MxstarParser.PostfixContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArrayExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpr(MxstarParser.ArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArrayExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpr(MxstarParser.ArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MemberExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMemberExpr(MxstarParser.MemberExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MemberExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMemberExpr(MxstarParser.MemberExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicOrExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicOrExpr(MxstarParser.LogicOrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicOrExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicOrExpr(MxstarParser.LogicOrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code VarExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVarExpr(MxstarParser.VarExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VarExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVarExpr(MxstarParser.VarExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParenExpr(MxstarParser.ParenExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParenExpr(MxstarParser.ParenExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MulLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMulLevel(MxstarParser.MulLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MulLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMulLevel(MxstarParser.MulLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AddLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddLevel(MxstarParser.AddLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AddLevel}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddLevel(MxstarParser.AddLevelContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(MxstarParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(MxstarParser.ConstantContext ctx);
}