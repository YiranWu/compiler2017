package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.Tables.Scope;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class IterStmt extends Stmt implements Scope {
    public LabelInst iter,exit;
}
