package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.ControlInst.BranchInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.BoolConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.Tables.Scope;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class IfStmt extends Stmt implements Scope {
    public Expr cond;
    public Stmt thenStmt, elseStmt;

    public IfStmt(Expr cond, Stmt thenStmt, Stmt elseStmt) {
        this.cond = cond;
        this.thenStmt = thenStmt;
        this.elseStmt = elseStmt;
    }

    public static Stmt getStmt(Expr cond, Stmt thenStmt, Stmt elseStmt) {
        if (!(cond.type instanceof BoolType))
            throw new CE("IfStmt:non-bool cond");
        if (cond instanceof BoolConstant) {
            if (((BoolConstant)cond).val) return thenStmt;
            else return elseStmt;
        }
        return new IfStmt(cond, thenStmt, elseStmt);
    }

    public void emit(List<Inst> instList) {
        LabelInst thenLabel=LabelInst.getInst("if_then");
        LabelInst elseLabel=LabelInst.getInst("if_else");
        LabelInst mergeLabel=LabelInst.getInst("if_merge");
        cond.emit(instList);
        cond.load(instList);
        instList.add(BranchInst.getInst(cond.operand, thenLabel, elseLabel));
        instList.add(thenLabel);
        if (thenStmt != null) thenStmt.emit(instList);
        instList.add(JumpInst.getInst(mergeLabel));
        instList.add(elseLabel);
        if (elseStmt != null) elseStmt.emit(instList);
        instList.add(JumpInst.getInst(mergeLabel));
        instList.add(mergeLabel);
    }
}
