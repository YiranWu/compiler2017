package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.FuncInst.RetInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.NullConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.NullType;
import yiranwu.FrontEnd.AST.Type.Basic.VoidType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;
import yiranwu.Utility.Utility;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class RetStmt extends Stmt {
    public Expr expr;
    public FuncType belong;

    public RetStmt(Expr expr, FuncType belong) {
        this.expr=expr;
        this.belong=belong;
    }

    public static Stmt getStmt(Expr expr) {
        FuncType funcType=GlobalTable.getFuncScope();
        if (funcType==null)
            throw new CE("RetStmt:not in func");
        if (expr==null)
            if (funcType.type instanceof VoidType) return new RetStmt(expr, funcType);
        if (expr!=null)
            if (funcType.type.isSameType(expr.type)) return new RetStmt(expr,funcType);
        throw new CE("return type mismatch");
    }

    public void emit(List<Inst> instList) {
        if (expr != null) {
            expr.emit(instList);
            expr.load(instList);
            instList.add(RetInst.getInst(expr.operand));
        }
        instList.add(JumpInst.getInst(belong.exit));
    }
}
