package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.ArrayType;
import yiranwu.FrontEnd.AST.Type.Basic.VoidType;
import yiranwu.Tables.Symbol;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class SimpDeclStmt extends Stmt {
    public Symbol symbol;
    public Expr expr;

    public SimpDeclStmt(Symbol symbol,Expr expr) {
        this.symbol = symbol;
        this.expr = expr;
    }

    public static Stmt getStmt(Symbol symbol,Expr expr) {
        if (symbol.type instanceof VoidType) throw new CE("Declaring void");
        if (expr!=null && !symbol.type.isSameType(expr.type)) {
            //System.err.println(((ArrayType)expr.type).dim);
            throw new CE("SimpDecl:Type mismatch");
        }
        return new SimpDeclStmt(symbol,expr);
    }

    public void emit(List<Inst> instList) {
        if (expr != null) {
            expr.emit(instList);
            expr.load(instList);
            instList.add(MoveInst.getInst(symbol.reg,expr.operand));
        }
    }
}
