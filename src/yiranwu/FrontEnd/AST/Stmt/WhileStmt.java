package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.ControlInst.BranchInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.Utility.CE;
import yiranwu.Utility.Utility;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class WhileStmt extends IterStmt {
    public Expr cond;
    public Stmt stmt;

    public static Stmt getStmt() {return new WhileStmt();}

    public void addCond(Expr cond) {
        if (!(cond.type instanceof BoolType)) throw new CE("WhileStmt:non-bool cond");
        this.cond = cond;
    }

    public void addStmt(Stmt stmt) {this.stmt=stmt;}

    @Override
    public void emit(List<Inst> instList) {
        LabelInst bodyLabel = LabelInst.getInst("while_body");
        iter=LabelInst.getInst("while_iter");
        exit=LabelInst.getInst("while_exit");
        instList.add(JumpInst.getInst(iter));
        instList.add(iter);
        cond.emit(instList);
        instList.add(BranchInst.getInst(cond.operand,bodyLabel,exit));
        instList.add(bodyLabel);
        stmt.emit(instList);
        instList.add(JumpInst.getInst(iter));
        instList.add(exit);
    }
}
