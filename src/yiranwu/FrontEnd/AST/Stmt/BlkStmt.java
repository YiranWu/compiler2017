package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.ASTNode;
import yiranwu.Tables.Scope;
import yiranwu.Utility.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class BlkStmt extends Stmt implements Scope {
    public ArrayList<Stmt> stmtList;

    public BlkStmt() {stmtList=new ArrayList<>();}

    public static Stmt getStmt() {return new BlkStmt();}

    public void addStmt(Stmt Stmt) {stmtList.add(Stmt);}

    @Override
    public void emit(List<Inst> instList) {
        stmtList.forEach(stmt -> stmt.emit(instList));
    }
}
