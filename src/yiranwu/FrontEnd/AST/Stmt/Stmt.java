package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.ASTNode;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class Stmt implements ASTNode {
    public abstract void emit(List<Inst> instList);
}
