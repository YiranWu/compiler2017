package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.CST.Parser.MxstarParser;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;
import yiranwu.Utility.Utility;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class BreakStmt extends Stmt {
    public IterStmt belong;

    public BreakStmt(IterStmt belong) {
        this.belong=belong;
    }

    public static Stmt getStmt() {
        if (GlobalTable.getIterScope()==null)
            throw new CE("Break with no iter");
        return new BreakStmt(GlobalTable.getIterScope());
    }

    public void emit(List<Inst> instList) {
        instList.add(JumpInst.getInst(belong.exit));
    }
}
