package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Expr.Expr;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class ExprStmt extends Stmt {
    public Expr expr;

    public ExprStmt(Expr expr) {this.expr=expr;}

    public static Stmt getStmt(Expr expr) {return new ExprStmt(expr);}

    public void emit(List<Inst> instList) {
        if (expr != null) expr.emit(instList);
    }
}
