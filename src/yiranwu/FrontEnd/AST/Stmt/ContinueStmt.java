package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class ContinueStmt extends Stmt {
    public IterStmt belong;

    public ContinueStmt(IterStmt belong) {
        this.belong=belong;
    }

    public static Stmt getStmt() {
        if (GlobalTable.getIterScope()==null)
            throw new CE("Continue with no iter");
        return new ContinueStmt(GlobalTable.getIterScope());
    }

    public void emit(List<Inst> instList) {
        instList.add(JumpInst.getInst(belong.iter));
    }
}
