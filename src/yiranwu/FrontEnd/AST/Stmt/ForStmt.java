package yiranwu.FrontEnd.AST.Stmt;

import yiranwu.BackEnd.CFG.Inst.ControlInst.BranchInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.BoolConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class ForStmt extends IterStmt {
    public Expr init, cond, step;
    public Stmt stmt;

    public static Stmt getStmt() {return new ForStmt();}

    public void addInit(Expr init) {this.init = init;}

    public void addCond(Expr cond) {
        if (cond == null) {
            this.cond= BoolConstant.getConstant(true);
            return;
        }
        if (cond.type instanceof BoolType) {
            this.cond=cond;
            return;
        }
        throw new CE("ForStmt:non-bool cond");
    }

    public void addStep(Expr step) {this.step = step;}

    public void addStmt(Stmt Stmt) {this.stmt = Stmt;}

    public void emit(List<Inst> instList) {
        LabelInst condLabel=LabelInst.getInst("for_cond");
        LabelInst bodyLabel=LabelInst.getInst("for_body");
        iter=LabelInst.getInst("for_iter");
        exit=LabelInst.getInst("for_exit");
        if (init!=null) init.emit(instList);
        instList.add(JumpInst.getInst(condLabel));
        instList.add(condLabel);
        if (cond==null) addCond(null);
        cond.emit(instList);
        instList.add(BranchInst.getInst(cond.operand,bodyLabel,exit));
        instList.add(bodyLabel);
        if (stmt != null) stmt.emit(instList);
        instList.add(JumpInst.getInst(iter));
        instList.add(iter);
        if (step != null) step.emit(instList);
        instList.add(JumpInst.getInst(condLabel));
        instList.add(exit);
    }
}
