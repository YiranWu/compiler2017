package yiranwu.FrontEnd.AST.Type;

import yiranwu.FrontEnd.AST.ASTNode;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class Type implements ASTNode {
    public int size() {return 8;}

    public abstract boolean isSameType(Type type);
}
