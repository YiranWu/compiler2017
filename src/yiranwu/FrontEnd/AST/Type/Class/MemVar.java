package yiranwu.FrontEnd.AST.Type.Class;

import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Utility.Utility;

/**
 * Created by wyr on 17-5-21.
 */
public class MemVar extends MemObj {
    public Type type;
    public int offset;
    public Expr expr;

    public MemVar(ClassType belong, String name, Type type) {
        super(name);
        this.type=type;
        this.offset=belong.nowSize;
        belong.nowSize+=Utility.getAligned(type.size());
    }
}
