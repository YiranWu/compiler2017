package yiranwu.FrontEnd.AST.Type.Class;

import yiranwu.FrontEnd.AST.Type.FuncType;

/**
 * Created by wyr on 17-5-21.
 */
public class MemFunc extends MemObj {
    public FuncType funcType;

    public MemFunc(String name,FuncType funcType) {
        super(name);
        this.funcType=funcType;
    }
}
