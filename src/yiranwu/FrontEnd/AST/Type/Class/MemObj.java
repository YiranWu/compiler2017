package yiranwu.FrontEnd.AST.Type.Class;

import yiranwu.Tables.GlobalTable;

/**
 * Created by wyr on 17-5-21.
 */
public class MemObj {
    public String name;
    public ClassType belong;

    MemObj(String name) {
        this.name=name;
        this.belong= GlobalTable.getClassScope();
    }
}
