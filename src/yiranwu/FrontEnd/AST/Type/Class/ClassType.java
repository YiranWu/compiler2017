package yiranwu.FrontEnd.AST.Type.Class;

import yiranwu.FrontEnd.AST.Type.Basic.NullType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.Scope;
import yiranwu.Utility.CE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wyr on 17-5-21.
 */
public class ClassType extends Type implements Scope {
    public String name;
    public int nowSize;
    public Map<String, MemVar> memVarMap;
    public Map<String, MemFunc> memFuncMap;
    public FuncType constructor;

    public ClassType(String name) {
        this.name=name;
        this.nowSize=0;
        this.memVarMap=new HashMap<>();
        this.memFuncMap=new HashMap<>();
        this.constructor=null;
    }

    public static Type getType(String name) {return new ClassType(name);}

    public void addMem(String name,Type type) {
        if (memVarMap.containsKey(name) || memFuncMap.containsKey(name))
            throw new CE("Class "+this.name+ ":duplicate member: "+name);
        if (type instanceof FuncType) {
            FuncType funcType=(FuncType)type;
            funcType.name=this.name+"."+funcType.name;
            MemFunc memFunc=new MemFunc(name,funcType);
            memFuncMap.put(name,memFunc);
        }
        else {
            MemVar memVar=new MemVar(this,name,type);
            memVarMap.put(name, memVar);
        }
    }

    public void addConstructor(FuncType funcType) {
        if (constructor!=null)
            throw new CE("Duplicate constructor");
        funcType.name=name+".constructor";
        constructor=funcType;
    }

    public MemObj getMem(String name) {
        if (memVarMap.containsKey(name)) return memVarMap.get(name);
        if (memFuncMap.containsKey(name)) return memFuncMap.get(name);
        throw new CE("Class "+this.name+"has no member named "+name);
    }

    public boolean isSameType(Type other) {
        return other instanceof NullType || other == this;
    }

}
