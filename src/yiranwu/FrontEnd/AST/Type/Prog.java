package yiranwu.FrontEnd.AST.Type;

import yiranwu.FrontEnd.AST.ASTNode;
import yiranwu.FrontEnd.AST.Stmt.SimpDeclStmt;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.Tables.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class Prog implements ASTNode, Scope {
    public List<FuncType> funcList;
    public List<SimpDeclStmt> varList;
    private List<ClassType> classList;

    public Prog() {
        classList = new ArrayList<>();
        funcList = new ArrayList<>();
        varList = new ArrayList<>();
    }

    public static Prog getProg() {return new Prog();}

    public void addClassType(ClassType classType) {classList.add(classType);}

    public void addFuncType(FuncType funcType) {funcList.add(funcType);}

    public void addGlobalVar(SimpDeclStmt var) {varList.add(var);}
}
