package yiranwu.FrontEnd.AST.Type;

import yiranwu.BackEnd.CFG.Allocator.Allocator;
import yiranwu.BackEnd.CFG.Graph;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.FrontEnd.AST.ASTNode;
import yiranwu.FrontEnd.AST.Stmt.BlkStmt;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.Tables.GlobalTable;
import yiranwu.Tables.Scope;
import yiranwu.Tables.Symbol;
import yiranwu.Utility.CE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class FuncType extends Type implements ASTNode, Scope {
    public String name;
    public Type type;
    public List<Symbol> paramList;
    public BlkStmt body;
    public LabelInst enter, entry, exit;
    public Graph graph;
    public Allocator allocator;

    public FuncType(String name,Type type,List<Symbol> paramList) {
        this.name=name;
        this.type=type;
        this.paramList=paramList;
    }

    public static FuncType getFuncType(String name,Type retType,List<Symbol> paramList) {
        if (GlobalTable.getClassScope()==null) {
            if (GlobalTable.symbolTable.contains(name))
                throw new CE("Duplicate function named "+name);
        }
        if (name.equals("main")) {
            if (!(retType instanceof IntType))
                throw new CE("retType of main:non-int type");
            if (paramList.size()!=0)
                throw new CE("param of main:too many args");
        }
        for (int i=0;i<paramList.size();++i) {
            for (int j=i+1;j<paramList.size();++j) {
                if (paramList.get(i).name.equals(paramList.get(j).name))
                    throw new CE("Duplicate funcparam named "+paramList.get(i).name);
            }
        }
        return new FuncType(name,retType,paramList);
    }

    public void addStmt(BlkStmt body) {this.body = body;}///////

    public boolean isSameType(Type type) {return false;}
}
