package yiranwu.FrontEnd.AST.Type.Basic;

import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-21.
 */
public class VoidType extends Type {
    public static VoidType voidObj = new VoidType();
    public static VoidType getType() {return voidObj;}

    public boolean isSameType(Type type) {return type instanceof VoidType;}
}
