package yiranwu.FrontEnd.AST.Type.Basic;

import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.FrontEnd.AST.Type.ArrayType;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;

/**
 * Created by wyr on 17-5-21.
 */
public class NullType extends Type {
    public static NullType nullObj = new NullType();
    public static NullType getType() {return nullObj;}

    public boolean isSameType(Type type) {
        return type instanceof NullType ||
                type instanceof ArrayType ||
                type instanceof StringType;
    }
}
