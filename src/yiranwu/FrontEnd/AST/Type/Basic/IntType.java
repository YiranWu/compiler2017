package yiranwu.FrontEnd.AST.Type.Basic;

import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-21.
 */
public class IntType extends Type {
    public static IntType intObj = new IntType();
    public static IntType getType() {return intObj;}

    public boolean isSameType(Type type) {return type instanceof IntType;}
}
