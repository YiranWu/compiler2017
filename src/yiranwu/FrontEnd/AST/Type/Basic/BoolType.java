package yiranwu.FrontEnd.AST.Type.Basic;

import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-21.
 */
public class BoolType extends Type {
    public static BoolType boolObj = new BoolType();
    public static BoolType getType() {return boolObj;}

    public boolean isSameType(Type type) {return type instanceof BoolType;}
}
