package yiranwu.FrontEnd.AST.Type.Basic;

import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-21.
 */
public class StringType extends Type {
    public static StringType stringObj = new StringType();
    public static StringType getType() {return stringObj;}

    public boolean isSameType(Type type) {return type instanceof StringType;}
}
