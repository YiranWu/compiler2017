package yiranwu.FrontEnd.AST.Type;

import yiranwu.FrontEnd.AST.Type.Basic.NullType;
import yiranwu.FrontEnd.AST.Type.Basic.VoidType;
import yiranwu.Utility.CE;

/**
 * Created by wyr on 17-5-21.
 */
public class ArrayType extends Type {
    public Type baseType;
    public int dim;

    public ArrayType(Type baseType, int dim) {
        this.baseType=baseType;
        this.dim=dim;
    }

    public static Type getType(Type baseType, int dim) {
        if (baseType instanceof VoidType) throw new CE("Array of void");
        if (dim==0) throw new CE("dim 0 Array");
        return new ArrayType(baseType, dim);
    }

    public static Type getEnclose(Type baseType) {
        if (baseType instanceof VoidType) throw new CE("Array of void");
        if (baseType instanceof ArrayType) {
            ArrayType array=(ArrayType)baseType;
            return new ArrayType(array.baseType,array.dim+1);
        }
        else return new ArrayType(baseType, 1);
    }

    public Type unpack() {
        if (dim==1) return baseType;
        else return new ArrayType(baseType,dim-1);
    }

    public boolean isSameType(Type type) {
        if (type instanceof ArrayType) {
            ArrayType arrayType=(ArrayType)type;
            return arrayType.baseType.isSameType(baseType) && arrayType.dim==dim;
        }
        if (type instanceof NullType) return true;
        return false;
    }
}
