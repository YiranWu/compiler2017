package yiranwu.FrontEnd.AST.Expr;

import yiranwu.BackEnd.CFG.Inst.FuncInst.CallInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.FrontEnd.AST.Type.Basic.NullType;
import yiranwu.FrontEnd.AST.Type.Basic.VoidType;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;
import yiranwu.Utility.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class CallExpr extends Expr {
    public FuncType funcType;
    public List<Expr> paramList;

    public CallExpr(Type type,boolean isLeftValue,FuncType funcType,List<Expr> paramList) {
        super(type,isLeftValue);
        this.funcType=funcType;
        this.paramList=paramList;
    }

    public static Expr getExpr(FuncType funcType, List<Expr> paramList) {
        return new CallExpr(funcType.type,false,funcType,paramList);
    }

    public static Expr getExpr(Expr expr,List<Expr> paramList) {
        if (expr instanceof NewExpr) {
            NewExpr newExpr=(NewExpr)expr;
            if (newExpr.type instanceof ClassType) {
                ClassType classType=(ClassType)newExpr.type;
                newExpr.constructor=classType.constructor;
                return newExpr;
            }
        }
        if (expr.type instanceof FuncType) {
            FuncType funcType=(FuncType)expr.type;
            if (expr instanceof MemberExpr)
                paramList.add(0,((MemberExpr)expr).expr);
            if (paramList.size() != funcType.paramList.size())
                throw new CE("CallExpr:Param number mismatch");
            for (int i=0;i<paramList.size();++i) {
                if (i==0 && expr instanceof MemberExpr)
                    continue;
                if (!funcType.paramList.get(i).type.isSameType(paramList.get(i).type)) {
                    throw new CE("CallExpr:Param Type mismatch");
                }
            }
            return new CallExpr(funcType.type,false,funcType,paramList);
        }
        throw new CE("CallExpr:Not a function");
    }
    
    public void emit(List<Inst> instList) {
        List<Operand> operands=new ArrayList<>();
        for (Expr param : paramList) {
            param.emit(instList);
            param.load(instList);
            operands.add(param.operand);
        }
        if (type instanceof VoidType)
            instList.add(CallInst.getInst(null,funcType,operands));
        else {
            operand = GlobalTable.regTable.addTempReg(null);
            instList.add(CallInst.getInst(operand,funcType,operands));
        }
    }
}
