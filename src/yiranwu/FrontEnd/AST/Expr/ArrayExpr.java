package yiranwu.FrontEnd.AST.Expr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.AddInst;
import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.MulInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.LoadInst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Type.ArrayType;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class ArrayExpr extends Expr {
    public Expr expr, array;

    private ArrayExpr(Type type,boolean isLVal,Expr expr,Expr array) {
        super(type,isLVal);
        this.expr=expr;
        this.array=array;
    }

    public static Expr getExpr(Expr expr,Expr array) {
        if (!(expr.type instanceof ArrayType))
            throw new CE("ArraExpr:lhs non-array type");
        if (!(array.type instanceof IntType))
            throw new CE("ArrayExpr:rhs non-int type");
        ArrayType arrayType=(ArrayType)expr.type;
        return new ArrayExpr(arrayType.unpack(),expr.isLVal,expr,array);
    }
    
    public void emit(List<Inst> instList) {
        expr.emit(instList);
        expr.load(instList);
        array.emit(instList);
        array.load(instList);
        VReg address=GlobalTable.regTable.addTempReg(null);
        VReg delta=GlobalTable.regTable.addTempReg(null);
        instList.add(MulInst.getInst(delta,array.operand,new IValue(type.size())));
        instList.add(AddInst.getInst(address,expr.operand,delta));
        operand=new Address(address,type.size());
    }

    @Override
    public void load(List<Inst> instList) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = GlobalTable.regTable.addTempReg(null);
            instList.add(LoadInst.getInst(operand, address));
        }
    }
}
