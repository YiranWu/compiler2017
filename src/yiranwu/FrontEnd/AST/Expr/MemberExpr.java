package yiranwu.FrontEnd.AST.Expr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.LoadInst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Type.ArrayType;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.Class.MemFunc;
import yiranwu.FrontEnd.AST.Type.Class.MemObj;
import yiranwu.FrontEnd.AST.Type.Class.MemVar;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class MemberExpr extends Expr {
    public Expr expr;
    public String member;

    public MemberExpr(Type type,boolean isLVal,Expr expr,String member) {
        super(type,isLVal);
        this.expr=expr;
        this.member=member;
    }

    public static Expr getExpr(Expr expr, String name) {
        if (expr.type instanceof ClassType) {
            ClassType classType=(ClassType)expr.type;
            MemObj member=classType.getMem(name);
            if (member instanceof MemVar)
                return new MemberExpr(((MemVar)member).type,expr.isLVal,expr,name);
            else if (member instanceof MemFunc)
                return new MemberExpr(((MemFunc)member).funcType,expr.isLVal,expr,name);
            throw new InternalError();
        }
        else if (expr.type instanceof ArrayType) {
            if (name.equals("size")) {
                return new MemberExpr(
                        GlobalTable.symbolTable.get("SysArraySize").type,
                        expr.isLVal, expr, name
                );
            }
        }
        else if (expr.type instanceof StringType) {
            if (name.equals("length")) {
                return new MemberExpr(
                        GlobalTable.symbolTable.get("SysStrLen").type,
                        expr.isLVal, expr, name
                );
            } else if (name.equals("ord")) {
                return new MemberExpr(
                        GlobalTable.symbolTable.get("SysStrOrd").type,
                        expr.isLVal, expr, name
                );
            } else if (name.equals("substring")) {
                return new MemberExpr(
                        GlobalTable.symbolTable.get("SysStrSub").type,
                        expr.isLVal, expr, name
                );
            } else if (name.equals("parseInt")) {
                return new MemberExpr(
                        GlobalTable.symbolTable.get("SysStrParseInt").type,
                        expr.isLVal, expr, name
                );
            }
        }
        throw new CE("MemberExpr:invalid lhs");
    }
    
    public void emit(List<Inst> instList) {
        if (expr.type instanceof ClassType) {
            ClassType classType=(ClassType)expr.type;
            MemObj memObj=classType.getMem(member);
            if (memObj instanceof MemVar) {
                MemVar memVar=(MemVar)memObj;
                expr.emit(instList);
                expr.load(instList);
                VReg address=(VReg)expr.operand;
                IValue delta=new IValue(memVar.offset);
                operand=new Address(address,delta,memVar.type.size());
            }
        }
    }

    @Override
    public void load(List<Inst> instList) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = GlobalTable.regTable.addTempReg(null);
            instList.add(LoadInst.getInst(operand, address));
        }
    }
}
