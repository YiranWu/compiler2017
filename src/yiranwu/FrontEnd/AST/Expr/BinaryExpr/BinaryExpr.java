package yiranwu.FrontEnd.AST.Expr.BinaryExpr;

import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class BinaryExpr extends Expr {
    public Expr lhs,rhs;

    public BinaryExpr(Type type,boolean isLVal,Expr lhs,Expr rhs) {
        super(type,isLVal);
        this.lhs=lhs;
        this.rhs=rhs;
    }
}
