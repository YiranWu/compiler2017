package yiranwu.FrontEnd.AST.Expr.BinaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.LessEqualInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Expr.CallExpr;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.BoolConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.IntConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.StringConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class LessEqualExpr extends BinaryExpr {
    private LessEqualExpr(Type type,Expr lhs,Expr rhs) {
        super(type,false,lhs,rhs);
    }

    public static Expr getExpr(Expr lhs,Expr rhs) {
        if (lhs.type instanceof IntType && rhs.type instanceof IntType) {
            if (lhs instanceof IntConstant && rhs instanceof IntConstant) {
                int val1=((IntConstant)lhs).val;
                int val2=((IntConstant)rhs).val;
                return BoolConstant.getConstant(val1<=val2);
            }
            return new LessEqualExpr(BoolType.getType(),lhs,rhs);
        }
        else if (lhs.type instanceof StringType && rhs.type instanceof StringType) {
            if (lhs instanceof StringConstant && rhs instanceof StringConstant) {
                String val1=((StringConstant)lhs).val;
                String val2=((StringConstant)rhs).val;
                return BoolConstant.getConstant(val1.compareTo(val2)<=0);
            }
            return CallExpr.getExpr(
                    (FuncType) GlobalTable.symbolTable.get("SysStrLeq").type,
                    new ArrayList<Expr>() {{
                        add(lhs);
                        add(rhs);
                    }}
            );
        }
        throw new CE("LessEqualExpr:type mismatch");
    }

    public void emit(List<Inst> instList) {
        lhs.emit(instList);
        lhs.load(instList);
        rhs.emit(instList);
        rhs.load(instList);
        operand = GlobalTable.regTable.addTempReg(null);
        instList.add(LessEqualInst.getInst(operand,lhs.operand,rhs.operand));
    }
}
