package yiranwu.FrontEnd.AST.Expr.BinaryExpr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.LoadInst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.StoreInst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class AssignExpr extends BinaryExpr {
    public AssignExpr(Type type,Expr lhs,Expr rhs) {
        super(type,true,lhs,rhs);
    }

    public static Expr getExpr(Expr lhs,Expr rhs) {
        if (!lhs.isLVal) throw new CE("AssignExpr:lhs non-LValue");
        if (lhs.type.isSameType(rhs.type))
            return new AssignExpr(lhs.type,lhs,rhs);
        throw new CE("AssignExpr:type mismatch");
    }

    public void emit(List<Inst> instList) {
        lhs.emit(instList);
        rhs.emit(instList);
        rhs.load(instList);
        operand=lhs.operand;
        if (lhs.operand instanceof Address)
            instList.add(StoreInst.getInst(rhs.operand, lhs.operand));
        else
            instList.add(MoveInst.getInst(lhs.operand, rhs.operand));
    }

    @Override
    public void load(List<Inst> instList) {
        if (operand instanceof Address) {
            Address address=(Address)operand;
            operand=GlobalTable.regTable.addTempReg(null);
            instList.add(LoadInst.getInst(operand,address));
        }
    }
}
