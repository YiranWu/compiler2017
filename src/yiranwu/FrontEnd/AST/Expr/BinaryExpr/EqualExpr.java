package yiranwu.FrontEnd.AST.Expr.BinaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.EqualInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Expr.CallExpr;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.BoolConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.IntConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.NullConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.StringConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class EqualExpr extends BinaryExpr {
    private EqualExpr(Type type,Expr lhs,Expr rhs) {
        super(type,false,lhs,rhs);
    }

    public static Expr getExpr(Expr lhs, Expr rhs) {
        if (!lhs.type.isSameType(rhs.type))
            throw new CE("EqualExpr:type mismatch");
        if (lhs instanceof NullConstant && rhs instanceof NullConstant)
            return BoolConstant.getConstant(true);
        else if (lhs instanceof BoolConstant && rhs instanceof BoolConstant) {
            boolean val1 = ((BoolConstant)lhs).val;
            boolean val2 = ((BoolConstant)rhs).val;
            return BoolConstant.getConstant(val1==val2);
        }
        else if (lhs instanceof IntConstant && rhs instanceof IntConstant) {
            int val1=((IntConstant)lhs).val;
            int val2=((IntConstant)rhs).val;
            return BoolConstant.getConstant(val1==val2);
        }
        else if (lhs instanceof StringConstant && rhs instanceof StringConstant) {
            String val1=((StringConstant)lhs).val;
            String val2=((StringConstant)rhs).val;
            return BoolConstant.getConstant(val1.equals(val2));
        }
        if (lhs.type instanceof StringType && rhs.type instanceof StringType) {
            return CallExpr.getExpr(
                    (FuncType) GlobalTable.symbolTable.get("SysStrEqual").type,
                    new ArrayList<Expr>() {{
                        add(lhs);
                        add(rhs);
                    }}
            );
        }
        return new EqualExpr(BoolType.getType(),lhs,rhs);
    }
    
    public void emit(List<Inst> instList) {
        lhs.emit(instList);
        lhs.load(instList);
        rhs.emit(instList);
        rhs.load(instList);
        operand = GlobalTable.regTable.addTempReg(null);
        instList.add(EqualInst.getInst(operand,lhs.operand,rhs.operand));
    }
}
