package yiranwu.FrontEnd.AST.Expr.BinaryExpr;

import yiranwu.BackEnd.CFG.Inst.ControlInst.BranchInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.BoolConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class LogicAndExpr extends BinaryExpr {
    private LogicAndExpr(Type type,Expr lhs,Expr rhs) {
        super(type,false,lhs,rhs);
    }

    public static Expr getExpr(Expr lhs, Expr rhs) {
        if (lhs.type instanceof BoolType && rhs.type instanceof BoolType) {
            if (lhs instanceof BoolConstant && rhs instanceof BoolConstant) {
                boolean val1=((BoolConstant)lhs).val;
                boolean val2=((BoolConstant)rhs).val;
                return BoolConstant.getConstant(val1&&val2);
            }
            return new LogicAndExpr(BoolType.getType(),lhs,rhs);
        }
        throw new CE("LogicAndExpr:type mismatch");
    }
    
    public void emit(List<Inst> instList) {
        LabelInst trueLabel=LabelInst.getInst("Logic_true");
        LabelInst falseLabel=LabelInst.getInst("Logic_false");
        LabelInst mergeLabel=LabelInst.getInst("Logic_merge");
        lhs.emit(instList);
        lhs.load(instList);
        instList.add(BranchInst.getInst(lhs.operand,trueLabel,falseLabel));
        instList.add(trueLabel);
        rhs.emit(instList);
        rhs.load(instList);
        operand=rhs.operand;
        instList.add(JumpInst.getInst(mergeLabel));
        instList.add(falseLabel);
        instList.add(MoveInst.getInst(operand,new IValue(0)));
        instList.add(JumpInst.getInst(mergeLabel));
        instList.add(mergeLabel);
    }
}
