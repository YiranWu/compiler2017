package yiranwu.FrontEnd.AST.Expr.BinaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.BitXorInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.IntConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class BitXorExpr extends BinaryExpr {
    private BitXorExpr(Type type,Expr lhs,Expr rhs) {
        super(type,false,lhs,rhs);
    }

    public static Expr getExpr(Expr lhs,Expr rhs) {
        if (lhs.type instanceof IntType && rhs.type instanceof IntType) {
            if (lhs instanceof IntConstant && rhs instanceof IntConstant) {
                int val1=((IntConstant)lhs).val;
                int val2=((IntConstant)rhs).val;
                return IntConstant.getConstant(val1^val2);
            }
            return new BitXorExpr(IntType.getType(),lhs,rhs);
        }
        throw new CE("BitXorExpr:type mismatch");
    }

    public void emit(List<Inst> instList) {
        lhs.emit(instList);
        lhs.load(instList);
        rhs.emit(instList);
        rhs.load(instList);
        operand = GlobalTable.regTable.addTempReg(null);
        instList.add(BitXorInst.getInst(operand,lhs.operand,rhs.operand));
    }
}
