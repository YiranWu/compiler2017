package yiranwu.FrontEnd.AST.Expr.BinaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.AddInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Expr.CallExpr;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.IntConstant;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.StringConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class AddExpr extends BinaryExpr {
    public AddExpr(Type type,boolean isLVal,Expr lhs,Expr rhs) {
        super(type,isLVal,lhs,rhs);
    }

    public static Expr getExpr(Expr lhs, Expr rhs) {
        if (lhs.type instanceof StringType && rhs.type instanceof StringType) {
            if (lhs instanceof StringConstant && rhs instanceof StringConstant) {
                String val1=((StringConstant)lhs).val;
                String val2=((StringConstant)rhs).val;
                return StringConstant.getConstant(val1+val2);
            }
            return CallExpr.getExpr(
                    (FuncType) GlobalTable.symbolTable.get("SysStrConcat").type,
                    new ArrayList<Expr>() {{
                        add(lhs);
                        add(rhs);
                    }}
            );
        }
        if (lhs.type instanceof IntType && rhs.type instanceof IntType) {
            if (lhs instanceof IntConstant && rhs instanceof IntConstant) {
                int val1=((IntConstant)lhs).val;
                int val2=((IntConstant)rhs).val;
                return IntConstant.getConstant(val1+val2);
            }
            return new AddExpr(IntType.getType(),false,lhs,rhs);
        }
        throw new CE("AddExpr:Type mismatch");
    }

    public void emit(List<Inst> instList) {
        lhs.emit(instList);
        lhs.load(instList);
        rhs.emit(instList);
        rhs.load(instList);
        operand=GlobalTable.regTable.addTempReg(null);
        instList.add(AddInst.getInst(operand,lhs.operand,rhs.operand));
    }
}