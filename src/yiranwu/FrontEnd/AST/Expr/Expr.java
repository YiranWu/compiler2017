package yiranwu.FrontEnd.AST.Expr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.FrontEnd.AST.ASTNode;
import yiranwu.FrontEnd.AST.Type.Type;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class Expr implements ASTNode {
    public Type type;
    public boolean isLVal;
    public Operand operand;

    public Expr(Type type, boolean isLVal) {
        this.type=type;
        this.isLVal=isLVal;
        this.operand=null;
    }

    public abstract void emit(List<Inst> instList);

    public void load(List<Inst> instList) {}
}
