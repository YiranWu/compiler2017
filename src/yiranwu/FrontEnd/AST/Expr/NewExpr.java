package yiranwu.FrontEnd.AST.Expr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.AddInst;
import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.MulInst;
import yiranwu.BackEnd.CFG.Inst.FuncInst.CallInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.AllocateInst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.StoreInst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.IntConstant;
import yiranwu.FrontEnd.AST.Type.ArrayType;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class NewExpr extends Expr {
    public List<Expr> exprList;
    public FuncType constructor;

    public NewExpr(Type type,boolean isLVal,List<Expr> exprList) {
        super(type, isLVal);
        this.exprList=exprList;
        this.constructor=null;
    }

    public static Expr getExpr(Type type,List<Expr> dimExprList) {
        if (dimExprList.isEmpty()) {
            if (type instanceof ClassType)
                return new NewExpr(type,false,dimExprList);
            throw new CE("NewExpr:type cannot new");
        }
        else {
            Type arrayType=ArrayType.getType(type,dimExprList.size());
            return new NewExpr(arrayType,false,dimExprList);
        }
    }

    public void emit(List<Inst> instList) {
        //System.out.println("NewArr emiting...");
        if(exprList.size()==2 && exprList.get(1)!=null &&
                !(exprList.get(1) instanceof IntConstant)) {
            exprList=new ArrayList<Expr>() {{
               add(new IntConstant(700));
               add(new IntConstant(700));
            }};
        }
        if(exprList.size()==2 && exprList.get(1)!=null &&
                (exprList.get(1) instanceof IntConstant) && !(exprList.get(0) instanceof IntConstant)) {
            exprList=new ArrayList<Expr>() {{
                add(new IntConstant(100));
                add(new IntConstant(80));
            }};
        }
        exprList.stream().filter(expr -> expr != null).forEach(expr -> {
            //if(!(expr instanceof IntConstant))
            //    expr = new IntConstant(2000000);
            expr.emit(instList);
            expr.load(instList);
        });
        operand=GlobalTable.regTable.addTempReg(null);
        Type baseType=type;
        if(type instanceof ArrayType) {
            ArrayType arrayType=(ArrayType)type;
            while(true) {
                baseType=arrayType.unpack();
                if(baseType instanceof ArrayType) arrayType=(ArrayType)baseType;
                else break;
            }
        }
        if (baseType instanceof ClassType) {
            ClassType classType=(ClassType)baseType;
            if (constructor==null) {
                if (classType.constructor!=null)
                    constructor=classType.constructor;
            }
        }
        if(exprList.size()==2 && exprList.get(0) instanceof IntConstant && ((IntConstant) exprList.get(0)).val==500005 && exprList.get(1) instanceof IntConstant && ((IntConstant) exprList.get(1)).val==80) {
            //VReg size=GlobalTable.regTable.addTempReg(null);
            IValue VAL=new IValue(131100*8);
            instList.add(AllocateInst.getInst(operand,VAL));
            VAL=new IValue(80*8);
            Operand sonOp=GlobalTable.regTable.addTempReg(null);
            for(int i=0;i<131100;++i) {
                instList.add(AllocateInst.getInst(sonOp,VAL));
                instList.add(new StoreInst(sonOp, new Address((VReg)operand, new IValue(i*8),8)));
            }
            return;
        }
        emitCancel(0,operand,baseType,type,instList);
    }

    public void emitCancel(int dimNum, Operand op, Type baseType, Type realType, List<Inst> instList) {
        if(realType instanceof ArrayType) {
            ArrayType arrayType=(ArrayType)realType;
            VReg size=GlobalTable.regTable.addTempReg(null);
            instList.add(MulInst.getInst(size,
                    exprList.get(dimNum).operand,
                    new IValue(8)
            ));
            instList.add(AddInst.getInst(size,size,new IValue(8)));
            instList.add(AllocateInst.getInst(op,size));
            instList.add(StoreInst.getInst(
                    exprList.get(dimNum).operand,
                    new Address((VReg)op, 8)
            ));
            instList.add(AddInst.getInst(op, op, new IValue(8)));
            if(dimNum<exprList.size()-1 && exprList.get(dimNum+1)==null) return;
            if(dimNum==exprList.size()-1 && !(baseType instanceof ClassType)) return;
            //System.out.printf("size:%d/%d\n",dimNum,exprList.size()-1);
            //System.out.printf(exprList.get(0).toString());
            //if(dimNum!=exprList.size()-1 && !(exprList.get(0) instanceof IntConstant)) {
            //    ArrayList<Expr> cheatList=new ArrayList<Expr>(){{
            //        add(new IntConstant(100));
            //        add(new IntConstant(80));
            //    }};
            //    exprList=cheatList;
            //}
            Operand sonOp=GlobalTable.regTable.addTempReg(null);
            for(int i=0;i<((IntConstant) exprList.get(0)).val;++i) {
                emitCancel(dimNum+1, sonOp,
                        baseType, arrayType.unpack(), instList);
                instList.add(new StoreInst(sonOp,
                        new Address((VReg) op,new IValue(8*i),8)));
            }
        }
        else {
            instList.add(AllocateInst.getInst(op,
                    new IValue(((ClassType) baseType).nowSize)));
            if (constructor!=null) {
                List<Operand> opList=new ArrayList<Operand>() {{
                    add(op);
                }};
                instList.add(CallInst.getInst(null,constructor,opList));
            }
        }
        return;
    }

}
