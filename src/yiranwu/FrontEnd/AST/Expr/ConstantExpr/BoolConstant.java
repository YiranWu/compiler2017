package yiranwu.FrontEnd.AST.Expr.ConstantExpr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class BoolConstant extends Constant {
    public boolean val;

    public BoolConstant(boolean val) {
        super(BoolType.getType());
        this.val=val;
    }

    public static Constant getConstant(boolean val) {return new BoolConstant(val);}

    public void emit(List<Inst> instList) {operand=new IValue(val?1:0);}
}
