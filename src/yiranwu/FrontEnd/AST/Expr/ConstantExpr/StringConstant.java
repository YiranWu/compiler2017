package yiranwu.FrontEnd.AST.Expr.ConstantExpr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Type.Basic.StringType;
import yiranwu.Tables.GlobalTable;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class StringConstant extends Constant {
    public String val;

    private StringConstant(String val) {
        super(StringType.getType());
        StringBuilder tmp= new StringBuilder();
        for (int i=0;i<val.length();) {
            if(val.charAt(i)!='\\') {
                tmp.append(val.charAt(i));
                i+=1;
            }
            else {
                if(val.charAt(i+1)=='n') tmp.append('\n');
                if(val.charAt(i+1)=='\"') tmp.append('\"');
                i+=2;
            }
        }
        this.val = tmp.toString();
    }

    public static Constant getConstant(String val) {return new StringConstant(val);}

    public void emit(List<Inst> instList) {operand=GlobalTable.regTable.addStrReg(val);}
}
