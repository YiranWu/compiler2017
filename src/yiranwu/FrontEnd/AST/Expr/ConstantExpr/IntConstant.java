package yiranwu.FrontEnd.AST.Expr.ConstantExpr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class IntConstant extends Constant {
    public int val;

    public IntConstant(int val) {
        super(IntType.getType());
        this.val = val;
    }

    public static Constant getConstant(int val) {return new IntConstant(val);}

    public void emit(List<Inst> instList) {operand=new IValue(val);}
}
