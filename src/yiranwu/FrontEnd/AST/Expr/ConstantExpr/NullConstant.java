package yiranwu.FrontEnd.AST.Expr.ConstantExpr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.FrontEnd.AST.Type.Basic.NullType;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class NullConstant extends Constant {
    public NullConstant() {super(NullType.getType());}

    public static Constant getConstant() {return new NullConstant();}

    public void emit(List<Inst> instList) {operand = new IValue(0);}
}