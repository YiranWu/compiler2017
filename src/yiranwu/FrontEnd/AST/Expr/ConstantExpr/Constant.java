package yiranwu.FrontEnd.AST.Expr.ConstantExpr;

import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class Constant extends Expr {
    public Constant(Type type) {
        super(type, false);
    }
}
