package yiranwu.FrontEnd.AST.Expr;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Type.Class.ClassType;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Tables.Symbol;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class IdExpr extends Expr {
    public Symbol symbol;

    public IdExpr(Type retType,boolean retIsLVal,Symbol symbol) {
        super(retType,retIsLVal);
        this.symbol=symbol;
    }

    public static Expr getExpr(String name) {
        if (!GlobalTable.symbolTable.contains(name))
            throw new CE("No symbol named "+name);
        Symbol symbol=GlobalTable.symbolTable.get(name);
        if (symbol.scope instanceof ClassType)
            return MemberExpr.getExpr(IdExpr.getExpr("this"),name);
        else {
            if (symbol.type instanceof FuncType)
                return new IdExpr(symbol.type,false,symbol);
            else return new IdExpr(symbol.type,true,symbol);
        }
    }

    public void emit(List<Inst> instList) {operand = symbol.reg;}
}
