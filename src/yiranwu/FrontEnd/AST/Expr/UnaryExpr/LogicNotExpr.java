package yiranwu.FrontEnd.AST.Expr.UnaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.BitXorInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.BoolConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.BoolType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class LogicNotExpr extends UnaryExpr {
    private LogicNotExpr(Type type,Expr expr) {super(type,false,expr);}

    public static Expr getExpr(Expr expr) {
        if (expr.type instanceof BoolType) {
            if (expr instanceof BoolConstant) {
                boolean val=((BoolConstant)expr).val;
                return BoolConstant.getConstant(!val);
            }
            return new LogicNotExpr(BoolType.getType(),expr);
        }
        throw new CE("LogicNotExpr:non-bool type");
    }

    public void emit(List<Inst> instList) {
        expr.emit(instList);
        expr.load(instList);
        operand = GlobalTable.regTable.addTempReg(null);
        instList.add(BitXorInst.getInst(operand, expr.operand, new IValue(1)));
    }
}
