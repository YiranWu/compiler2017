package yiranwu.FrontEnd.AST.Expr.UnaryExpr;

import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Type;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class UnaryExpr extends Expr {
    public Expr expr;

    public UnaryExpr(Type type,boolean isLVal,Expr expr) {
        super(type,isLVal);
        this.expr=expr;
    }
}
