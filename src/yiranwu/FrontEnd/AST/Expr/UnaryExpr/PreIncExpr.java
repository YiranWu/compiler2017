package yiranwu.FrontEnd.AST.Expr.UnaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.AddInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.StoreInst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class PreIncExpr extends UnaryExpr {
    private PreIncExpr(Type type,boolean isLVal,Expr expr) {
        super(type,isLVal,expr);
    }

    public static Expr getExpr(Expr expr) {
        if (!expr.isLVal)
            throw new CE("PreIncExpr:not LValue");
        if (expr.type instanceof IntType)
            return new PreIncExpr(IntType.getType(), false, expr);
        throw new CE("PreIncExpr:non-int type");
    }

    public void emit(List<Inst> instList) {
        expr.emit(instList);
        if (expr.operand instanceof Address) {
            Address address=(Address)expr.operand;
            address=new Address(address.base,address.offset,address.size);
            expr.load(instList);
            operand=expr.operand;
            instList.add(AddInst.getInst(operand,operand,new IValue(1)));
            instList.add(StoreInst.getInst(operand,address));
        }
        else {
            expr.load(instList);
            operand=expr.operand;
            instList.add(AddInst.getInst(operand,operand,new IValue(1)));
        }
    }
}