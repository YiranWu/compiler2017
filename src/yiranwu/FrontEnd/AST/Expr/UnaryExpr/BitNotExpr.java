package yiranwu.FrontEnd.AST.Expr.UnaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.UnaryInst.BitNotInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.FrontEnd.AST.Expr.ConstantExpr.IntConstant;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class BitNotExpr extends UnaryExpr {
    public BitNotExpr(Type type,Expr expr) {super(type,false,expr);}

    public static Expr getExpr(Expr expr) {
        if (expr.type instanceof IntType) {
            if (expr instanceof IntConstant) {
                int val = ((IntConstant)expr).val;
                return IntConstant.getConstant(~val);
            }
            return new BitNotExpr(IntType.getType(),expr);
        }
        throw new CE("BitNotExpr:non-int type");
    }

    public void emit(List<Inst> instList) {
        expr.emit(instList);
        expr.load(instList);
        operand=GlobalTable.regTable.addTempReg(null);
        instList.add(BitNotInst.getInst(operand,expr.operand));
    }
}
