package yiranwu.FrontEnd.AST.Expr.UnaryExpr;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.SubInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.StoreInst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.VReg.GlobalReg;
import yiranwu.FrontEnd.AST.Expr.Expr;
import yiranwu.FrontEnd.AST.Type.Basic.IntType;
import yiranwu.FrontEnd.AST.Type.Type;
import yiranwu.Tables.GlobalTable;
import yiranwu.Utility.CE;

import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class PostDecExpr extends UnaryExpr {
    private PostDecExpr(Type type,boolean isLVal,Expr expr) {
        super(type,isLVal,expr);
    }

    public static Expr getExpr(Expr expr) {
        if (!expr.isLVal)
            throw new CE("PostDecExpr:not LValue");
        if (expr.type instanceof IntType)
            return new PostDecExpr(IntType.getType(),false,expr);
        throw new CE("PostDecExpr:non-int type");
    }

    public void emit(List<Inst> instList) {
        expr.emit(instList);
        operand=GlobalTable.regTable.addTempReg(null);
        if (expr.operand instanceof Address) {
            Address address=(Address)expr.operand;
            address=new Address(address.base,address.offset,address.size);
            expr.load(instList);
            instList.add(MoveInst.getInst(operand,expr.operand));
            instList.add(SubInst.getInst(expr.operand,expr.operand,new IValue(1)));
            instList.add(StoreInst.getInst(expr.operand,address));
        }
        else {
            expr.load(instList);
            instList.add(MoveInst.getInst(operand, expr.operand));
            instList.add(SubInst.getInst(expr.operand, expr.operand, new IValue(1)));
        }
    }
}
