package yiranwu.Utility;

import yiranwu.FrontEnd.CST.Listener.BaseListener;

/**
 * Created by wyr on 17-5-21.
 */
public class CE extends Error {
    public CE(String err) {
        super("CE at row "+ BaseListener.r+" column "+BaseListener.c+" :"+err);
    }
}
