package yiranwu.Utility;

import yiranwu.BackEnd.Translator.x86Reg;

/**
 * Created by wyr on 17-5-21.
 */
public class Utility {
    public static int getAligned(int size) {
        int regSize= x86Reg.size();
        return (size+regSize-1)/regSize*regSize;
    }
}
