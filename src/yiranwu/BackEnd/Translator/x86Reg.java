package yiranwu.BackEnd.Translator;

import yiranwu.BackEnd.CFG.Allocator.PhyReg;

/**
 * Created by wyr on 17-5-23.
 */
public class x86Reg extends PhyReg {
    public x86Reg(int id,String name) {
        super(id,name);
    }
    public static PhyReg rax=new x86Reg(0, "rax");
    public static PhyReg eax=new x86Reg(0, "eax");
    public static PhyReg rbx=new x86Reg(1, "rbx");
    public static PhyReg rcx=new x86Reg(2, "rcx");
    public static PhyReg cl=new x86Reg(2, "cl");
    public static PhyReg rdx=new x86Reg(3, "rdx");
    public static PhyReg edx=new x86Reg(3, "edx");
    public static PhyReg rsi= new x86Reg(4, "rsi");
    public static PhyReg rdi= new x86Reg(5, "rdi");
    public static PhyReg rbp= new x86Reg(6, "rbp");
    public static PhyReg rsp= new x86Reg(7, "rsp");
    public static PhyReg r8= new x86Reg(8, "r8");
    public static PhyReg e8= new x86Reg(8, "r8d");
    public static PhyReg r9= new x86Reg(9, "r9");
    public static PhyReg r10= new x86Reg(10, "r10");
    public static PhyReg r11= new x86Reg(11, "r11");
    public static PhyReg r12= new x86Reg(12, "r12");
    public static PhyReg r13= new x86Reg(13, "r13");
    public static PhyReg r14= new x86Reg(14, "r14");
    public static PhyReg e14= new x86Reg(14, "r14d");
    public static PhyReg r15= new x86Reg(15, "r15");
    public static PhyReg e15= new x86Reg(15, "r15d");
    public static PhyReg b15= new x86Reg(15, "r15b");

    public static PhyReg t1=r8;
    public static PhyReg et1=e8;
    public static PhyReg t2=r9;

    public static int size() {return 8;}

    @Override
    public String toString() {
        return name;
    }
}
