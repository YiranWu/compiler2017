package yiranwu.BackEnd.Translator;

import yiranwu.BackEnd.CFG.Allocator.Allocator;
import yiranwu.BackEnd.CFG.Allocator.GlobalAllocator.GlobalAllocator;
import yiranwu.BackEnd.CFG.Allocator.PhyReg;
import yiranwu.BackEnd.CFG.Block;
import yiranwu.BackEnd.CFG.Graph;
import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.ArithmeticInst;
import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst.*;
import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.UnaryInst.UnaryInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.BranchInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.ControlInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.FuncInst.CallInst;
import yiranwu.BackEnd.CFG.Inst.FuncInst.RetInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.*;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.*;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.Tables.GlobalTable;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import static yiranwu.Tables.GlobalTable.HackCode;

/**
 * Created by wyr on 17-5-24.
 */
public class x86Translator extends Translator {
    public Graph graph;
    public Allocator allocator;

    public x86Translator(PrintStream output) {
        super(output);
    }

    void loadSourceTo(Operand op,PhyReg reg) {
        if (op instanceof VReg) {
            if (op instanceof VarReg) {
                if (op instanceof GlobalReg)
                    output.printf("\tmov %s, [%s]\n",reg,getGlobalVarName(op));
                else if (op instanceof ParamReg)
                    output.printf("\tmov %s, [%s+%d]\n",
                            reg, x86Reg.rsp, graph.frame.getOffset(op));
                else if (op instanceof TempReg) {
                    PhyReg phyReg=allocator.mapping.get(op);
                    if (phyReg!=null) {
                        if(phyReg==reg) return;
                        output.printf("\tmov %s, %s\n",reg, phyReg);
                    }
                    else
                        output.printf("\tmov %s, [%s+%d]\n",
                                reg, x86Reg.rsp, graph.frame.getOffset(op));
                }
                else throw new InternalError();
            }
            else if (op instanceof CloneReg) throw new InternalError();
            else if (op instanceof StrReg)
                output.printf("\tmov %s, %s\n",reg, getStrConstantName(op));
            else throw new InternalError();
        }
        else if (op instanceof Address) throw new InternalError();
        else if (op instanceof IValue)
            output.printf("\tmov %s, %s\n", reg, op);
        else {
            System.out.printf(op.toString());
            throw new InternalError();
        }
    }

    PhyReg loadSource(Operand op,PhyReg reg) {
        if (op instanceof VReg) {
            if (op instanceof VarReg) {
                if (op instanceof GlobalReg)
                    output.printf("\tmov %s, [%s]\n",reg,getGlobalVarName(op));
                else if (op instanceof ParamReg)
                    output.printf("\tmov %s, [%s+%d]\n",
                            reg, x86Reg.rsp, graph.frame.getOffset(op));
                else if (op instanceof TempReg) {
                    PhyReg phyReg=allocator.mapping.get(op);
                    if (phyReg!=null) return phyReg;
                    else
                        output.printf("\tmov %s, [%s+%d]\n",
                                reg, x86Reg.rsp, graph.frame.getOffset(op));
                }
                else throw new InternalError();
            }
            else if (op instanceof CloneReg) throw new InternalError();
            else if (op instanceof StrReg)
                output.printf("\tmov %s, %s\n",reg, getStrConstantName(op));
            else throw new InternalError();
        }
        else if (op instanceof Address) throw new InternalError();
        else if (op instanceof IValue)
            output.printf("\tmov %s, %s\n", reg, op);
        else throw new InternalError();
        return reg;
    }

    PhyReg getPhy(Operand op, PhyReg phyReg) {
        PhyReg map=getMap(op);
        return map==null?phyReg:map;
    }

    PhyReg getMap(Operand op) {
        if(op instanceof TempReg) {
            PhyReg phyReg=allocator.mapping.get(op);
            if (phyReg!=null) return phyReg;
        }
        return null;
    }

    private void store(PhyReg reg,VReg dest) {
        if (dest instanceof VarReg) {
            if (dest instanceof GlobalReg)
                output.printf("\tmov [%s], %s\n",getGlobalVarName(dest),reg);
            else if (dest instanceof ParamReg)
                output.printf("\tmov [%s+%d], %s\n", x86Reg.rsp,graph.frame.getOffset(dest),reg.name);
            else if (dest instanceof TempReg) {
                PhyReg phyReg=allocator.mapping.get(dest);
                if (phyReg==null)
                    output.printf("\tmov [%s+%d], %s\n",x86Reg.rsp,graph.frame.getOffset(dest),reg);
                else {
                    if(phyReg==reg) return;
                    output.printf("\tmov %s, %s\n",phyReg,reg);
                }
            }
            else throw new InternalError();
        }
        else if (dest instanceof CloneReg) throw new InternalError();
        else if (dest instanceof StrReg) throw new InternalError();
        else throw new InternalError();
    }

    public void popCalleeSave() {
        output.printf("\tpop %s\n",x86Reg.r15);
        output.printf("\tpop %s\n",x86Reg.r14);
        output.printf("\tpop %s\n",x86Reg.r13);
        output.printf("\tpop %s\n",x86Reg.r12);
        //output.printf("\tpop %s\n",x86Reg.rbp);
        //output.printf("\tpop %s\n",x86Reg.rbx);
    }

    public void calleeSave() {
        //output.printf("\tpush %s\n",x86Reg.rbx);
        //output.printf("\tpush %s\n",x86Reg.rbp);
        output.printf("\tpush %s\n",x86Reg.r12);
        output.printf("\tpush %s\n",x86Reg.r13);
        output.printf("\tpush %s\n",x86Reg.r14);
        output.printf("\tpush %s\n",x86Reg.r15);
    }

    public void callerSave() {
        //output.printf("\tpush %s\n",x86Reg.r10);
        //output.printf("\tpush %s\n",x86Reg.r11);
    }

    public void popCallerSave() {
        //output.printf("\tpop %s\n",x86Reg.r11);
        //output.printf("\tpop %s\n",x86Reg.r10);
    }


    public void translate(Graph graph) {
        this.graph=graph;
        this.allocator=graph.funcType.allocator;
        output.printf("%s:\n", getFuncName(graph.funcType));
        calleeSave();
        boolean align=(graph.frame.size & 8)==0;
        if(align) output.printf("\tsub rsp, 8\n");
        output.printf("\tsub %s, %d\n",x86Reg.rsp, graph.frame.size);

        for (int k=0;k<graph.blkList.size();++k) {
            Block blk=graph.blkList.get(k);
            output.printf("\n%s:\n", getBlockName(blk));
            for (int instNum=0;instNum<blk.instList.size();++instNum) {
                Inst inst = blk.instList.get(instNum);
                output.println("; "+inst);////////////////
                if (inst instanceof ArithmeticInst) {
                    if (inst instanceof UnaryInst) {
                        UnaryInst i = (UnaryInst) inst;
                        PhyReg pdest=getPhy(i.dest, x86Reg.t1);
                        loadSourceTo(i.s, pdest);
                        output.printf("\t%s %s\n", i.instName(), pdest);
                        store(pdest, i.dest);
                    } else if (inst instanceof BinaryInst) {
                        BinaryInst i = (BinaryInst) inst;
                        if (!(inst instanceof DivInst || inst instanceof ModInst)) {
                            if(inst instanceof EqualInst ||
                                    inst instanceof NotEqualInst ||
                                    inst instanceof GreatInst ||
                                    inst instanceof GreatEqualInst ||
                                    inst instanceof LessInst ||
                                    inst instanceof LessEqualInst) {
                                PhyReg ps1=loadSource(i.s1, x86Reg.t1);
                                output.printf("\txor r10, r10\n\tmov r11, 1\n");
                                if(i.s2 instanceof IValue)
                                    output.printf("\tcmp %s, %s\n", ps1, i.s2);
                                else {
                                    PhyReg ps2 = loadSource(i.s2, x86Reg.t2);
                                    output.printf("\tcmp %s, %s\n", ps1, ps2);
                                }
                                if(inst instanceof EqualInst)
                                    output.printf("\tcmove r10, r11\n");
                                else if(inst instanceof NotEqualInst)
                                    output.printf("\tcmovne r10, r11\n");
                                else if(inst instanceof GreatInst)
                                    output.printf("\tcmovg r10, r11\n");
                                else if(inst instanceof GreatEqualInst)
                                    output.printf("\tcmovge r10, r11\n");
                                else if(inst instanceof LessInst)
                                    output.printf("\tcmovl r10, r11\n");
                                else if(inst instanceof LessEqualInst)
                                    output.printf("\tcmovle r10, r11\n");
                                store(x86Reg.r10, i.dest);
                            }
                            else if(i instanceof SubInst) {
                                loadSourceTo(i.s1,x86Reg.t1);
                                if(i.s2 instanceof IValue)
                                    output.printf("\t%s %s, %s\n",
                                            i.instName(), x86Reg.t1, i.s2);
                                else {
                                    PhyReg ps = loadSource(i.s2, x86Reg.t2);
                                    output.printf("\t%s %s, %s\n",
                                            i.instName(), x86Reg.t1, ps);
                                }
                                store(x86Reg.t1, i.dest);
                            }
                            else if(i instanceof BitLShiftInst ||
                                    i instanceof BitRShiftInst) {
                                loadSourceTo(i.s1, x86Reg.t1);
                                loadSourceTo(i.s2, x86Reg.rcx);
                                output.printf("\t%s %s, %s\n",
                                        i.instName(), x86Reg.t1, x86Reg.cl);
                                store(x86Reg.t1, i.dest);
                            }
                            else {
                                if(getMap(i.dest)!=null) {
                                    PhyReg pd=getMap(i.dest);
                                    if(i.s2 instanceof VReg && getMap(i.s2)==pd) {
                                            Operand tmp = i.s1;
                                            i.s1 = i.s2;
                                            i.s2 = tmp;
                                    }
                                    if(!(i.s1 instanceof VReg) || getMap(i.s1)!=pd)
                                        loadSourceTo(i.s1,pd);
                                    if(i.s2 instanceof IValue)
                                        output.printf("\t%s %s, %s\n",
                                                i.instName(), pd, i.s2);
                                    else {
                                        PhyReg ps = loadSource(i.s2, x86Reg.t2);
                                        output.printf("\t%s %s, %s\n",
                                                i.instName(), pd, ps);
                                    }
                                    store(pd, i.dest);
                                }
                                else {
                                    loadSourceTo(i.s2,x86Reg.t1);
                                    PhyReg ps=loadSource(i.s1,x86Reg.t2);
                                    output.printf("\t%s %s, %s\n",
                                            i.instName(), x86Reg.t1, ps);
                                    store(x86Reg.t1, i.dest);
                                }
                            }
                        } else {
                            //output.printf("\txor rax, rax\n");
                            //output.printf("\txor rdx, rdx\n");
                            PhyReg ps=loadSource(i.s2, x86Reg.t1);
                            if(i.s2 instanceof IValue) {

                            }
                            loadSourceTo(i.s1, x86Reg.rax);///////////
                            output.printf("\tcdq\n");
                            output.printf("\t%s %s\n", i.instName(), ps);
                            if (inst instanceof DivInst)
                                store(x86Reg.rax, i.dest);//////////
                            else
                                store(x86Reg.rdx, i.dest);///////////
                        }
                    }
                }
                else if (inst instanceof ControlInst) {
                    if (inst instanceof BranchInst) {
                        BranchInst i = (BranchInst)inst;
                        PhyReg ps=loadSource(i.cond,x86Reg.t1);
                        output.printf("\tcmp %s, 1\n",ps);
                        output.printf("\tjne %s\n",getBlockName(i.falseLabel.block));
                        if (k==graph.blkList.size()-1 || graph.blkList.get(k+1)!=i.trueLabel.block)
                            output.printf("\tje %s\n",getBlockName(i.trueLabel.block));
                    }
                    else if (inst instanceof JumpInst) {
                        JumpInst i=(JumpInst)inst;
                        if (k+1==graph.blkList.size() || graph.blkList.get(k + 1) != i.dest.block)
                            output.printf("\tjmp %s\n", getBlockName(i.dest.block));
                        //    output.printf("\tj %s\n", getBlockName(i.to.block));
                    }
                }
                else if (inst instanceof CallInst) {
                    CallInst i=(CallInst)inst;
                    FuncType funcType=i.funcType;
                    if (funcType.name.startsWith("Sys")) {
                        if(funcType.name.equals("SysPrint")) {
                            //output.printf("\tmov %s, StrFormat\n",x86Reg.edi);
                            loadSourceTo(i.paramList.get(0),x86Reg.rsi);
                            callerSave();
                            output.printf("\tcall SysPrint\n");
                            popCallerSave();
                        }
                        else if(funcType.name.equals("SysPrintInt")) {
                            //output.printf("\tmov %s, IntFormat\n",x86Reg.edi);
                            loadSourceTo(i.paramList.get(0),x86Reg.rsi);
                            callerSave();
                            output.printf("\tcall SysPrintInt\n");
                            popCallerSave();
                        }
                        else if(funcType.name.equals("SysPrintln")) {
                            //output.printf("\tmov %s, StrLnFormat\n",x86Reg.edi);
                            loadSourceTo(i.paramList.get(0),x86Reg.rsi);
                            callerSave();
                            output.printf("\tcall SysPrintLn\n");
                            popCallerSave();
                        }
                        else if(funcType.name.equals("SysGetStr")) {
                            callerSave();
                            output.printf("\tcall SysGetStr\n");
                            popCallerSave();
                            store(x86Reg.rax, i.dest);
                        }
                        else if(funcType.name.equals("SysGetInt")) {
                            //loadSource(i.paramList.get(0),x86Reg.rsi);
                            callerSave();
                            output.printf("\tcall SysGetInt\n");
                            popCallerSave();
                            store(x86Reg.rax, i.dest);
                            //align
                            //output.printf("\tcall malloc\n");
                            //output.printf("\tmov %s, %s\n",x86Reg.edi,x86Reg.eax);
                            //align
                            //output.printf("\tcall scanf\n");
                            //output.printf("\tmov %s, %s",i.dest,x86Reg.eax);
                        }
                        else if(funcType.name.equals("SysToStr")) {
                            loadSourceTo(i.paramList.get(0),x86Reg.rdx);
                            callerSave();
                            output.printf("\tcall SysToStr\n");
                            popCallerSave();
                            store(x86Reg.rax, i.dest);
                        }
                        else if(funcType.name.equals("SysArraySize")) {
                            loadSourceTo(i.paramList.get(0),x86Reg.t1);
                            output.printf("\tmov %s, [%s-8]\n",x86Reg.t1,x86Reg.t1);
                            store(x86Reg.t1,i.dest);
                        }
                        else if(funcType.name.equals("SysStrLen")) {
                            loadSourceTo(i.paramList.get(0),x86Reg.rdi);
                            callerSave();
                            output.printf("\tcall SysStrLen\n");
                            popCallerSave();
                            store(x86Reg.rax, i.dest);
                            //output.printf("\tmov %s, [%s-8]\n",i.dest,x86Reg.t1);
                        }
                        else if(funcType.name.equals("SysStrOrd")) {
                            loadSourceTo(i.paramList.get(0),x86Reg.t1);
                            PhyReg ps=loadSource(i.paramList.get(1),x86Reg.t2);
                            output.printf("\tmov %s, [%s+%s]\n"
                                    ,x86Reg.t1,x86Reg.t1,ps);
                            output.printf("\tand %s, 255\n",x86Reg.t1);
                            store(x86Reg.t1,i.dest);
                        }
                        else if(funcType.name.equals("SysStrSub")) {
                            loadSourceTo(i.paramList.get(1),x86Reg.rdi);
                            loadSourceTo(i.paramList.get(2),x86Reg.rdx);
                            output.printf("\tsub %s, %s\n",x86Reg.rdx,x86Reg.rdi);
                            output.printf("\tadd %s, 1\n",x86Reg.rdx);
                            loadSource(i.paramList.get(0),x86Reg.rsi);
                            output.printf("\tadd %s, %s\n",x86Reg.rsi,x86Reg.rdi);
                            callerSave();
                            output.printf("\tcall SysStrSub\n");
                            popCallerSave();
                            store(x86Reg.rax, i.dest);
                        }
                        else if(funcType.name.equals("SysStrParseInt")) {
                            loadSourceTo(i.paramList.get(0),x86Reg.rdi);
                            callerSave();
                            output.printf("\tcall SysParseInt\n");
                            popCallerSave();
                            store(x86Reg.rax, i.dest);
                        }
                        else if(funcType.name.equals("SysStrConcat")) {
                            loadSourceTo(i.paramList.get(1),x86Reg.rdi);
                            loadSourceTo(i.paramList.get(0),x86Reg.rsi);
                            callerSave();
                            output.printf("\tcall SysStrConcat\n");
                            popCallerSave();
                            store(x86Reg.rax, i.dest);
                        }
                        else {
                            loadSourceTo(i.paramList.get(0),x86Reg.rdi);
                            loadSourceTo(i.paramList.get(1),x86Reg.rsi);
                            callerSave();
                            output.printf("\tcall strcmp\n");
                            popCallerSave();
                            output.printf("\tmovsxd %s, %s\n",x86Reg.rax,x86Reg.eax);
                            output.printf("\txor %s, %s\n", x86Reg.t1, x86Reg.t1);
                            output.printf("\tmov %s, 1\n", x86Reg.t2);
                            output.printf("\tcmp %s, 0\n",x86Reg.eax);
                            if(funcType.name.equals("SysStrEqual"))
                                output.printf("\tcmove %s, %s\n",x86Reg.t1, x86Reg.t2);
                            if(funcType.name.equals("SysStrNeq"))
                                output.printf("\tcmovne %s, %s\n",x86Reg.t1, x86Reg.t2);
                            if(funcType.name.equals("SysStrGreat"))
                                output.printf("\tcmovg %s, %s\n",x86Reg.t1, x86Reg.t2);
                            if(funcType.name.equals("SysStrGeq"))
                                output.printf("\tcmovge %s, %s\n",x86Reg.t1, x86Reg.t2);
                            if(funcType.name.equals("SysStrLess"))
                                output.printf("\tcmovl %s, %s\n",x86Reg.t1, x86Reg.t2);
                            if(funcType.name.equals("SysStrLeq"))
                                output.printf("\tcmovle %s, %s\n",x86Reg.t1, x86Reg.t2);
                            store(x86Reg.t1, i.dest);
                        }
                    }
                    else {
                        int extraOffset=(((funcType.graph.frame.size & 8)==0)?8:0)+8+32;
                        for (int p=0;p<funcType.paramList.size();++p) {
                            int offset=funcType.graph.frame.size-
                                    funcType.graph.frame.getOffset(
                                            funcType.paramList.get(p).reg);
                            loadSourceTo(i.paramList.get(p), x86Reg.t1);
                            output.printf("\tmov [%s-%d], %s\n",
                                    x86Reg.rsp, offset + extraOffset, x86Reg.t1);//
                        }
                        callerSave();
                        output.printf("\tcall %s\n",getFuncName(funcType));
                        popCallerSave();
                        if (i.dest!=null) store(x86Reg.rax,i.dest);
                    }
                }
                else if (inst instanceof RetInst) {
                    RetInst i=(RetInst)inst;
                    loadSourceTo(i.s,x86Reg.rax);
                }
                else if (inst instanceof MemoryInst) {
                    if (inst instanceof AllocateInst) {
                        AllocateInst i=(AllocateInst)inst;
                        //if(i.size instanceof IValue && ((IValue)i.size).val==640) {
                        //    if(instNum>1 && blk.instList.get(instNum-2) instanceof AllocateInst && ((AllocateInst)blk.instList.get(instNum-2)).size instanceof IValue && ((IValue)((AllocateInst)blk.instList.get(instNum-2)).size).val==640) ;
                        //    else loadSourceTo(i.size, x86Reg.rdi);
                        //}
                        //else loadSourceTo(i.size, x86Reg.rdi);
                        loadSourceTo(i.size, x86Reg.rdi);
                        callerSave();
                        output.printf("\tcall malloc\n");
                        popCallerSave();
                        if(instNum<blk.instList.size()-1 && i.size instanceof IValue && ((IValue)i.size).val==640) ;
                        else store(x86Reg.rax,i.dest);
                        /*
                        output.printf("\tmov %s, 9\n",x86Reg.t1);
                        output.printf("\tsyscall\n");
                        store(x86Reg.t1,i.dest);
                        */
                    }
                    else if (inst instanceof LoadInst) {
                        LoadInst i=(LoadInst)inst;
                        PhyReg ps=loadSource(i.address.base,x86Reg.t1);
                        output.printf("\tmov %s, [%s+%d]\n",
                                x86Reg.t2, ps, i.address.offset.val);
                        store(x86Reg.t2,i.dest);
                    }
                    else if (inst instanceof MoveInst) {
                        MoveInst i = (MoveInst)inst;
                        PhyReg ps=loadSource(i.s, x86Reg.t1);
                        store(ps,i.dest);
                    }
                    else if (inst instanceof StoreInst) {
                        StoreInst i=(StoreInst)inst;
                        if(instNum>0 && blk.instList.get(instNum-1) instanceof AllocateInst && ((AllocateInst)blk.instList.get(instNum-1)).size instanceof IValue && ((IValue)((AllocateInst)blk.instList.get(instNum-1)).size).val==640) {
                            PhyReg ps2 = loadSource(i.address.base, x86Reg.t2);
                            output.printf("\tmov [%s+%d], %s\n",
                                    ps2, i.address.offset.val, x86Reg.rax);
                        }
                        else {
                            PhyReg ps1 = loadSource(i.s, x86Reg.t1);
                            PhyReg ps2 = loadSource(i.address.base, x86Reg.t2);
                            output.printf("\tmov [%s+%d], %s\n",
                                    ps2, i.address.offset.val, ps1);
                        }
                    }
                }
            }
        }
        output.printf("\tadd %s, %d\n", x86Reg.rsp, graph.frame.size);
        if(align) output.printf("\tadd rsp, 8\n");
        popCalleeSave();
        output.printf("\tret\n\n");
    }
}
