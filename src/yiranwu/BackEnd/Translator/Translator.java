package yiranwu.BackEnd.Translator;

import yiranwu.BackEnd.CFG.Block;
import yiranwu.BackEnd.CFG.Graph;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.GlobalReg;
import yiranwu.BackEnd.CFG.Operand.VReg.StrReg;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.Tables.GlobalTable;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * Created by wyr on 17-5-23.
 */
public abstract class Translator {
    public PrintStream output;

    public Translator(PrintStream output) {
        this.output = output;
    }

    public abstract void translate(Graph graph);

    String getFuncName(FuncType funcType) {
        if (funcType.name.equals("main") || funcType.name.startsWith("Sys"))
            return funcType.name;
        else
            return String.format("func_%s", funcType.name);
    }

    String getBlockName(Block block) {
        return String.format("block_func_%s_id_%d_name_%s",
                block.funcType.name, block.id, block.name);
    }

    String getGlobalVarName(Operand operand) {
        return String.format("globalVar_%d", ((VReg)operand).id);
    }

    String getStrConstantName(Operand operand) {
        return String.format("strConstant_%d", ((VReg)operand).id);
    }
    
    public void translate() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("./lib/library.s")));
        String line=reader.readLine();
        while (line!=null && !line.startsWith("\t.data")) {
            output.println(line);
            line = reader.readLine();
        }
        for (FuncType funcType : GlobalTable.prog.funcList)
            translate(funcType.graph);
        while (line != null) {
            output.println(line);
            line = reader.readLine();
        }
        output.printf("\tsection .data\n");
        for (VReg reg : GlobalTable.regTable.regList) {
            if (reg instanceof StrReg) {
                String val = ((StrReg)reg).str;
                output.printf("%s:\n\tdb \"", getStrConstantName(reg));
                for (int i=0;i<val.length();++i) {
                    if(val.charAt(i)=='\"') {
                        output.printf("\", 34, \"");
                        continue;
                    }
                    if(val.charAt(i)!='\n')
                        output.printf("%c",val.charAt(i));
                    else
                        output.printf("\", 10, \"");
                }
                output.printf("\", 0\n");
            }
            else if (reg instanceof GlobalReg) {
                output.printf("%s:\n", getGlobalVarName(reg));
                output.printf("\tdq 0\n");//output.printf("\tdq 0\n", ((GlobalReg)reg).symbol.type.size());
            }
        }
        reader = new BufferedReader(new InputStreamReader(new FileInputStream("./lib/data.s")));
        line=reader.readLine();
        while (line!=null) {
            output.println(line);
            line = reader.readLine();
        }
    }
}
