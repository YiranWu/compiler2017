package yiranwu.BackEnd.CFG.Operand;

/**
 * Created by wyr on 17-5-21.
 */
public class IValue extends Operand {
    public int val;

    public IValue(int val) {this.val=val;}

    public boolean equals(Object obj) {
        if (obj instanceof IValue) {
            IValue ival=(IValue)obj;
            return ival.val==val;
        }
        else return false;
    }

    @Override
    public String toString() {return String.valueOf(val);}
}
