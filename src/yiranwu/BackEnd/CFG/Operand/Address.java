package yiranwu.BackEnd.CFG.Operand;

import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.Utility.CE;

/**
 * Created by wyr on 17-5-21.
 */
public class Address extends Operand {
    public VReg base;
    public IValue offset;
    public int size;

    public Address(VReg base,IValue offset,int size) {
        if (size!=0 && size!=8) {
            System.err.println(size);
            throw new CE(size+"VReg size mismatch");
        }
        this.base=base;
        this.offset=offset;
        this.size=size;
    }

    public Address(VReg base,int size) {
        if (size!=0 && size!=8) {
            throw new CE(size+"VReg size mismatch");
        }
        this.base=base;
        this.offset=new IValue(0);
        this.size=size;
    }
}
