package yiranwu.BackEnd.CFG.Operand.VReg;

import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.Tables.GlobalTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public class VReg extends Operand {
    public int id;
    public List<VReg> cloneList;
    public VReg() {
        this.id=GlobalTable.regTable.regList.size();
        this.cloneList=new ArrayList<>();
    }

    public VReg clone() {
        VReg clone=new CloneReg(this,cloneList.size());
        cloneList.add(clone);
        return clone;
    }
}
