package yiranwu.BackEnd.CFG.Operand.VReg;

/**
 * Created by wyr on 17-5-21.
 */
public class CloneReg extends VReg {
    public VReg origin;
    public int ver;

    public CloneReg(VReg origin,int ver) {
        this.origin=origin;
        this.ver=ver;
    }

}
