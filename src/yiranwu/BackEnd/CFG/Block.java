package yiranwu.BackEnd.CFG;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Type.FuncType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.addAll;

/**
 * Created by wyr on 17-5-22.
 */
public class Block {
    public FuncType funcType;
    public String name;
    public int id;
    public LabelInst label;
    public List<Inst> instList;
    public List<Block> successors, predecessors;
    public Liveliness liveliness;

    Block(FuncType funcType, String name, int id, LabelInst label) {
        this.funcType = funcType;
        this.name = name;
        this.id = id;
        this.label = label;
        this.instList = new ArrayList<>();
        this.successors = new ArrayList<>();
        this.predecessors = new ArrayList<>();
        this.liveliness = new Liveliness();
    }

    public class Liveliness {
        public List<VReg> used, defined;
        public Set<VReg> liveIn, liveOut;

        public Liveliness() {
            this.used = new ArrayList<>();
            this.defined = new ArrayList<>();
            this.liveIn = new HashSet<>();
            this.liveOut = new HashSet<>();
        }
    }
}