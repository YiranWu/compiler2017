package yiranwu.BackEnd.CFG;

import yiranwu.BackEnd.CFG.Inst.ControlInst.BranchInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.ControlInst;
import yiranwu.BackEnd.CFG.Inst.ControlInst.JumpInst;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.TempReg;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.BackEnd.Translator.x86Reg;
import yiranwu.FrontEnd.AST.Stmt.SimpDeclStmt;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.Tables.GlobalTable;
import yiranwu.Tables.Symbol;

import java.util.*;

/**
 * Created by wyr on 17-5-22.
 */
public class Graph {
    public FuncType funcType;
    public List<Block> blkList;
    public Block enter, entry, exit;
    public Frame frame;

    public Graph(FuncType funcType) {
        this.funcType = funcType;
        this.buildGraph();
    }

    public void buildGraph() {
        List<Inst> instList = new ArrayList<>();
        funcType.enter = LabelInst.getInst("enter");
        funcType.entry = LabelInst.getInst("entry");
        funcType.exit = LabelInst.getInst("exit");
        instList.add(funcType.enter);
        if (funcType.name.equals("main")) {
            for (SimpDeclStmt decl : GlobalTable.prog.varList) {
                decl.emit(instList);
            }
        }
        instList.add(JumpInst.getInst(funcType.entry));
        instList.add(funcType.entry);
        funcType.body.emit(instList);
        instList.add(JumpInst.getInst(funcType.exit));
        instList.add(funcType.exit);

        blkList = new ArrayList<>();
        for (int i = 0, j; i < instList.size(); i = j) {
            if (!(instList.get(i) instanceof LabelInst)) j = i + 1;
            else {
                LabelInst label = (LabelInst) instList.get(i);
                Block block = label.block = addBlock(label.name, label);
                for (j = i + 1; j < instList.size(); ++j) {
                    if (instList.get(j) instanceof LabelInst) break;
                    block.instList.add(instList.get(j));
                    if (instList.get(j) instanceof ControlInst) break;
                }
            }
        }
        for (Block block : blkList) {
            if (block.name.equals("enter")) enter = block;
            else if (block.name.equals("entry")) entry = block;
            else if (block.name.equals("exit")) exit = block;
        }
        buildFrame();
        refresh();
    }

    public Block addBlock(String name, LabelInst label) {
        Block block = new Block(funcType, name, blkList.size(), label);
        blkList.add(block);
        return block;
    }

    public class Frame {
        public int size;
        public Map<VReg, Integer> temp, param;

        public Frame() {
            size = 0;
            temp = new HashMap<>();
            param = new HashMap<>();
        }

        public int getOffset(Operand operand) {
            if (operand instanceof VReg) {
                if (temp.containsKey(operand)) return temp.get(operand);
                if (param.containsKey(operand)) return param.get(operand);
            }
            throw new InternalError();
        }
    }

    private void buildFrame() {
        Set<VReg> regSet = new HashSet<VReg>() {{
            for (Block block : blkList) {
                for (Inst inst : block.instList) {
                    for (VReg reg : inst.getUsedReg())
                        if (reg instanceof TempReg) add(reg);
                    for (VReg reg : inst.getDefReg())
                        if (reg instanceof TempReg) add(reg);
                }
            }
        }};
        frame = new Frame();
        for (VReg reg : regSet) {
            frame.temp.put(reg, frame.size);
            frame.size += x86Reg.size();
        }
        for (Symbol param : funcType.paramList) {
            frame.param.put(param.reg, frame.size);
            frame.size += param.type.size();
        }
    }

    public Graph refresh() {
        refreshGraph();
        analysisLiveliness();
        buildFrame();
        return this;
    }

    private void refreshGraph() {
        for (Block block : blkList) {
            block.successors = new ArrayList<>();
            block.predecessors = new ArrayList<>();
        }
        for (Block block : blkList) {
            if (!block.instList.isEmpty()) {
                Inst instruction = block.instList.get(block.instList.size() - 1);
                if (instruction instanceof JumpInst) {
                    JumpInst i = (JumpInst) instruction;
                    block.successors.add(i.dest.block);
                } else if (instruction instanceof BranchInst) {
                    BranchInst i = (BranchInst) instruction;
                    block.successors.add(i.trueLabel.block);
                    block.successors.add(i.falseLabel.block);
                }
            }
        }
        blkList = depthFirstSearch(enter, new HashSet<>());
        for (Block block : blkList) {
            for (Block successor : block.successors) {
                successor.predecessors.add(block);
            }
        }
    }

    private List<Block> depthFirstSearch(Block block, Set<Block> visit) {
        visit.add(block);
        List<Block> list = new ArrayList<Block>() {{
            add(block);
        }};
        for (Block successor : block.successors) {
            if (visit.contains(successor)) {
                continue;
            }
            if (successor != exit) {
                visit.add(successor);
                list.addAll(depthFirstSearch(successor, visit));
            }
        }
        if (block == enter) {
            list.add(exit);
        }
        return list;
    }

    private void analysisLiveliness() {
        for (Block block : blkList) {
            block.liveliness.used = new ArrayList<>();
            block.liveliness.defined = new ArrayList<>();
            for (Inst instruction : block.instList) {
                for (VReg register : instruction.getUsedReg()) {
                    if (!block.liveliness.defined.contains(register)) {
                        block.liveliness.used.add(register);
                    }
                }
                for (VReg register : instruction.getDefReg()) {
                    block.liveliness.defined.add(register);
                }
            }
        }
        for (Block block : blkList) {
            block.liveliness.liveIn = new HashSet<>();
            block.liveliness.liveOut = new HashSet<>();
        }
        while (true) {
            for (Block block : blkList) {
                block.liveliness.liveIn = new HashSet<VReg>() {{
                    block.liveliness.liveOut.forEach(this::add);
                    block.liveliness.defined.forEach(this::remove);
                    block.liveliness.used.forEach(this::add);
                }};
            }
            boolean modified = false;
            for (Block block : blkList) {
                Set<VReg> origin = block.liveliness.liveOut;
                block.liveliness.liveOut = new HashSet<VReg>() {{
                    for (Block successor : block.successors) {
                        addAll(successor.liveliness.liveIn);
                    }
                }};
                if (!block.liveliness.liveOut.equals(origin)) {
                    modified = true;
                }
            }
            if (!modified) {
                break;
            }
        }
    }
}