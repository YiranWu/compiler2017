package yiranwu.BackEnd.CFG.Inst.FuncInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class RetInst extends FuncInst {
    public Operand s;

    private RetInst(Operand s) {this.s=s;}

    public static Inst getInst(Operand s) {return new RetInst(s);}

    @Override
    public List<Operand> getDefOp() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.singletonList(s);
    }

    @Override
    public void setDefReg(VReg from,VReg to) {
    }

    @Override
    public void setUsedReg(VReg from,Operand to) {
        if (s==from) s=to;
    }
}
