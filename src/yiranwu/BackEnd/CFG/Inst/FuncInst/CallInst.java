package yiranwu.BackEnd.CFG.Inst.FuncInst;


import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Stmt.SimpDeclStmt;
import yiranwu.FrontEnd.AST.Type.FuncType;
import yiranwu.Tables.GlobalTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class CallInst extends FuncInst {
    public VReg dest;
    public FuncType funcType;
    public List<Operand> paramList;

    private CallInst(VReg dest,FuncType funcType,List<Operand> paramList) {
        this.dest=dest;
        this.funcType=funcType;
        this.paramList=paramList;
    }

    public static Inst getInst(Operand dest,FuncType funcType,List<Operand> paramList) {
        if (dest==null)
            return new CallInst(null,funcType,paramList);
        else if (dest instanceof VReg)
            return new CallInst((VReg)dest,funcType,paramList);
        throw new InternalError();
    }

    @Override
    public List<Operand> getDefOp() {
        return new ArrayList<Operand>() {{
            if (dest!=null) add(dest);
            if (!funcType.name.startsWith("Sys")) {
                for (SimpDeclStmt variable : GlobalTable.prog.varList)
                    add(variable.symbol.reg);
            }
        }};
    }

    @Override
    public List<Operand> getUsedOp() {
        return new ArrayList<Operand>() {{
            addAll(paramList);
            if (!funcType.name.startsWith("Sys")) {
                for (SimpDeclStmt variable : GlobalTable.prog.varList)
                    add(variable.symbol.reg);
            }
        }};
    }

    @Override
    public void setDefReg(VReg from, VReg to) {
        if (dest==from) dest=to;
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
        for (int i = 0; i < paramList.size(); ++i)
            if (paramList.get(i)==from) paramList.set(i,to);
    }
}
