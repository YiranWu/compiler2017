package yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

/**
 * Created by wyr on 17-5-22.
 */
public class LessInst extends BinaryInst {
    private LessInst(VReg dest,Operand s1,Operand s2) {super(dest,s1,s2);}

    public static Inst getInst(Operand dest, Operand s1, Operand s2) {
        if (dest instanceof VReg)
            return new LessInst((VReg)dest,s1,s2).rebuild();
        throw new InternalError();
    }

    @Override
    public Inst rebuild() {
        if (s1 instanceof IValue && s2 instanceof IValue) {
            int val1=((IValue)s1).val;
            int val2=((IValue)s2).val;
            if(val2!=0) return MoveInst.getInst(dest,new IValue(val1<val2?1:0));
        }
        return this;
    }

    public String instName() {return "setl";}
}
