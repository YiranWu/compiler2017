package yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.ArithmeticInst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public abstract class BinaryInst extends ArithmeticInst {
    public VReg dest;
    public Operand s1,s2;

    protected BinaryInst(VReg dest,Operand s1,Operand s2) {
        this.dest=dest;
        this.s1=s1;
        this.s2=s2;
    }

    @Override
    public List<Operand> getDefOp() {
        return Collections.singletonList(dest);
    }

    @Override
    public List<Operand> getUsedOp() {
        return Arrays.asList(s1,s2);
    }

    @Override
    public void setDefReg(VReg from,VReg to) {
        if (dest==from) dest=to;
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
        if (s1==from) s1=to;
        if (s2==from) s2=to;
    }
}
