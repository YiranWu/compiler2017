package yiranwu.BackEnd.CFG.Inst.ArithmeticInst.BinaryInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

/**
 * Created by wyr on 17-5-22.
 */
public class BitLShiftInst extends BinaryInst {
    private BitLShiftInst(VReg dest,Operand s1,Operand s2) {super(dest,s1,s2);}

    public static Inst getInst(Operand dest,Operand s1,Operand s2) {
        if (dest instanceof VReg)
            return new BitLShiftInst((VReg)dest,s1,s2).rebuild();
        throw new InternalError();
    }

    @Override
    public Inst rebuild() {return this;}

    public String instName() {return "shl";}
}
