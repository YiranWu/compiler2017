package yiranwu.BackEnd.CFG.Inst.ArithmeticInst.UnaryInst;

import yiranwu.BackEnd.CFG.Inst.ArithmeticInst.ArithmeticInst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public abstract class UnaryInst extends ArithmeticInst {
    public VReg dest;
    public Operand s;

    protected UnaryInst(VReg dest,Operand s) {
        this.dest=dest;
        this.s=s;
    }

    @Override
    public List<Operand> getDefOp() {
        return Collections.singletonList(dest);
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.singletonList(s);
    }

    @Override
    public void setDefReg(VReg from, VReg to) {
        if (dest == from) {
            dest = to;
        }
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
        if (s == from) {
            s = to;
        }
    }
}
