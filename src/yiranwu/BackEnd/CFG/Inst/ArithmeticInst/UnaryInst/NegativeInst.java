package yiranwu.BackEnd.CFG.Inst.ArithmeticInst.UnaryInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

/**
 * Created by wyr on 17-5-22.
 */
public class NegativeInst extends UnaryInst {
    private NegativeInst(VReg dest,Operand s) {super(dest,s);}

    public static Inst getInst(Operand dest,Operand s) {
        return new NegativeInst((VReg)dest,s).rebuild();
    }

    @Override
    public Inst rebuild() {
        if (s instanceof IValue) {
            int val=((IValue)s).val;
            return MoveInst.getInst(dest,new IValue(-val));
        }
        return this;
    }

    public String instName()  {return "neg";}
}
