package yiranwu.BackEnd.CFG.Inst.ArithmeticInst;

import yiranwu.BackEnd.CFG.Inst.Inst;

/**
 * Created by wyr on 17-5-22.
 */
public abstract class ArithmeticInst extends Inst {
    public abstract String instName();
}
