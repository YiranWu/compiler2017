package yiranwu.BackEnd.CFG.Inst;

import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyr on 17-5-21.
 */
public abstract class Inst {
    public Inst rebuild() {return this;}

    public abstract List<Operand> getDefOp();
    public abstract List<Operand> getUsedOp();

    public List<VReg> getDefReg() {
        return new ArrayList<VReg>() {{
            for (Operand operand : getDefOp()) {
                if (operand instanceof VReg) {
                    add((VReg)operand);
                }
            }
        }};
    }

    public List<VReg> getUsedReg() {
        return new ArrayList<VReg>() {{
            for (Operand operand : getUsedOp()) {
                if (operand instanceof VReg) {
                    add((VReg)operand);
                }
            }
        }};
    }

    public abstract void setDefReg(VReg from, VReg to);
    public abstract void setUsedReg(VReg from, Operand to);

}
