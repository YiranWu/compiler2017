package yiranwu.BackEnd.CFG.Inst.ControlInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.BackEnd.CFG.Operand.IValue;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class BranchInst extends ControlInst {
    public Operand cond;
    public LabelInst trueLabel,falseLabel;

    private BranchInst(Operand cond,LabelInst trueLabel,LabelInst falseLabel) {
        this.cond=cond;
        this.trueLabel=trueLabel;
        this.falseLabel=falseLabel;
    }

    public static Inst getInst(Operand cond,LabelInst trueLabel,LabelInst falseLabel) {
        return new BranchInst(cond,trueLabel,falseLabel).rebuild();
    }

    @Override
    public Inst rebuild() {
        if (cond instanceof IValue) {
            int val=((IValue)cond).val;
            if (val==0) return JumpInst.getInst(falseLabel);
            else return JumpInst.getInst(trueLabel);
        }
        return this;
    }

    @Override
    public List<Operand> getDefOp() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.singletonList(cond);
    }

    @Override
    public void setDefReg(VReg from,VReg to) {
    }

    @Override
    public void setUsedReg(VReg from,Operand to) {
        if (cond==from) cond=to;
    }
}
