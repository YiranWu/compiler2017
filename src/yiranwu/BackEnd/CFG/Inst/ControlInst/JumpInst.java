package yiranwu.BackEnd.CFG.Inst.ControlInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.LabelInst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class JumpInst extends ControlInst {
    public LabelInst dest;

    private JumpInst(LabelInst dest) {this.dest=dest;}

    public static Inst getInst(LabelInst toLabel) {return new JumpInst(toLabel);}

    @Override
    public List<Operand> getDefOp() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.emptyList();
    }

    @Override
    public void setDefReg(VReg from, VReg to) {
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
    }
}
