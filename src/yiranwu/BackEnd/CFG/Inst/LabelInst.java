package yiranwu.BackEnd.CFG.Inst;

import yiranwu.BackEnd.CFG.Block;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class LabelInst extends Inst {
    public String name;
    public Block block;

    public LabelInst(String name) {this.name=name;}

    public static LabelInst getInst(String name) {return new LabelInst(name);}

    @Override
    public List<Operand> getDefOp() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.emptyList();
    }

    @Override
    public void setDefReg(VReg from, VReg to) {
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
    }
}
