package yiranwu.BackEnd.CFG.Inst.MemoryInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class StoreInst extends MemoryInst {
    public Operand s;
    public Address address;

    public StoreInst(Operand s, Address address) {
        this.s=s;
        this.address=address;
    }

    public static Inst getInst(Operand s,Operand address) {
        if (address instanceof Address)
            return new StoreInst(s,(Address)address);
        throw new InternalError();
    }

    @Override
    public List<Operand> getDefOp() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOp() {
        return Arrays.asList(s, address.base);
    }

    @Override
    public void setDefReg(VReg from, VReg to) {
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
        if (s==from) s=to;
        if (address.base==from)
            address=new Address((VReg)to,address.offset,address.size);
    }
}
