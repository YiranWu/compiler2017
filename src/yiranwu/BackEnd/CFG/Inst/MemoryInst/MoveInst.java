package yiranwu.BackEnd.CFG.Inst.MemoryInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class MoveInst extends MemoryInst {
    public VReg dest;
    public Operand s;

    private MoveInst(VReg dest, Operand s) {
        this.dest=dest;
        this.s=s;
    }

    public static Inst getInst(Operand dest,Operand s) {
        if (dest instanceof VReg)
            return new MoveInst((VReg)dest,s);
        throw new InternalError();
    }

    @Override
    public List<Operand> getDefOp() {
        return Collections.singletonList(dest);
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.singletonList(s);
    }

    @Override
    public void setDefReg(VReg from, VReg to) {
        if (dest==from) dest=to;
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
        if (s==from) s=to;
    }
}
