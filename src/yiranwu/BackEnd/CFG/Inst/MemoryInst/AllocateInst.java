package yiranwu.BackEnd.CFG.Inst.MemoryInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class AllocateInst extends MemoryInst {
    public VReg dest;
    public Operand size;

    private AllocateInst(VReg dest,Operand size) {
        this.dest=dest;
        this.size=size;
    }

    public static Inst getInst(Operand dest,Operand size) {
        if (dest instanceof VReg)
            return new AllocateInst((VReg)dest,size);
        throw new InternalError();
    }

    @Override
    public List<Operand> getDefOp() {
        return Collections.singletonList(dest);
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.singletonList(size);
    }

    @Override
    public void setDefReg(VReg from, VReg to) {
        if (dest==from) dest=to;
    }

    @Override
    public void setUsedReg(VReg from, Operand to) {
        if (size==from) size=to;
    }
}
