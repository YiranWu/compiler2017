package yiranwu.BackEnd.CFG.Inst.MemoryInst;

import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Operand.Address;
import yiranwu.BackEnd.CFG.Operand.Operand;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyr on 17-5-22.
 */
public class LoadInst extends MemoryInst {
    public VReg dest;
    public Address address;

    private LoadInst(VReg dest, Address address) {
        this.dest=dest;
        this.address=address;
    }

    public static Inst getInst(Operand dest,Operand address) {
        if (dest instanceof VReg && address instanceof Address)
            return new LoadInst((VReg)dest,(Address)address);
        throw new InternalError();
    }

    @Override
    public List<Operand> getDefOp() {
        return Collections.singletonList(dest);
    }

    @Override
    public List<Operand> getUsedOp() {
        return Collections.singletonList(address.base);
    }

    @Override
    public void setDefReg(VReg from,VReg to) {
        if (dest==from) dest=to;
    }

    @Override
    public void setUsedReg(VReg from,Operand to) {
        if (address.base==from)
            address=new Address((VReg)to,address.offset,address.size);
    }
}
