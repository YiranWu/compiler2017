package yiranwu.BackEnd.CFG.Allocator.GlobalAllocator;

import yiranwu.BackEnd.CFG.Allocator.PhyReg;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.*;

/**
 * Created by wyr on 17-5-29.
 */
public class GraphColoring extends BaseColoring {
    private Set<VReg> vertices;
    private Map<VReg, Integer> degree;

    public GraphColoring(InterGraph graph) {
        super(graph);
        vertices = new HashSet<>();
        degree = new HashMap<>();
    }

    public Map<VReg, PhyReg> analysis() {
        for (VReg vertex : graph.vSet) {
            vertices.add(vertex);
            degree.put(vertex, graph.forbids.get(vertex).size());
        }
        Stack<VReg> stack = new Stack<>();
        while (stack.size() < graph.vSet.size()) {
            int origin = stack.size();
            for (VReg vertex : vertices) {
                if (degree.get(vertex) < InterGraph.color.size()) {
                    stack.add(vertex);
                    remove(vertex);
                    break;
                }
            }
            if (stack.size() != origin) {
                continue;
            }
            for (VReg vertex : vertices) {
                if (degree.get(vertex) >= InterGraph.color.size()) {
                    stack.add(vertex);
                    remove(vertex);
                    break;
                }
            }
        }
        while (!stack.isEmpty()) {
            color(stack.pop());
        }
        return mapping;
    }

    private void remove(VReg vertex) {
        vertices.remove(vertex);
        for (VReg neighbor : graph.forbids.get(vertex)) {
            degree.put(neighbor, degree.get(neighbor) - 1);
        }
    }
}
