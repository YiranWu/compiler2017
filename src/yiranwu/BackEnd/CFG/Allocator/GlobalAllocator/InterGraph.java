package yiranwu.BackEnd.CFG.Allocator.GlobalAllocator;

import yiranwu.BackEnd.CFG.Allocator.PhyReg;
import yiranwu.BackEnd.CFG.Operand.VReg.TempReg;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.BackEnd.Translator.x86Reg;

import java.util.*;

/**
 * Created by wyr on 17-5-23.
 */
public class InterGraph {
    public static List<PhyReg> color=new ArrayList<PhyReg>() {{
        //add(x86Reg.rbx);
        //add(x86Reg.rbp);
        //add(x86Reg.r10);
        //add(x86Reg.r11);
        add(x86Reg.r12);
        add(x86Reg.r13);
        //add(x86Reg.rcx);
        add(x86Reg.r14);
        add(x86Reg.r15);
    }};

    public Set<VReg> vSet;
    public Map<VReg, Set<VReg>> forbids;
    public Map<VReg, Set<VReg>> recommends;

    public InterGraph() {
        vSet=new HashSet<>();
        forbids=new HashMap<>();
        recommends=new HashMap<>();
    }

    void add(VReg x) {
        vSet.add(x);
        forbids.put(x, new HashSet<>());
        recommends.put(x, new HashSet<>());
    }

    void forbid(VReg x, VReg y) {
        if (x == y) {
            return;
        }
        if (x instanceof TempReg && y instanceof TempReg) {
            forbids.get(x).add(y);
            forbids.get(y).add(x);
        }
    }

    void recommend(VReg x, VReg y) {
        if (x == y) {
            return;
        }
        if (x instanceof TempReg && y instanceof TempReg) {
            recommends.get(x).add(y);
            recommends.get(y).add(x);
        }
    }
}
