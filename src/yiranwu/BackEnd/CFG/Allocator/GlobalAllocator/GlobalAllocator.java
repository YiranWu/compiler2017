package yiranwu.BackEnd.CFG.Allocator.GlobalAllocator;

import yiranwu.BackEnd.CFG.Allocator.Allocator;
import yiranwu.BackEnd.CFG.Block;
import yiranwu.BackEnd.CFG.Inst.Inst;
import yiranwu.BackEnd.CFG.Inst.MemoryInst.MoveInst;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Type.FuncType;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wyr on 17-5-29.
 */
public class GlobalAllocator extends Allocator {
    public GlobalAllocator(FuncType funcType) {
        super(funcType);
        funcType.graph.refresh();
        InterGraph graph = new InterGraph();
        for (Block block : funcType.graph.blkList) {
            for (Inst inst : block.instList) {
                for (VReg register : inst.getUsedReg()) {
                    graph.add(register);
                }
                for (VReg register : inst.getDefReg()) {
                    graph.add(register);
                }
            }
        }
        for (Block block : funcType.graph.blkList) {
            Set<VReg> live = new HashSet<VReg>() {{
                block.liveliness.liveOut.forEach(this::add);
            }};
            for (int i = block.instList.size() - 1; i >= 0; --i) {
                Inst inst = block.instList.get(i);
                for (VReg register : inst.getDefReg()) {
                    for (VReg living : live) {
                        graph.forbid(register, living);
                    }
                }
                inst.getDefReg().forEach(live::remove);
                inst.getUsedReg().forEach(live::add);
            }
        }
        for (Block block : funcType.graph.blkList) {
            for (Inst inst : block.instList) {
                if (inst instanceof MoveInst) {
                    MoveInst i = (MoveInst) inst;
                    if (i.s instanceof VReg) {
                        graph.recommend(i.dest, (VReg) i.s);
                    }
                }
            }
        }
        mapping = new GraphColoring(graph).analysis();
    }
}
