package yiranwu.BackEnd.CFG.Allocator.GlobalAllocator;

import yiranwu.BackEnd.CFG.Allocator.PhyReg;
import yiranwu.BackEnd.CFG.Operand.VReg.VReg;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by wyr on 17-5-29.
 */
public class BaseColoring {
    protected InterGraph graph;
    Map<VReg, PhyReg> mapping;

    BaseColoring(InterGraph graph) {
        this.graph = graph;
        this.mapping = new HashMap<>();
    }

    void color(VReg vertex) {
        Set<PhyReg> used = new HashSet<PhyReg>() {{
            for (VReg neighbor : graph.forbids.get(vertex)) {
                if (mapping.containsKey(neighbor)) {
                    add(mapping.get(neighbor));
                }
            }
            add(null);
        }};
        for (VReg neighbor : graph.recommends.get(vertex)) {
            if (!mapping.containsKey(neighbor)) {
                continue;
            }
            PhyReg color = mapping.get(neighbor);
            if (!mapping.containsKey(vertex) && !used.contains(color)) {
                mapping.put(vertex, color);
                break;
            }
        }
        for (PhyReg color : InterGraph.color) {
            if (!mapping.containsKey(vertex) && !used.contains(color)) {
                mapping.put(vertex, color);
                break;
            }
        }
        if (!mapping.containsKey(vertex)) {
            mapping.put(vertex, null);
        }
    }
}
