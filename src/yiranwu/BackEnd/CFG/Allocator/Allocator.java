package yiranwu.BackEnd.CFG.Allocator;

import yiranwu.BackEnd.CFG.Operand.VReg.VReg;
import yiranwu.FrontEnd.AST.Type.FuncType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by wyr on 17-5-23.
 */
public abstract class Allocator {
    public FuncType funcType;
    public Map<VReg,PhyReg> mapping;

    public Allocator(FuncType function) {
        this.funcType=function;
        this.mapping=new HashMap<>();
    }
}
