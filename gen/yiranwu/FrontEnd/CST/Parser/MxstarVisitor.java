// Generated from /home/wyr/compiler_project/compiler2017-2.0/src/yiranwu/FrontEnd/CST/Parser/Mxstar.g4 by ANTLR 4.6
package yiranwu.FrontEnd.CST.Parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MxstarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MxstarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MxstarParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(MxstarParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Simp}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimp(MxstarParser.SimpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc(MxstarParser.FuncContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Class}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass(MxstarParser.ClassContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#simpDeclStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpDeclStmt(MxstarParser.SimpDeclStmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ArrType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrType(MxstarParser.ArrTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SimpType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpType(MxstarParser.SimpTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IdType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdType(MxstarParser.IdTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#simpTypeSpec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpTypeSpec(MxstarParser.SimpTypeSpecContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#funcDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncDef(MxstarParser.FuncDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#paramList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamList(MxstarParser.ParamListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(MxstarParser.ParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#classDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDef(MxstarParser.ClassDefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcInClass}
	 * labeled alternative in {@link MxstarParser#declInClass}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncInClass(MxstarParser.FuncInClassContext ctx);
	/**
	 * Visit a parse tree produced by the {@code simpInClass}
	 * labeled alternative in {@link MxstarParser#declInClass}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpInClass(MxstarParser.SimpInClassContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpLine(MxstarParser.ExpLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Blk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlk(MxstarParser.BlkContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IFBlk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIFBlk(MxstarParser.IFBlkContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IterBlk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIterBlk(MxstarParser.IterBlkContext ctx);
	/**
	 * Visit a parse tree produced by the {@code CtrlLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtrlLine(MxstarParser.CtrlLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code DeclLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclLine(MxstarParser.DeclLineContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#exprStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprStmt(MxstarParser.ExprStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#blkStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlkStmt(MxstarParser.BlkStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#ifStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStmt(MxstarParser.IfStmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ForBlk}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForBlk(MxstarParser.ForBlkContext ctx);
	/**
	 * Visit a parse tree produced by the {@code WhileBlk}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileBlk(MxstarParser.WhileBlkContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BreakLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakLine(MxstarParser.BreakLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ContinueLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueLine(MxstarParser.ContinueLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RetLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRetLine(MxstarParser.RetLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VisitArray}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVisitArray(MxstarParser.VisitArrayContext ctx);
	/**
	 * Visit a parse tree produced by the {@code New}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew(MxstarParser.NewContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FuncCall}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncCall(MxstarParser.FuncCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Postfix}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix(MxstarParser.PostfixContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VisitMember}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVisitMember(MxstarParser.VisitMemberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Const}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst(MxstarParser.ConstContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ID}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitID(MxstarParser.IDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Binary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary(MxstarParser.BinaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpr(MxstarParser.ParenExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary(MxstarParser.UnaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#argList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgList(MxstarParser.ArgListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MxstarParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(MxstarParser.ConstantContext ctx);
}