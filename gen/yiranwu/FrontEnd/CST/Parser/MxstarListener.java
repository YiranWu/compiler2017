// Generated from /home/wyr/compiler_project/compiler2017-2.0/src/yiranwu/FrontEnd/CST/Parser/Mxstar.g4 by ANTLR 4.6
package yiranwu.FrontEnd.CST.Parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MxstarParser}.
 */
public interface MxstarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MxstarParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(MxstarParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(MxstarParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Simp}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterSimp(MxstarParser.SimpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Simp}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitSimp(MxstarParser.SimpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterFunc(MxstarParser.FuncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitFunc(MxstarParser.FuncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Class}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterClass(MxstarParser.ClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Class}
	 * labeled alternative in {@link MxstarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitClass(MxstarParser.ClassContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#simpDeclStmt}.
	 * @param ctx the parse tree
	 */
	void enterSimpDeclStmt(MxstarParser.SimpDeclStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#simpDeclStmt}.
	 * @param ctx the parse tree
	 */
	void exitSimpDeclStmt(MxstarParser.SimpDeclStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArrType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void enterArrType(MxstarParser.ArrTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArrType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void exitArrType(MxstarParser.ArrTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SimpType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void enterSimpType(MxstarParser.SimpTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SimpType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void exitSimpType(MxstarParser.SimpTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IdType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void enterIdType(MxstarParser.IdTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IdType}
	 * labeled alternative in {@link MxstarParser#typeSpec}.
	 * @param ctx the parse tree
	 */
	void exitIdType(MxstarParser.IdTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#simpTypeSpec}.
	 * @param ctx the parse tree
	 */
	void enterSimpTypeSpec(MxstarParser.SimpTypeSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#simpTypeSpec}.
	 * @param ctx the parse tree
	 */
	void exitSimpTypeSpec(MxstarParser.SimpTypeSpecContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#funcDef}.
	 * @param ctx the parse tree
	 */
	void enterFuncDef(MxstarParser.FuncDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#funcDef}.
	 * @param ctx the parse tree
	 */
	void exitFuncDef(MxstarParser.FuncDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#paramList}.
	 * @param ctx the parse tree
	 */
	void enterParamList(MxstarParser.ParamListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#paramList}.
	 * @param ctx the parse tree
	 */
	void exitParamList(MxstarParser.ParamListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#param}.
	 * @param ctx the parse tree
	 */
	void enterParam(MxstarParser.ParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#param}.
	 * @param ctx the parse tree
	 */
	void exitParam(MxstarParser.ParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#classDef}.
	 * @param ctx the parse tree
	 */
	void enterClassDef(MxstarParser.ClassDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#classDef}.
	 * @param ctx the parse tree
	 */
	void exitClassDef(MxstarParser.ClassDefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcInClass}
	 * labeled alternative in {@link MxstarParser#declInClass}.
	 * @param ctx the parse tree
	 */
	void enterFuncInClass(MxstarParser.FuncInClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcInClass}
	 * labeled alternative in {@link MxstarParser#declInClass}.
	 * @param ctx the parse tree
	 */
	void exitFuncInClass(MxstarParser.FuncInClassContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpInClass}
	 * labeled alternative in {@link MxstarParser#declInClass}.
	 * @param ctx the parse tree
	 */
	void enterSimpInClass(MxstarParser.SimpInClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpInClass}
	 * labeled alternative in {@link MxstarParser#declInClass}.
	 * @param ctx the parse tree
	 */
	void exitSimpInClass(MxstarParser.SimpInClassContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterExpLine(MxstarParser.ExpLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitExpLine(MxstarParser.ExpLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Blk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterBlk(MxstarParser.BlkContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Blk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitBlk(MxstarParser.BlkContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IFBlk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterIFBlk(MxstarParser.IFBlkContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IFBlk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitIFBlk(MxstarParser.IFBlkContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IterBlk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterIterBlk(MxstarParser.IterBlkContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IterBlk}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitIterBlk(MxstarParser.IterBlkContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CtrlLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterCtrlLine(MxstarParser.CtrlLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CtrlLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitCtrlLine(MxstarParser.CtrlLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DeclLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterDeclLine(MxstarParser.DeclLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DeclLine}
	 * labeled alternative in {@link MxstarParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitDeclLine(MxstarParser.DeclLineContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#exprStmt}.
	 * @param ctx the parse tree
	 */
	void enterExprStmt(MxstarParser.ExprStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#exprStmt}.
	 * @param ctx the parse tree
	 */
	void exitExprStmt(MxstarParser.ExprStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#blkStmt}.
	 * @param ctx the parse tree
	 */
	void enterBlkStmt(MxstarParser.BlkStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#blkStmt}.
	 * @param ctx the parse tree
	 */
	void exitBlkStmt(MxstarParser.BlkStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void enterIfStmt(MxstarParser.IfStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void exitIfStmt(MxstarParser.IfStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ForBlk}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void enterForBlk(MxstarParser.ForBlkContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ForBlk}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void exitForBlk(MxstarParser.ForBlkContext ctx);
	/**
	 * Enter a parse tree produced by the {@code WhileBlk}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void enterWhileBlk(MxstarParser.WhileBlkContext ctx);
	/**
	 * Exit a parse tree produced by the {@code WhileBlk}
	 * labeled alternative in {@link MxstarParser#iterStmt}.
	 * @param ctx the parse tree
	 */
	void exitWhileBlk(MxstarParser.WhileBlkContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BreakLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void enterBreakLine(MxstarParser.BreakLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BreakLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void exitBreakLine(MxstarParser.BreakLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ContinueLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void enterContinueLine(MxstarParser.ContinueLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ContinueLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void exitContinueLine(MxstarParser.ContinueLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RetLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void enterRetLine(MxstarParser.RetLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RetLine}
	 * labeled alternative in {@link MxstarParser#ctrlStmt}.
	 * @param ctx the parse tree
	 */
	void exitRetLine(MxstarParser.RetLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code VisitArray}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVisitArray(MxstarParser.VisitArrayContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VisitArray}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVisitArray(MxstarParser.VisitArrayContext ctx);
	/**
	 * Enter a parse tree produced by the {@code New}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNew(MxstarParser.NewContext ctx);
	/**
	 * Exit a parse tree produced by the {@code New}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNew(MxstarParser.NewContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FuncCall}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFuncCall(MxstarParser.FuncCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FuncCall}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFuncCall(MxstarParser.FuncCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Postfix}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfix(MxstarParser.PostfixContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Postfix}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfix(MxstarParser.PostfixContext ctx);
	/**
	 * Enter a parse tree produced by the {@code VisitMember}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVisitMember(MxstarParser.VisitMemberContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VisitMember}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVisitMember(MxstarParser.VisitMemberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Const}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConst(MxstarParser.ConstContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Const}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConst(MxstarParser.ConstContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ID}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterID(MxstarParser.IDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ID}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitID(MxstarParser.IDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Binary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinary(MxstarParser.BinaryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Binary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinary(MxstarParser.BinaryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParenExpr(MxstarParser.ParenExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParenExpr(MxstarParser.ParenExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary(MxstarParser.UnaryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MxstarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary(MxstarParser.UnaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#argList}.
	 * @param ctx the parse tree
	 */
	void enterArgList(MxstarParser.ArgListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#argList}.
	 * @param ctx the parse tree
	 */
	void exitArgList(MxstarParser.ArgListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MxstarParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(MxstarParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MxstarParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(MxstarParser.ConstantContext ctx);
}