// Generated from /home/wyr/compiler_project/compiler2017-2.0/src/yiranwu/FrontEnd/CST/Parser/Mxstar.g4 by ANTLR 4.6
package yiranwu.FrontEnd.CST.Parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MxstarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		Break=1, Char=2, Class=3, Continue=4, Else=5, For=6, If=7, Int=8, Return=9, 
		Void=10, While=11, Bool=12, String=13, New=14, LeftParen=15, RightParen=16, 
		LeftBrack=17, RightBrack=18, LeftBrace=19, RightBrace=20, Less=21, Leq=22, 
		Greater=23, Geq=24, LeftShift=25, RightShift=26, Plus=27, PlusPlus=28, 
		Minus=29, MinusMinus=30, Mul=31, Div=32, Mod=33, And=34, AndAnd=35, Or=36, 
		OrOr=37, Caret=38, Not=39, Tilde=40, Semi=41, Coma=42, Assign=43, Equal=44, 
		Neq=45, Dot=46, IntConstant=47, BoolConstant=48, NullConstant=49, StrLiteral=50, 
		Identifier=51, Blank=52, Comment=53;
	public static final int
		RULE_prog = 0, RULE_declaration = 1, RULE_simpDeclStmt = 2, RULE_typeSpec = 3, 
		RULE_simpTypeSpec = 4, RULE_funcDef = 5, RULE_paramList = 6, RULE_param = 7, 
		RULE_classDef = 8, RULE_declInClass = 9, RULE_stmt = 10, RULE_exprStmt = 11, 
		RULE_blkStmt = 12, RULE_ifStmt = 13, RULE_iterStmt = 14, RULE_ctrlStmt = 15, 
		RULE_expression = 16, RULE_argList = 17, RULE_constant = 18;
	public static final String[] ruleNames = {
		"prog", "declaration", "simpDeclStmt", "typeSpec", "simpTypeSpec", "funcDef", 
		"paramList", "param", "classDef", "declInClass", "stmt", "exprStmt", "blkStmt", 
		"ifStmt", "iterStmt", "ctrlStmt", "expression", "argList", "constant"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'break'", "'char'", "'class'", "'continue'", "'else'", "'for'", 
		"'if'", "'int'", "'return'", "'void'", "'while'", "'bool'", "'string'", 
		"'new'", "'('", "')'", "'['", "']'", "'{'", "'}'", "'<'", "'<='", "'>'", 
		"'>='", "'<<'", "'>>'", "'+'", "'++'", "'-'", "'--'", "'*'", "'/'", "'%'", 
		"'&'", "'&&'", "'|'", "'||'", "'^'", "'!'", "'~'", "';'", "','", "'='", 
		"'=='", "'!='", "'.'", null, null, "'null'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "Break", "Char", "Class", "Continue", "Else", "For", "If", "Int", 
		"Return", "Void", "While", "Bool", "String", "New", "LeftParen", "RightParen", 
		"LeftBrack", "RightBrack", "LeftBrace", "RightBrace", "Less", "Leq", "Greater", 
		"Geq", "LeftShift", "RightShift", "Plus", "PlusPlus", "Minus", "MinusMinus", 
		"Mul", "Div", "Mod", "And", "AndAnd", "Or", "OrOr", "Caret", "Not", "Tilde", 
		"Semi", "Coma", "Assign", "Equal", "Neq", "Dot", "IntConstant", "BoolConstant", 
		"NullConstant", "StrLiteral", "Identifier", "Blank", "Comment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Mxstar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MxstarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Class) | (1L << Int) | (1L << Void) | (1L << Bool) | (1L << String) | (1L << Identifier))) != 0)) {
				{
				{
				setState(38);
				declaration();
				}
				}
				setState(43);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
	 
		public DeclarationContext() { }
		public void copyFrom(DeclarationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FuncContext extends DeclarationContext {
		public FuncDefContext funcDef() {
			return getRuleContext(FuncDefContext.class,0);
		}
		public FuncContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterFunc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitFunc(this);
		}
	}
	public static class ClassContext extends DeclarationContext {
		public ClassDefContext classDef() {
			return getRuleContext(ClassDefContext.class,0);
		}
		public ClassContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitClass(this);
		}
	}
	public static class SimpContext extends DeclarationContext {
		public SimpDeclStmtContext simpDeclStmt() {
			return getRuleContext(SimpDeclStmtContext.class,0);
		}
		public SimpContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterSimp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitSimp(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		try {
			setState(47);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new SimpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(44);
				simpDeclStmt();
				}
				break;
			case 2:
				_localctx = new FuncContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(45);
				funcDef();
				}
				break;
			case 3:
				_localctx = new ClassContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(46);
				classDef();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpDeclStmtContext extends ParserRuleContext {
		public TypeSpecContext typeSpec() {
			return getRuleContext(TypeSpecContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MxstarParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public SimpDeclStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpDeclStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterSimpDeclStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitSimpDeclStmt(this);
		}
	}

	public final SimpDeclStmtContext simpDeclStmt() throws RecognitionException {
		SimpDeclStmtContext _localctx = new SimpDeclStmtContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_simpDeclStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			typeSpec(0);
			setState(50);
			match(Identifier);
			setState(53);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Assign) {
				{
				setState(51);
				match(Assign);
				setState(52);
				expression(0);
				}
			}

			setState(55);
			match(Semi);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeSpecContext extends ParserRuleContext {
		public TypeSpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeSpec; }
	 
		public TypeSpecContext() { }
		public void copyFrom(TypeSpecContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArrTypeContext extends TypeSpecContext {
		public TypeSpecContext typeSpec() {
			return getRuleContext(TypeSpecContext.class,0);
		}
		public ArrTypeContext(TypeSpecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterArrType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitArrType(this);
		}
	}
	public static class SimpTypeContext extends TypeSpecContext {
		public SimpTypeSpecContext simpTypeSpec() {
			return getRuleContext(SimpTypeSpecContext.class,0);
		}
		public SimpTypeContext(TypeSpecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterSimpType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitSimpType(this);
		}
	}
	public static class IdTypeContext extends TypeSpecContext {
		public TerminalNode Identifier() { return getToken(MxstarParser.Identifier, 0); }
		public IdTypeContext(TypeSpecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterIdType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitIdType(this);
		}
	}

	public final TypeSpecContext typeSpec() throws RecognitionException {
		return typeSpec(0);
	}

	private TypeSpecContext typeSpec(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypeSpecContext _localctx = new TypeSpecContext(_ctx, _parentState);
		TypeSpecContext _prevctx = _localctx;
		int _startState = 6;
		enterRecursionRule(_localctx, 6, RULE_typeSpec, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Int:
			case Void:
			case Bool:
			case String:
				{
				_localctx = new SimpTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(58);
				simpTypeSpec();
				}
				break;
			case Identifier:
				{
				_localctx = new IdTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(59);
				match(Identifier);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(67);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ArrTypeContext(new TypeSpecContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_typeSpec);
					setState(62);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(63);
					match(LeftBrack);
					setState(64);
					match(RightBrack);
					}
					} 
				}
				setState(69);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SimpTypeSpecContext extends ParserRuleContext {
		public Token typespec;
		public SimpTypeSpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpTypeSpec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterSimpTypeSpec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitSimpTypeSpec(this);
		}
	}

	public final SimpTypeSpecContext simpTypeSpec() throws RecognitionException {
		SimpTypeSpecContext _localctx = new SimpTypeSpecContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_simpTypeSpec);
		try {
			setState(74);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Bool:
				enterOuterAlt(_localctx, 1);
				{
				setState(70);
				((SimpTypeSpecContext)_localctx).typespec = match(Bool);
				}
				break;
			case Int:
				enterOuterAlt(_localctx, 2);
				{
				setState(71);
				((SimpTypeSpecContext)_localctx).typespec = match(Int);
				}
				break;
			case Void:
				enterOuterAlt(_localctx, 3);
				{
				setState(72);
				((SimpTypeSpecContext)_localctx).typespec = match(Void);
				}
				break;
			case String:
				enterOuterAlt(_localctx, 4);
				{
				setState(73);
				((SimpTypeSpecContext)_localctx).typespec = match(String);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncDefContext extends ParserRuleContext {
		public TypeSpecContext typeSpec() {
			return getRuleContext(TypeSpecContext.class,0);
		}
		public BlkStmtContext blkStmt() {
			return getRuleContext(BlkStmtContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MxstarParser.Identifier, 0); }
		public ParamListContext paramList() {
			return getRuleContext(ParamListContext.class,0);
		}
		public FuncDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterFuncDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitFuncDef(this);
		}
	}

	public final FuncDefContext funcDef() throws RecognitionException {
		FuncDefContext _localctx = new FuncDefContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_funcDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			typeSpec(0);
			setState(78);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
				setState(77);
				match(Identifier);
				}
			}

			setState(80);
			match(LeftParen);
			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Int) | (1L << Void) | (1L << Bool) | (1L << String) | (1L << Identifier))) != 0)) {
				{
				setState(81);
				paramList();
				}
			}

			setState(84);
			match(RightParen);
			setState(85);
			blkStmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamListContext extends ParserRuleContext {
		public List<ParamContext> param() {
			return getRuleContexts(ParamContext.class);
		}
		public ParamContext param(int i) {
			return getRuleContext(ParamContext.class,i);
		}
		public ParamListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterParamList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitParamList(this);
		}
	}

	public final ParamListContext paramList() throws RecognitionException {
		ParamListContext _localctx = new ParamListContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_paramList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			param();
			setState(92);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Coma) {
				{
				{
				setState(88);
				match(Coma);
				setState(89);
				param();
				}
				}
				setState(94);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamContext extends ParserRuleContext {
		public TypeSpecContext typeSpec() {
			return getRuleContext(TypeSpecContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MxstarParser.Identifier, 0); }
		public ParamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterParam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitParam(this);
		}
	}

	public final ParamContext param() throws RecognitionException {
		ParamContext _localctx = new ParamContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_param);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			typeSpec(0);
			setState(96);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDefContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(MxstarParser.Identifier, 0); }
		public List<DeclInClassContext> declInClass() {
			return getRuleContexts(DeclInClassContext.class);
		}
		public DeclInClassContext declInClass(int i) {
			return getRuleContext(DeclInClassContext.class,i);
		}
		public ClassDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterClassDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitClassDef(this);
		}
	}

	public final ClassDefContext classDef() throws RecognitionException {
		ClassDefContext _localctx = new ClassDefContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_classDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			match(Class);
			setState(99);
			match(Identifier);
			setState(100);
			match(LeftBrace);
			setState(104);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Int) | (1L << Void) | (1L << Bool) | (1L << String) | (1L << Identifier))) != 0)) {
				{
				{
				setState(101);
				declInClass();
				}
				}
				setState(106);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(107);
			match(RightBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclInClassContext extends ParserRuleContext {
		public DeclInClassContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declInClass; }
	 
		public DeclInClassContext() { }
		public void copyFrom(DeclInClassContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FuncInClassContext extends DeclInClassContext {
		public FuncDefContext funcDef() {
			return getRuleContext(FuncDefContext.class,0);
		}
		public FuncInClassContext(DeclInClassContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterFuncInClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitFuncInClass(this);
		}
	}
	public static class SimpInClassContext extends DeclInClassContext {
		public SimpDeclStmtContext simpDeclStmt() {
			return getRuleContext(SimpDeclStmtContext.class,0);
		}
		public SimpInClassContext(DeclInClassContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterSimpInClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitSimpInClass(this);
		}
	}

	public final DeclInClassContext declInClass() throws RecognitionException {
		DeclInClassContext _localctx = new DeclInClassContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_declInClass);
		try {
			setState(111);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				_localctx = new FuncInClassContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(109);
				funcDef();
				}
				break;
			case 2:
				_localctx = new SimpInClassContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(110);
				simpDeclStmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	 
		public StmtContext() { }
		public void copyFrom(StmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExpLineContext extends StmtContext {
		public ExprStmtContext exprStmt() {
			return getRuleContext(ExprStmtContext.class,0);
		}
		public ExpLineContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterExpLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitExpLine(this);
		}
	}
	public static class BlkContext extends StmtContext {
		public BlkStmtContext blkStmt() {
			return getRuleContext(BlkStmtContext.class,0);
		}
		public BlkContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterBlk(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitBlk(this);
		}
	}
	public static class IFBlkContext extends StmtContext {
		public IfStmtContext ifStmt() {
			return getRuleContext(IfStmtContext.class,0);
		}
		public IFBlkContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterIFBlk(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitIFBlk(this);
		}
	}
	public static class IterBlkContext extends StmtContext {
		public IterStmtContext iterStmt() {
			return getRuleContext(IterStmtContext.class,0);
		}
		public IterBlkContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterIterBlk(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitIterBlk(this);
		}
	}
	public static class DeclLineContext extends StmtContext {
		public SimpDeclStmtContext simpDeclStmt() {
			return getRuleContext(SimpDeclStmtContext.class,0);
		}
		public DeclLineContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterDeclLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitDeclLine(this);
		}
	}
	public static class CtrlLineContext extends StmtContext {
		public CtrlStmtContext ctrlStmt() {
			return getRuleContext(CtrlStmtContext.class,0);
		}
		public CtrlLineContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterCtrlLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitCtrlLine(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_stmt);
		try {
			setState(119);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				_localctx = new ExpLineContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(113);
				exprStmt();
				}
				break;
			case 2:
				_localctx = new BlkContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(114);
				blkStmt();
				}
				break;
			case 3:
				_localctx = new IFBlkContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(115);
				ifStmt();
				}
				break;
			case 4:
				_localctx = new IterBlkContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(116);
				iterStmt();
				}
				break;
			case 5:
				_localctx = new CtrlLineContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(117);
				ctrlStmt();
				}
				break;
			case 6:
				_localctx = new DeclLineContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(118);
				simpDeclStmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprStmtContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExprStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterExprStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitExprStmt(this);
		}
	}

	public final ExprStmtContext exprStmt() throws RecognitionException {
		ExprStmtContext _localctx = new ExprStmtContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_exprStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << New) | (1L << LeftParen) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Not) | (1L << Tilde) | (1L << IntConstant) | (1L << BoolConstant) | (1L << NullConstant) | (1L << StrLiteral) | (1L << Identifier))) != 0)) {
				{
				setState(121);
				expression(0);
				}
			}

			setState(124);
			match(Semi);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlkStmtContext extends ParserRuleContext {
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public BlkStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blkStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterBlkStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitBlkStmt(this);
		}
	}

	public final BlkStmtContext blkStmt() throws RecognitionException {
		BlkStmtContext _localctx = new BlkStmtContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_blkStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126);
			match(LeftBrace);
			setState(130);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Break) | (1L << Continue) | (1L << For) | (1L << If) | (1L << Int) | (1L << Return) | (1L << Void) | (1L << While) | (1L << Bool) | (1L << String) | (1L << New) | (1L << LeftParen) | (1L << LeftBrace) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Not) | (1L << Tilde) | (1L << Semi) | (1L << IntConstant) | (1L << BoolConstant) | (1L << NullConstant) | (1L << StrLiteral) | (1L << Identifier))) != 0)) {
				{
				{
				setState(127);
				stmt();
				}
				}
				setState(132);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(133);
			match(RightBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStmtContext extends ParserRuleContext {
		public StmtContext thenstmt;
		public StmtContext elsestmt;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public IfStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterIfStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitIfStmt(this);
		}
	}

	public final IfStmtContext ifStmt() throws RecognitionException {
		IfStmtContext _localctx = new IfStmtContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_ifStmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(135);
			match(If);
			setState(136);
			match(LeftParen);
			setState(137);
			expression(0);
			setState(138);
			match(RightParen);
			setState(139);
			((IfStmtContext)_localctx).thenstmt = stmt();
			setState(142);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(140);
				match(Else);
				setState(141);
				((IfStmtContext)_localctx).elsestmt = stmt();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IterStmtContext extends ParserRuleContext {
		public IterStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_iterStmt; }
	 
		public IterStmtContext() { }
		public void copyFrom(IterStmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ForBlkContext extends IterStmtContext {
		public ExpressionContext init;
		public ExpressionContext cond;
		public ExpressionContext step;
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ForBlkContext(IterStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterForBlk(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitForBlk(this);
		}
	}
	public static class WhileBlkContext extends IterStmtContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public WhileBlkContext(IterStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterWhileBlk(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitWhileBlk(this);
		}
	}

	public final IterStmtContext iterStmt() throws RecognitionException {
		IterStmtContext _localctx = new IterStmtContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_iterStmt);
		int _la;
		try {
			setState(165);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case For:
				_localctx = new ForBlkContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(144);
				match(For);
				setState(145);
				match(LeftParen);
				setState(147);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << New) | (1L << LeftParen) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Not) | (1L << Tilde) | (1L << IntConstant) | (1L << BoolConstant) | (1L << NullConstant) | (1L << StrLiteral) | (1L << Identifier))) != 0)) {
					{
					setState(146);
					((ForBlkContext)_localctx).init = expression(0);
					}
				}

				setState(149);
				match(Semi);
				setState(151);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << New) | (1L << LeftParen) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Not) | (1L << Tilde) | (1L << IntConstant) | (1L << BoolConstant) | (1L << NullConstant) | (1L << StrLiteral) | (1L << Identifier))) != 0)) {
					{
					setState(150);
					((ForBlkContext)_localctx).cond = expression(0);
					}
				}

				setState(153);
				match(Semi);
				setState(155);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << New) | (1L << LeftParen) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Not) | (1L << Tilde) | (1L << IntConstant) | (1L << BoolConstant) | (1L << NullConstant) | (1L << StrLiteral) | (1L << Identifier))) != 0)) {
					{
					setState(154);
					((ForBlkContext)_localctx).step = expression(0);
					}
				}

				setState(157);
				match(RightParen);
				setState(158);
				stmt();
				}
				break;
			case While:
				_localctx = new WhileBlkContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(159);
				match(While);
				setState(160);
				match(LeftParen);
				setState(161);
				expression(0);
				setState(162);
				match(RightParen);
				setState(163);
				stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CtrlStmtContext extends ParserRuleContext {
		public CtrlStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ctrlStmt; }
	 
		public CtrlStmtContext() { }
		public void copyFrom(CtrlStmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ContinueLineContext extends CtrlStmtContext {
		public ContinueLineContext(CtrlStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterContinueLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitContinueLine(this);
		}
	}
	public static class RetLineContext extends CtrlStmtContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RetLineContext(CtrlStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterRetLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitRetLine(this);
		}
	}
	public static class BreakLineContext extends CtrlStmtContext {
		public BreakLineContext(CtrlStmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterBreakLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitBreakLine(this);
		}
	}

	public final CtrlStmtContext ctrlStmt() throws RecognitionException {
		CtrlStmtContext _localctx = new CtrlStmtContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_ctrlStmt);
		int _la;
		try {
			setState(176);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Break:
				_localctx = new BreakLineContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(167);
				match(Break);
				setState(168);
				match(Semi);
				}
				break;
			case Continue:
				_localctx = new ContinueLineContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(169);
				match(Continue);
				setState(170);
				match(Semi);
				}
				break;
			case Return:
				_localctx = new RetLineContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(171);
				match(Return);
				setState(173);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << New) | (1L << LeftParen) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Not) | (1L << Tilde) | (1L << IntConstant) | (1L << BoolConstant) | (1L << NullConstant) | (1L << StrLiteral) | (1L << Identifier))) != 0)) {
					{
					setState(172);
					expression(0);
					}
				}

				setState(175);
				match(Semi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VisitArrayContext extends ExpressionContext {
		public ExpressionContext arrname;
		public ExpressionContext indice;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public VisitArrayContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterVisitArray(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitVisitArray(this);
		}
	}
	public static class NewContext extends ExpressionContext {
		public SimpTypeSpecContext simpTypeSpec() {
			return getRuleContext(SimpTypeSpecContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public NewContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterNew(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitNew(this);
		}
	}
	public static class FuncCallContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArgListContext argList() {
			return getRuleContext(ArgListContext.class,0);
		}
		public FuncCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterFuncCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitFuncCall(this);
		}
	}
	public static class PostfixContext extends ExpressionContext {
		public Token op;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PostfixContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterPostfix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitPostfix(this);
		}
	}
	public static class VisitMemberContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MxstarParser.Identifier, 0); }
		public VisitMemberContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterVisitMember(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitVisitMember(this);
		}
	}
	public static class ConstContext extends ExpressionContext {
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public ConstContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterConst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitConst(this);
		}
	}
	public static class IDContext extends ExpressionContext {
		public TerminalNode Identifier() { return getToken(MxstarParser.Identifier, 0); }
		public IDContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterID(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitID(this);
		}
	}
	public static class BinaryContext extends ExpressionContext {
		public Token op;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public BinaryContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterBinary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitBinary(this);
		}
	}
	public static class ParenExprContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParenExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterParenExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitParenExpr(this);
		}
	}
	public static class UnaryContext extends ExpressionContext {
		public Token op;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public UnaryContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterUnary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitUnary(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(213);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Identifier:
				{
				_localctx = new IDContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(179);
				match(Identifier);
				}
				break;
			case IntConstant:
			case BoolConstant:
			case NullConstant:
			case StrLiteral:
				{
				_localctx = new ConstContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(180);
				constant();
				}
				break;
			case LeftParen:
				{
				_localctx = new ParenExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(181);
				match(LeftParen);
				setState(182);
				expression(0);
				setState(183);
				match(RightParen);
				}
				break;
			case PlusPlus:
				{
				_localctx = new UnaryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(185);
				((UnaryContext)_localctx).op = match(PlusPlus);
				setState(186);
				expression(25);
				}
				break;
			case MinusMinus:
				{
				_localctx = new UnaryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(187);
				((UnaryContext)_localctx).op = match(MinusMinus);
				setState(188);
				expression(24);
				}
				break;
			case Minus:
				{
				_localctx = new UnaryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(189);
				((UnaryContext)_localctx).op = match(Minus);
				setState(190);
				expression(23);
				}
				break;
			case Tilde:
				{
				_localctx = new UnaryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(191);
				((UnaryContext)_localctx).op = match(Tilde);
				setState(192);
				expression(22);
				}
				break;
			case Not:
				{
				_localctx = new UnaryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(193);
				((UnaryContext)_localctx).op = match(Not);
				setState(194);
				expression(21);
				}
				break;
			case New:
				{
				_localctx = new NewContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(195);
				match(New);
				setState(196);
				simpTypeSpec();
				setState(203);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(197);
						match(LeftBrack);
						setState(198);
						expression(0);
						setState(199);
						match(RightBrack);
						}
						} 
					}
					setState(205);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
				}
				setState(210);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(206);
						match(LeftBrack);
						setState(207);
						match(RightBrack);
						}
						} 
					}
					setState(212);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(292);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(290);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
					case 1:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(215);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(216);
						((BinaryContext)_localctx).op = match(Mul);
						setState(217);
						expression(20);
						}
						break;
					case 2:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(218);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(219);
						((BinaryContext)_localctx).op = match(Div);
						setState(220);
						expression(19);
						}
						break;
					case 3:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(221);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(222);
						((BinaryContext)_localctx).op = match(Mod);
						setState(223);
						expression(18);
						}
						break;
					case 4:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(224);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(225);
						((BinaryContext)_localctx).op = match(Plus);
						setState(226);
						expression(17);
						}
						break;
					case 5:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(227);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(228);
						((BinaryContext)_localctx).op = match(Minus);
						setState(229);
						expression(16);
						}
						break;
					case 6:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(230);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(231);
						((BinaryContext)_localctx).op = match(LeftShift);
						setState(232);
						expression(15);
						}
						break;
					case 7:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(233);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(234);
						((BinaryContext)_localctx).op = match(RightShift);
						setState(235);
						expression(14);
						}
						break;
					case 8:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(236);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(237);
						((BinaryContext)_localctx).op = match(Greater);
						setState(238);
						expression(13);
						}
						break;
					case 9:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(239);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(240);
						((BinaryContext)_localctx).op = match(Geq);
						setState(241);
						expression(12);
						}
						break;
					case 10:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(242);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(243);
						((BinaryContext)_localctx).op = match(Less);
						setState(244);
						expression(11);
						}
						break;
					case 11:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(245);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(246);
						((BinaryContext)_localctx).op = match(Leq);
						setState(247);
						expression(10);
						}
						break;
					case 12:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(248);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(249);
						((BinaryContext)_localctx).op = match(Equal);
						setState(250);
						expression(9);
						}
						break;
					case 13:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(251);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(252);
						((BinaryContext)_localctx).op = match(Neq);
						setState(253);
						expression(8);
						}
						break;
					case 14:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(254);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(255);
						((BinaryContext)_localctx).op = match(And);
						setState(256);
						expression(7);
						}
						break;
					case 15:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(257);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(258);
						((BinaryContext)_localctx).op = match(Caret);
						setState(259);
						expression(6);
						}
						break;
					case 16:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(260);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(261);
						((BinaryContext)_localctx).op = match(Or);
						setState(262);
						expression(5);
						}
						break;
					case 17:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(263);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(264);
						((BinaryContext)_localctx).op = match(AndAnd);
						setState(265);
						expression(4);
						}
						break;
					case 18:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(266);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(267);
						((BinaryContext)_localctx).op = match(OrOr);
						setState(268);
						expression(3);
						}
						break;
					case 19:
						{
						_localctx = new BinaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(269);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(270);
						((BinaryContext)_localctx).op = match(Assign);
						setState(271);
						expression(1);
						}
						break;
					case 20:
						{
						_localctx = new PostfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(272);
						if (!(precpred(_ctx, 30))) throw new FailedPredicateException(this, "precpred(_ctx, 30)");
						setState(273);
						((PostfixContext)_localctx).op = match(PlusPlus);
						}
						break;
					case 21:
						{
						_localctx = new PostfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(274);
						if (!(precpred(_ctx, 29))) throw new FailedPredicateException(this, "precpred(_ctx, 29)");
						setState(275);
						((PostfixContext)_localctx).op = match(MinusMinus);
						}
						break;
					case 22:
						{
						_localctx = new VisitArrayContext(new ExpressionContext(_parentctx, _parentState));
						((VisitArrayContext)_localctx).arrname = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(276);
						if (!(precpred(_ctx, 28))) throw new FailedPredicateException(this, "precpred(_ctx, 28)");
						setState(277);
						match(LeftBrack);
						setState(278);
						((VisitArrayContext)_localctx).indice = expression(0);
						setState(279);
						match(RightBrack);
						}
						break;
					case 23:
						{
						_localctx = new FuncCallContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(281);
						if (!(precpred(_ctx, 27))) throw new FailedPredicateException(this, "precpred(_ctx, 27)");
						setState(282);
						match(LeftParen);
						setState(284);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << New) | (1L << LeftParen) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Not) | (1L << Tilde) | (1L << IntConstant) | (1L << BoolConstant) | (1L << NullConstant) | (1L << StrLiteral) | (1L << Identifier))) != 0)) {
							{
							setState(283);
							argList();
							}
						}

						setState(286);
						match(RightParen);
						}
						break;
					case 24:
						{
						_localctx = new VisitMemberContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(287);
						if (!(precpred(_ctx, 26))) throw new FailedPredicateException(this, "precpred(_ctx, 26)");
						setState(288);
						match(Dot);
						setState(289);
						match(Identifier);
						}
						break;
					}
					} 
				}
				setState(294);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ArgListContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ArgListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterArgList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitArgList(this);
		}
	}

	public final ArgListContext argList() throws RecognitionException {
		ArgListContext _localctx = new ArgListContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_argList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(295);
			expression(0);
			setState(300);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Coma) {
				{
				{
				setState(296);
				match(Coma);
				setState(297);
				expression(0);
				}
				}
				setState(302);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public Token type;
		public TerminalNode StrLiteral() { return getToken(MxstarParser.StrLiteral, 0); }
		public TerminalNode IntConstant() { return getToken(MxstarParser.IntConstant, 0); }
		public TerminalNode BoolConstant() { return getToken(MxstarParser.BoolConstant, 0); }
		public TerminalNode NullConstant() { return getToken(MxstarParser.NullConstant, 0); }
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MxstarListener ) ((MxstarListener)listener).exitConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_constant);
		try {
			setState(307);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case StrLiteral:
				enterOuterAlt(_localctx, 1);
				{
				setState(303);
				((ConstantContext)_localctx).type = match(StrLiteral);
				}
				break;
			case IntConstant:
				enterOuterAlt(_localctx, 2);
				{
				setState(304);
				((ConstantContext)_localctx).type = match(IntConstant);
				}
				break;
			case BoolConstant:
				enterOuterAlt(_localctx, 3);
				{
				setState(305);
				((ConstantContext)_localctx).type = match(BoolConstant);
				}
				break;
			case NullConstant:
				enterOuterAlt(_localctx, 4);
				{
				setState(306);
				((ConstantContext)_localctx).type = match(NullConstant);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 3:
			return typeSpec_sempred((TypeSpecContext)_localctx, predIndex);
		case 16:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean typeSpec_sempred(TypeSpecContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 19);
		case 2:
			return precpred(_ctx, 18);
		case 3:
			return precpred(_ctx, 17);
		case 4:
			return precpred(_ctx, 16);
		case 5:
			return precpred(_ctx, 15);
		case 6:
			return precpred(_ctx, 14);
		case 7:
			return precpred(_ctx, 13);
		case 8:
			return precpred(_ctx, 12);
		case 9:
			return precpred(_ctx, 11);
		case 10:
			return precpred(_ctx, 10);
		case 11:
			return precpred(_ctx, 9);
		case 12:
			return precpred(_ctx, 8);
		case 13:
			return precpred(_ctx, 7);
		case 14:
			return precpred(_ctx, 6);
		case 15:
			return precpred(_ctx, 5);
		case 16:
			return precpred(_ctx, 4);
		case 17:
			return precpred(_ctx, 3);
		case 18:
			return precpred(_ctx, 2);
		case 19:
			return precpred(_ctx, 1);
		case 20:
			return precpred(_ctx, 30);
		case 21:
			return precpred(_ctx, 29);
		case 22:
			return precpred(_ctx, 28);
		case 23:
			return precpred(_ctx, 27);
		case 24:
			return precpred(_ctx, 26);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\67\u0138\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\3\2\7\2*\n\2\f\2\16\2-\13\2\3\3\3\3\3\3\5\3\62\n"+
		"\3\3\4\3\4\3\4\3\4\5\48\n\4\3\4\3\4\3\5\3\5\3\5\5\5?\n\5\3\5\3\5\3\5\7"+
		"\5D\n\5\f\5\16\5G\13\5\3\6\3\6\3\6\3\6\5\6M\n\6\3\7\3\7\5\7Q\n\7\3\7\3"+
		"\7\5\7U\n\7\3\7\3\7\3\7\3\b\3\b\3\b\7\b]\n\b\f\b\16\b`\13\b\3\t\3\t\3"+
		"\t\3\n\3\n\3\n\3\n\7\ni\n\n\f\n\16\nl\13\n\3\n\3\n\3\13\3\13\5\13r\n\13"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\5\fz\n\f\3\r\5\r}\n\r\3\r\3\r\3\16\3\16\7\16"+
		"\u0083\n\16\f\16\16\16\u0086\13\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\5\17\u0091\n\17\3\20\3\20\3\20\5\20\u0096\n\20\3\20\3\20\5"+
		"\20\u009a\n\20\3\20\3\20\5\20\u009e\n\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\5\20\u00a8\n\20\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u00b0\n"+
		"\21\3\21\5\21\u00b3\n\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\7\22\u00cc\n\22\f\22\16\22\u00cf\13\22\3\22\3\22\7\22\u00d3\n\22\f\22"+
		"\16\22\u00d6\13\22\5\22\u00d8\n\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u011f\n\22\3\22\3\22\3\22\3\22\7\22"+
		"\u0125\n\22\f\22\16\22\u0128\13\22\3\23\3\23\3\23\7\23\u012d\n\23\f\23"+
		"\16\23\u0130\13\23\3\24\3\24\3\24\3\24\5\24\u0136\n\24\3\24\2\4\b\"\25"+
		"\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&\2\2\u0168\2+\3\2\2\2\4\61"+
		"\3\2\2\2\6\63\3\2\2\2\b>\3\2\2\2\nL\3\2\2\2\fN\3\2\2\2\16Y\3\2\2\2\20"+
		"a\3\2\2\2\22d\3\2\2\2\24q\3\2\2\2\26y\3\2\2\2\30|\3\2\2\2\32\u0080\3\2"+
		"\2\2\34\u0089\3\2\2\2\36\u00a7\3\2\2\2 \u00b2\3\2\2\2\"\u00d7\3\2\2\2"+
		"$\u0129\3\2\2\2&\u0135\3\2\2\2(*\5\4\3\2)(\3\2\2\2*-\3\2\2\2+)\3\2\2\2"+
		"+,\3\2\2\2,\3\3\2\2\2-+\3\2\2\2.\62\5\6\4\2/\62\5\f\7\2\60\62\5\22\n\2"+
		"\61.\3\2\2\2\61/\3\2\2\2\61\60\3\2\2\2\62\5\3\2\2\2\63\64\5\b\5\2\64\67"+
		"\7\65\2\2\65\66\7-\2\2\668\5\"\22\2\67\65\3\2\2\2\678\3\2\2\289\3\2\2"+
		"\29:\7+\2\2:\7\3\2\2\2;<\b\5\1\2<?\5\n\6\2=?\7\65\2\2>;\3\2\2\2>=\3\2"+
		"\2\2?E\3\2\2\2@A\f\4\2\2AB\7\23\2\2BD\7\24\2\2C@\3\2\2\2DG\3\2\2\2EC\3"+
		"\2\2\2EF\3\2\2\2F\t\3\2\2\2GE\3\2\2\2HM\7\16\2\2IM\7\n\2\2JM\7\f\2\2K"+
		"M\7\17\2\2LH\3\2\2\2LI\3\2\2\2LJ\3\2\2\2LK\3\2\2\2M\13\3\2\2\2NP\5\b\5"+
		"\2OQ\7\65\2\2PO\3\2\2\2PQ\3\2\2\2QR\3\2\2\2RT\7\21\2\2SU\5\16\b\2TS\3"+
		"\2\2\2TU\3\2\2\2UV\3\2\2\2VW\7\22\2\2WX\5\32\16\2X\r\3\2\2\2Y^\5\20\t"+
		"\2Z[\7,\2\2[]\5\20\t\2\\Z\3\2\2\2]`\3\2\2\2^\\\3\2\2\2^_\3\2\2\2_\17\3"+
		"\2\2\2`^\3\2\2\2ab\5\b\5\2bc\7\65\2\2c\21\3\2\2\2de\7\5\2\2ef\7\65\2\2"+
		"fj\7\25\2\2gi\5\24\13\2hg\3\2\2\2il\3\2\2\2jh\3\2\2\2jk\3\2\2\2km\3\2"+
		"\2\2lj\3\2\2\2mn\7\26\2\2n\23\3\2\2\2or\5\f\7\2pr\5\6\4\2qo\3\2\2\2qp"+
		"\3\2\2\2r\25\3\2\2\2sz\5\30\r\2tz\5\32\16\2uz\5\34\17\2vz\5\36\20\2wz"+
		"\5 \21\2xz\5\6\4\2ys\3\2\2\2yt\3\2\2\2yu\3\2\2\2yv\3\2\2\2yw\3\2\2\2y"+
		"x\3\2\2\2z\27\3\2\2\2{}\5\"\22\2|{\3\2\2\2|}\3\2\2\2}~\3\2\2\2~\177\7"+
		"+\2\2\177\31\3\2\2\2\u0080\u0084\7\25\2\2\u0081\u0083\5\26\f\2\u0082\u0081"+
		"\3\2\2\2\u0083\u0086\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085"+
		"\u0087\3\2\2\2\u0086\u0084\3\2\2\2\u0087\u0088\7\26\2\2\u0088\33\3\2\2"+
		"\2\u0089\u008a\7\t\2\2\u008a\u008b\7\21\2\2\u008b\u008c\5\"\22\2\u008c"+
		"\u008d\7\22\2\2\u008d\u0090\5\26\f\2\u008e\u008f\7\7\2\2\u008f\u0091\5"+
		"\26\f\2\u0090\u008e\3\2\2\2\u0090\u0091\3\2\2\2\u0091\35\3\2\2\2\u0092"+
		"\u0093\7\b\2\2\u0093\u0095\7\21\2\2\u0094\u0096\5\"\22\2\u0095\u0094\3"+
		"\2\2\2\u0095\u0096\3\2\2\2\u0096\u0097\3\2\2\2\u0097\u0099\7+\2\2\u0098"+
		"\u009a\5\"\22\2\u0099\u0098\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009b\3"+
		"\2\2\2\u009b\u009d\7+\2\2\u009c\u009e\5\"\22\2\u009d\u009c\3\2\2\2\u009d"+
		"\u009e\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a0\7\22\2\2\u00a0\u00a8\5"+
		"\26\f\2\u00a1\u00a2\7\r\2\2\u00a2\u00a3\7\21\2\2\u00a3\u00a4\5\"\22\2"+
		"\u00a4\u00a5\7\22\2\2\u00a5\u00a6\5\26\f\2\u00a6\u00a8\3\2\2\2\u00a7\u0092"+
		"\3\2\2\2\u00a7\u00a1\3\2\2\2\u00a8\37\3\2\2\2\u00a9\u00aa\7\3\2\2\u00aa"+
		"\u00b3\7+\2\2\u00ab\u00ac\7\6\2\2\u00ac\u00b3\7+\2\2\u00ad\u00af\7\13"+
		"\2\2\u00ae\u00b0\5\"\22\2\u00af\u00ae\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0"+
		"\u00b1\3\2\2\2\u00b1\u00b3\7+\2\2\u00b2\u00a9\3\2\2\2\u00b2\u00ab\3\2"+
		"\2\2\u00b2\u00ad\3\2\2\2\u00b3!\3\2\2\2\u00b4\u00b5\b\22\1\2\u00b5\u00d8"+
		"\7\65\2\2\u00b6\u00d8\5&\24\2\u00b7\u00b8\7\21\2\2\u00b8\u00b9\5\"\22"+
		"\2\u00b9\u00ba\7\22\2\2\u00ba\u00d8\3\2\2\2\u00bb\u00bc\7\36\2\2\u00bc"+
		"\u00d8\5\"\22\33\u00bd\u00be\7 \2\2\u00be\u00d8\5\"\22\32\u00bf\u00c0"+
		"\7\37\2\2\u00c0\u00d8\5\"\22\31\u00c1\u00c2\7*\2\2\u00c2\u00d8\5\"\22"+
		"\30\u00c3\u00c4\7)\2\2\u00c4\u00d8\5\"\22\27\u00c5\u00c6\7\20\2\2\u00c6"+
		"\u00cd\5\n\6\2\u00c7\u00c8\7\23\2\2\u00c8\u00c9\5\"\22\2\u00c9\u00ca\7"+
		"\24\2\2\u00ca\u00cc\3\2\2\2\u00cb\u00c7\3\2\2\2\u00cc\u00cf\3\2\2\2\u00cd"+
		"\u00cb\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce\u00d4\3\2\2\2\u00cf\u00cd\3\2"+
		"\2\2\u00d0\u00d1\7\23\2\2\u00d1\u00d3\7\24\2\2\u00d2\u00d0\3\2\2\2\u00d3"+
		"\u00d6\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d8\3\2"+
		"\2\2\u00d6\u00d4\3\2\2\2\u00d7\u00b4\3\2\2\2\u00d7\u00b6\3\2\2\2\u00d7"+
		"\u00b7\3\2\2\2\u00d7\u00bb\3\2\2\2\u00d7\u00bd\3\2\2\2\u00d7\u00bf\3\2"+
		"\2\2\u00d7\u00c1\3\2\2\2\u00d7\u00c3\3\2\2\2\u00d7\u00c5\3\2\2\2\u00d8"+
		"\u0126\3\2\2\2\u00d9\u00da\f\25\2\2\u00da\u00db\7!\2\2\u00db\u0125\5\""+
		"\22\26\u00dc\u00dd\f\24\2\2\u00dd\u00de\7\"\2\2\u00de\u0125\5\"\22\25"+
		"\u00df\u00e0\f\23\2\2\u00e0\u00e1\7#\2\2\u00e1\u0125\5\"\22\24\u00e2\u00e3"+
		"\f\22\2\2\u00e3\u00e4\7\35\2\2\u00e4\u0125\5\"\22\23\u00e5\u00e6\f\21"+
		"\2\2\u00e6\u00e7\7\37\2\2\u00e7\u0125\5\"\22\22\u00e8\u00e9\f\20\2\2\u00e9"+
		"\u00ea\7\33\2\2\u00ea\u0125\5\"\22\21\u00eb\u00ec\f\17\2\2\u00ec\u00ed"+
		"\7\34\2\2\u00ed\u0125\5\"\22\20\u00ee\u00ef\f\16\2\2\u00ef\u00f0\7\31"+
		"\2\2\u00f0\u0125\5\"\22\17\u00f1\u00f2\f\r\2\2\u00f2\u00f3\7\32\2\2\u00f3"+
		"\u0125\5\"\22\16\u00f4\u00f5\f\f\2\2\u00f5\u00f6\7\27\2\2\u00f6\u0125"+
		"\5\"\22\r\u00f7\u00f8\f\13\2\2\u00f8\u00f9\7\30\2\2\u00f9\u0125\5\"\22"+
		"\f\u00fa\u00fb\f\n\2\2\u00fb\u00fc\7.\2\2\u00fc\u0125\5\"\22\13\u00fd"+
		"\u00fe\f\t\2\2\u00fe\u00ff\7/\2\2\u00ff\u0125\5\"\22\n\u0100\u0101\f\b"+
		"\2\2\u0101\u0102\7$\2\2\u0102\u0125\5\"\22\t\u0103\u0104\f\7\2\2\u0104"+
		"\u0105\7(\2\2\u0105\u0125\5\"\22\b\u0106\u0107\f\6\2\2\u0107\u0108\7&"+
		"\2\2\u0108\u0125\5\"\22\7\u0109\u010a\f\5\2\2\u010a\u010b\7%\2\2\u010b"+
		"\u0125\5\"\22\6\u010c\u010d\f\4\2\2\u010d\u010e\7\'\2\2\u010e\u0125\5"+
		"\"\22\5\u010f\u0110\f\3\2\2\u0110\u0111\7-\2\2\u0111\u0125\5\"\22\3\u0112"+
		"\u0113\f \2\2\u0113\u0125\7\36\2\2\u0114\u0115\f\37\2\2\u0115\u0125\7"+
		" \2\2\u0116\u0117\f\36\2\2\u0117\u0118\7\23\2\2\u0118\u0119\5\"\22\2\u0119"+
		"\u011a\7\24\2\2\u011a\u0125\3\2\2\2\u011b\u011c\f\35\2\2\u011c\u011e\7"+
		"\21\2\2\u011d\u011f\5$\23\2\u011e\u011d\3\2\2\2\u011e\u011f\3\2\2\2\u011f"+
		"\u0120\3\2\2\2\u0120\u0125\7\22\2\2\u0121\u0122\f\34\2\2\u0122\u0123\7"+
		"\60\2\2\u0123\u0125\7\65\2\2\u0124\u00d9\3\2\2\2\u0124\u00dc\3\2\2\2\u0124"+
		"\u00df\3\2\2\2\u0124\u00e2\3\2\2\2\u0124\u00e5\3\2\2\2\u0124\u00e8\3\2"+
		"\2\2\u0124\u00eb\3\2\2\2\u0124\u00ee\3\2\2\2\u0124\u00f1\3\2\2\2\u0124"+
		"\u00f4\3\2\2\2\u0124\u00f7\3\2\2\2\u0124\u00fa\3\2\2\2\u0124\u00fd\3\2"+
		"\2\2\u0124\u0100\3\2\2\2\u0124\u0103\3\2\2\2\u0124\u0106\3\2\2\2\u0124"+
		"\u0109\3\2\2\2\u0124\u010c\3\2\2\2\u0124\u010f\3\2\2\2\u0124\u0112\3\2"+
		"\2\2\u0124\u0114\3\2\2\2\u0124\u0116\3\2\2\2\u0124\u011b\3\2\2\2\u0124"+
		"\u0121\3\2\2\2\u0125\u0128\3\2\2\2\u0126\u0124\3\2\2\2\u0126\u0127\3\2"+
		"\2\2\u0127#\3\2\2\2\u0128\u0126\3\2\2\2\u0129\u012e\5\"\22\2\u012a\u012b"+
		"\7,\2\2\u012b\u012d\5\"\22\2\u012c\u012a\3\2\2\2\u012d\u0130\3\2\2\2\u012e"+
		"\u012c\3\2\2\2\u012e\u012f\3\2\2\2\u012f%\3\2\2\2\u0130\u012e\3\2\2\2"+
		"\u0131\u0136\7\64\2\2\u0132\u0136\7\61\2\2\u0133\u0136\7\62\2\2\u0134"+
		"\u0136\7\63\2\2\u0135\u0131\3\2\2\2\u0135\u0132\3\2\2\2\u0135\u0133\3"+
		"\2\2\2\u0135\u0134\3\2\2\2\u0136\'\3\2\2\2\37+\61\67>ELPT^jqy|\u0084\u0090"+
		"\u0095\u0099\u009d\u00a7\u00af\u00b2\u00cd\u00d4\u00d7\u011e\u0124\u0126"+
		"\u012e\u0135";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}